//image settings are up here
int screen_margin = 75;

//application running data
frame chart = null;
frame chart2 = null;
boolean isRunning = false;
CSV csv = null;



void setup(){
  size(600,600);  
  chart  = new chartFrame();
  chart2 = new chartFrame();
  csv = new CSV();
  selectInput("Select a csv data file to use","parseCSV");
  
}

void parseCSV(File csvfile){
   if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
   }else{
     csv.setupData(csvfile);
     //we have retrieved the data we need, begin running!
     isRunning = true;
     
     //decide how we will calibrate the scale
     int maxDimension = csv.getMax("VALUE1");
     if(csv.getMax("VALUE0") > maxDimension){maxDimension = csv.getMax("VALUE0");}
     
     chart.setupData("Test Chart",
                     "YEAR",0,
                     "VALUES 1 (BAR) AND 0 (LINE)",2,  //what? you thought the axis name is tied to the column data? pfffft
                     ColorMode.HIGHLIGHT,3,
                     csv,ChartType.BAR);
      chart.setToolTip("","YEAR","VALUE1");               
      
     //here we need to omit some things to prevent clashing with the other chart
     chart2.setupData("",
                       "",0 , 
                       "",1,
                       ColorMode.MAKE_BRIGHT,3,
                       csv,ChartType.LINE);
     chart2.setToolTip("","YEAR","VALUE0");
     chart2.forceScale(maxDimension);

   }
   
}

void draw(){
  background( 255 );
  if(isRunning){
    if( chart != null ){
       chart.setRect( screen_margin, screen_margin, width -screen_margin*2 , height -screen_margin*2  );
       chart.draw();
    }    
    if( chart2 != null){
       chart2.setRect( screen_margin, screen_margin, width -screen_margin*2 , height -screen_margin*2  );
       chart2.draw();
    }
  }
}


//a helper class for dealing with CSV parsing and data
class CSV
{
 String path = ""; 
 Table data = null;
 void setupData(File file)
 {
   path = file.getAbsolutePath();
   data = loadTable(path,"header");
 }
 int getMax(String Column)
 {
   float maxval = 0;
     int xsize = csv.data.getRowCount();
      for(int i = 0; i < xsize; i ++)
      {
        if(csv.data.getRow(i).getFloat(Column) > maxval)
          maxval = csv.data.getRow(i).getFloat(Column) ;
      }
   return (int)maxval;
 }
}

abstract class frame {
  int u0,v0,w,h;
  int framewidth = 0;
  int frameheight = 0;
  int framemode = 0;
  String tooltip = "";
  String tooltipX = "";
  String tooltipY = "";
  String tooltipXData = "";
  String tooltipYData = "";
  float maxval = 0f;
  void setRect( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
    
    this.framewidth = w -u0;
    this.frameheight = h - v0;
   }
  void drawFrame(){
    stroke(0);
    noFill();
    rect(u0,v0,w,h);
  };
  abstract void setupData(String t, String x,int xcol, String y,int ycol,ColorMode cm, int colorcol,CSV csv,ChartType chartType);
  abstract void draw(); 
  void setToolTip(String t, String tx, String ty)
  {
    tooltip = t; tooltipX = tx; tooltipY = ty;
  }
  void forceScale(int maxScale)
  {
    maxval = maxScale;
  }
}