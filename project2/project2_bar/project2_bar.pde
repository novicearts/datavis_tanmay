//image settings are up here
int screen_margin = 75;

//application running data
frame chart = null;
boolean isRunning = false;
CSV csv = null;



void setup(){
  size(600,600);  
  chart  = new chartFrame();
  csv = new CSV();
  selectInput("Select a csv data file to use","parseCSV");
  
}

void parseCSV(File csvfile){
   if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
   }else{
     csv.setupData(csvfile);
     //we have retrieved the data we need, begin running!
     
     
     chart.setupData("Test Chart",
                     "YEAR",0,
                     "VALUE 0",1,
                     ColorMode.HIGHLIGHT,3,
                     csv,ChartType.BAR);
    isRunning = true;
   }
   
}

void draw(){
  background( 255 );
  
  if(isRunning){
    println("fileloaded");
    println(csv.data.getRow(1).getString("PARTY"));
    if( chart != null ){
       chart.setRect( screen_margin, screen_margin, width -screen_margin*2 , height -screen_margin*2  );
       chart.draw();
    }     
  }
  else
  {
    println("are we running?");
  }
}


//a helper class for dealing with CSV parsing and data
class CSV
{
 String path = ""; 
 Table data = null;
 void setupData(File file)
 {
   path = file.getAbsolutePath();
   data = loadTable(path,"header");
 }
}

abstract class frame {
  int u0,v0,w,h;
  int framewidth = 0;
  int frameheight = 0;
  int framemode = 0;
  void setRect( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
    
    this.framewidth = w -u0;
    this.frameheight = h - v0;
   }
  void drawFrame(){
    stroke(0);
    noFill();
    rect(u0,v0,w,h);
  };
  abstract void setupData(String t, String x,int xcol, String y,int ycol,ColorMode cm, int colorcol,CSV csv,ChartType chartType);
  abstract void draw(); 
}