class chartFrame extends frame
{ 
  String title = "";
  int titlemargin = 10;
  String xaxis = "";
  int xmargin = 60;
  String yaxis = "";
  int ymargin = 15;
  int yVertMargin = -40;
  int textsize = 32;
  
  int xlabeldata = 0;
  int columndata = 1;
  
  int xsize = 0;
  float maxval = 0f;

  int chartmode = 0; //0 == bar, 1 == line
  
  ColorMode colorMode;
  int colorColumn = -1;  //if <0, we dont have a use for this
  CSV csv = null;
  
  
  //use the x title and the y title to extract the actual column in the table
  void setupData(String t, String x,int xcol, String y, int ycol, ColorMode cm,int cc,CSV c, ChartType chartType)
  {
    title = t;
    xaxis = x;
    columndata = ycol;
    xlabeldata = xcol;
    yaxis = y;
    csv = c;
    this.colorMode = cm;
    this.colorColumn =cc; 
    switch(chartType){
      case BAR:
      chartmode = 0;
      break;
      case LINE:
      chartmode = 1;
      break;
      
      default:
      chartmode = 0;
      break;
    }
    
    if(csv == null)
    {print("failed to save data");}
    else
    {
      xsize = csv.data.getRowCount();
      for(int i = 0; i < xsize; i ++)
      {
        if(csv.data.getRow(i).getFloat(columndata) > maxval)
          maxval = csv.data.getRow(i).getFloat(columndata) ;
      }
    }
  }
  void draw(){
    stroke(0,0,0);
    drawFrame();
    //draw the title
    fill(50);
    textSize(textsize);
    text(title, u0+w/2 - textsize*title.length()/4, v0 - titlemargin);
    
    //draw the xaxis
    textSize(textsize/2);
    text(xaxis, u0+w/2 - textsize*xaxis.length()/8, v0 + h + xmargin);
    
    //draw the yaxis
    pushMatrix();
    translate(u0 - textsize*xaxis.length()/8-ymargin,v0 + h/2 -yVertMargin);
    rotate(-PI/2.0);
    text(yaxis, 0, 0);
    popMatrix();
    
    //now draw the data
    if(csv != null)
   {
      rectMode(CENTER); 
      textSize(textsize/2.5);
      fill(0,200,200);
      int gap = w/xsize;
      
      
      switch(chartmode)
      {
        case 0:
        {
          for(int i = 0; i < xsize; i++)
          {
            noStroke();
            //draw all the bars
            float barheight = csv.data.getRow(i).getFloat(columndata) * frameheight/maxval;
            //color the bars provided we are given a color mode
            switch(colorMode){
              case HIGHLIGHT:
              {
                if(csv.data.getRow(i).getString("PARTY").contains("DEM"))
                {
                  fill(0,0,255);
                }
                else if (csv.data.getRow(i).getString("PARTY").contains("REP"))
                {
                  fill(255,0,0);
                }
              }
              break;
              default:
              break;
            }
            
            rect(gap*i+u0+gap/2,h+v0-barheight/2,gap-15,barheight);
          }
          
        }
        break;
        case 1:
        {
           //start with getting xsize # of circles drawn
          for(int i = 0; i < xsize; i++)
          {
            float barheight = csv.data.getRow(i).getFloat(columndata) * frameheight/maxval;
            // --use this code for rendering line charts
            noStroke();
            switch(colorMode){
              case HIGHLIGHT_AND_LINE:
              case HIGHLIGHT:
              {
                if(csv.data.getRow(i).getString("PARTY").contains("DEM"))
                {
                  fill(0,0,255);
                }
                else if (csv.data.getRow(i).getString("PARTY").contains("REP"))
                {
                  fill(255,0,0);
                }
              }
              break;
              default:
              break;
            }
            ellipse(gap*i+u0+gap/2,v0+h-barheight,/*gap/2*/10,/*gap/2*/10);            
          }
          //this is for drawing the line segments
          for(int i = 0; i < xsize-1; i++)
          {
            switch(colorMode){
              case HIGHLIGHT_AND_LINE:
              {
                if(csv.data.getRow(i).getString("PARTY").contains("DEM"))
                {
                  stroke(0,0,255);
                }
                else if (csv.data.getRow(i).getString("PARTY").contains("REP"))
                {
                  stroke(255,0,0);
                }
              }
              break;
              default:
                stroke(0,0,0);
              break;
            }
            float barheight1 = csv.data.getRow(i).getFloat(columndata) * frameheight/maxval;
            float barheight2 = csv.data.getRow(i+1).getFloat(columndata) * frameheight/maxval;
            line(gap*i+u0+gap/2,v0+h-barheight1, gap*(i+1)+u0+gap/2,v0+h-barheight2);
          }
        }
        break;
      }
      fill(50);
      stroke(0,0,0);
      //handle the xdata labels here
      for(int i = 0; i < xsize; i++)
      {
        pushMatrix();
        translate(gap*i+u0+gap/2,
              v0+h  + textsize + csv.data.getRow(i).getString(xlabeldata).length());
        rotate(-PI/2.0);
        text(csv.data.getRow(i).getString(xlabeldata),0,0);
        popMatrix();
      }
     
      rectMode(CORNER);
      
      //draw the scales (i have my maxval, and i have 0)  
      float scalex = u0;
      float interval = maxval/10;
      text("0",scalex  - (textsize/2.5)*"0".length(),v0+h);
      for(int i = (int)interval; i <= maxval; i += ceil(interval))
      {
        float height = v0+h - i * frameheight/maxval;
        text(str((int)i),
            scalex - (textsize/2.5)*str((int)i).length(),
            height);
        strokeWeight(3);
        line(scalex,height,scalex+20,height);
        
      }
      strokeWeight(1);

    }
 
  }
}