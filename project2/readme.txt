Each of the projects contains one sketch demonstrating a chart type:
*BAR
*LINE
*BOTH


Color represents the PARTY associated with the data from the "test.csv" file in the data folder.
Note, the BOTH tab does contain some features different from the other 2 charts, and the test code section is very experimental code.
The BOTH project also features a tooltip for bar charts. Simply hover over a bar to get information from it.