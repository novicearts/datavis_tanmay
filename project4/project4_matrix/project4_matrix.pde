//image settings are up here
int screen_margin = 100;
int screen_width = width -screen_margin*2;
int screen_height = height - screen_margin*2;

//application running data
frame largeChart = null;
//we're gonna allocate a list of charts
ArrayList<frame> charts = new ArrayList<frame>();
boolean isRunning = false;
boolean showLarge = false;
CSV csv = null;
String title = "Scatterplot Matrix";
int numberOfColumns = 0;

void setup(){
  size(800,600);  
  screen_width = width -screen_margin*2;
  screen_height = height - screen_margin*2;
  largeChart  = new chartFrame();
  csv = new CSV();
  selectInput("Select a csv data file to use","parseCSV");
  
}



void parseCSV(File csvfile){
   if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
   }else{
     
     csv.setupData(csvfile);
     title = csv.name+" Matrix";
     //we have retrieved the data we need, begin running!
     
     ArrayList<String> floatcolumns = new ArrayList<String>();
     IntList floatindices = new IntList();
     
     //change this so that we successfully select all float/int based channels
     numberOfColumns = csv.data.getColumnCount();
     
     for(int i = 0; i < numberOfColumns; i++)
     {
       //if the column is a float...
       if(!Float.isNaN(csv.data.getRow(0).getFloat(i)))
       {
        
       floatcolumns.add(csv.data.getColumnTitle(i));
       floatindices.append(i);   
       }
       
   }
     int usableColumns = 0;
     for(String str : floatcolumns){ if(!str.equals("")){usableColumns++;} }
     
     //println("col count: "+numberOfColumns);
     int framewidthinterval = screen_width/usableColumns;
     int frameheightinterval = screen_height/usableColumns;
     //println("scrn dim: ", framewidthinterval,frameheightinterval);
     int framex = screen_margin;
     int framey = screen_margin - screen_margin/2;
     
     //first, let's get a proper cartesian product here
     //let's set the rect prematurely here
     for(int i = 0; i < usableColumns; i ++)
     {
       for(int j = i; j < usableColumns; j++)
       {
         int i_index = floatindices.get(i);
         int j_index = floatindices.get(j);
         if(i_index != j_index)
         {
           //println(csv.data.getColumnTitle(i),csv.data.getColumnTitle(j));
           
           frame chart = new chartFrame();
           
           framex = screen_margin + (framewidthinterval+ 40)*i;
           framey = screen_margin - screen_margin/3 + (frameheightinterval+20)*j;
           
           String ytitle = "";
           String xtitle = "";
           xtitle = csv.data.getColumnTitle(i_index);
           ytitle = csv.data.getColumnTitle(j_index);
           //with this, hopefully we are allowing the frame title to be correctly placed
           if(j_index-1 == i_index) 
           {
             chart.showTitle = true;
           }
           if(i_index==0)
           {
             
             chart.showYScale = true;
             chart.showYTitle = true;
           }
           if(j_index == floatindices.get(floatindices.size()-1))
           {
             chart.showXScale = true;
             println("set xscale of "+xtitle);
           }
           chart.setupData(xtitle,
                           "",i_index,
                           ytitle,j_index,
                           ColorMode.SHOW_TYPE,3,
                           csv,ChartType.SCATTER, 2, false,false);
           
           
           //now the hard part...figure out where the chart should be, and what size it should be
           chart.setRect( framex , framey , framewidthinterval , frameheightinterval  );
           
           charts.add(chart);
           
         }
       }
     }
     
    //init the chart
    ((chartFrame)largeChart).copyData((chartFrame)charts.get(0),csv);
    largeChart.setRect(screen_margin+screen_width/2,screen_margin,
                        screen_width/2,screen_height/2 - screen_margin/3);
    largeChart.showXScale = true;
    largeChart.showYScale = true;
    largeChart.showTitle = true;
    largeChart.showYTitle = true;
    largeChart.showHalfway = false;
    ((chartFrame)largeChart).drawGridLines = true;
    if(csv.hasStringColumns())
    largeChart.tooltipType = csv.stringOnlyColumns.get(csv.selectedColorType);
    //showLarge = true;
    isRunning = true;
   }
   
}

void mousePressed()
{
  showLarge = false;
  for(frame chart : charts)
  {
    chart.selectFill = color(255,0);
    chart.isSelected = false;
    if(mouseX > chart.u0 && mouseX < chart.u0 + chart.w
      &&mouseY > chart.v0 && mouseY < chart.v0 + chart.h)
    {
      showLarge = true;
      ((chartFrame)largeChart).copyData((chartFrame)chart,csv);
      
      ((chartFrame)largeChart).drawGridLines = true;
      ((chartFrame)largeChart).drawToolTip = true;
      chart.isSelected = true;
      ((chartFrame)largeChart).pointsize = 4;
      largeChart.tooltipX = ((chartFrame)chart).title;
      largeChart.tooltipY = ((chartFrame)chart).yaxis;
      if(csv.hasStringColumns())
      
      chart.selectFill = color(120,50);
    }
  }
}


void draw(){
  background( 255 );
 int textsize = 64;
 fill(0);
 
  if(isRunning){
    
    textSize(textsize/2);
    text(title, screen_margin/2+width/2 - textsize*title.length()/6, screen_margin/2 );
    for(frame chart : charts)
    {
      if( chart != null ){
         chart.draw();
      }
    }
    
    if(showLarge)
    {
      if(largeChart != null){
        largeChart.draw();
      }
    }
    if(csv.hasStringColumns())
    {
      //draw legend
      int keyheight = screen_height+screen_margin/2 ;
      int keywidth =screen_width+screen_margin/2; 
      text(csv.stringOnlyColumns.get(csv.selectedColorType)+ " Key: ", keywidth, keyheight);
      int ci = 0;
      for(String s : csv.stringValues.get(csv.selectedColorType))
      {
        keyheight += 20;
        fill(0);stroke(0);
        text(s, screen_width+screen_margin, keyheight+5);
        noStroke();
        fill(csv.colorList.get(csv.selectedColorType).get(ci));
        ellipse(keywidth+screen_margin/3,keyheight,5,5);  
        
        ci++;
      }
      
    }
  }
}


//a helper class for dealing with CSV parsing and data
class CSV
{
 String path = ""; 
 String name = "";
 StringList  stringOnlyColumns = new StringList();
 ArrayList<StringList>  stringValues = new ArrayList<StringList>();
 IntList stringColumnIndices = new IntList();
 
 //the color generated is directly associated with the string associated in the list
 ArrayList<IntList>  colorList = new ArrayList<IntList>();
 
 int selectedColorType = 0;  //this is the channel that we will use by default to show categorization using colors
 float coloralpha = 260;
 
 ArrayList<String>  floatOnlyColumns = new ArrayList<String>();
 IntList floatColumnIndices = new IntList(); 
 Table data = null;
 int numberOfColumns = 0;
 
 
 boolean hasStringColumns()
 {
   return stringOnlyColumns.size() > 0;
 }
 void setupData(File file)
 {
   path = file.getAbsolutePath();
   name = file.getName();
   int pos = name.lastIndexOf(".");
   if (pos > 0) {
       name = name.substring(0, pos);
   }
   data = loadTable(path,"header");
   numberOfColumns = data.getColumnCount();
   colorMode(HSB,360,100,100);
   for(int i = 0; i < numberOfColumns; i++)
   {
     //for float columns
     if( !Float.isNaN( data.getRow( 0 ).getFloat(i) ) ){
        floatOnlyColumns.add(data.getColumnTitle(i));
        floatColumnIndices.append(i);
      }
      //for string columns
      else{
        stringOnlyColumns.append(data.getColumnTitle(i));
        stringColumnIndices.append(i);
        
        //now get the values of that string
        StringList values = new StringList();
        IntList colorvalues = new IntList();
        for(String s : data.getStringColumn(i))
        {
          if(!values.hasValue(s))
          {
            values.append(s);
          }
        }
        
        for(int cci = 0; cci < values.size(); cci ++)
        {
          color ci = color(map(cci,0,values.size(),30,360),65,85,coloralpha);
          colorvalues.append(ci);
        }
        stringValues.add(values);
        colorList.add(colorvalues);
      }
   }
   colorMode(RGB,255);
 }
}

abstract class frame {
  int u0,v0,w,h;
  int framewidth = 0;
  int frameheight = 0;
  int framemode = 0;
  String tooltip = "";
  String tooltipX = "";
  String tooltipY = "";
  String tooltipType = "";
  String tooltipXData = "";
  String tooltipYData = "";
  String tooltipTypeData = "";
  boolean showXScale = false;
  boolean showYScale = false;
  boolean showXTitle = false;
  boolean showYTitle = false;
  boolean showTitle = false;
  boolean showHalfway = false;
  boolean isSelected = false;
  color selectFill = color(255,0);
  float dotAlpha = 60f;
  color dotFill = color(0,200,200);
  color selectedDotFill = color(200,0,100);
  void setRect( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
    
    this.framewidth = w -u0;
    this.frameheight = h - v0;
   }
  void drawFrame(){
    stroke(0);
    fill(selectFill);
    rect(u0,v0,w,h);
  };
  abstract void setupData
  (String t, String x,int xcol, String y,int ycol,ColorMode cm, int colorcol,CSV csv,ChartType chartType, int pointsize, boolean grid, boolean tooltip);
  abstract void draw(); 
}