class chartFrame extends frame
{ 
  String title = "";
  int titlemargin = 10;
  String xaxis = "";
  int xmargin = 60;
  String yaxis = "";
  int ymargin = 30;
  int yVertMargin = -40;
  int textsize = 32;
  
  int xcolumndata = 0;
  int ycolumndata = 1;
  
  int pointsize = 5;
  
  int rowsize = 0;
  float Xmaxval = 0f;
  float Ymaxval = 0f;
  float Xminval = 0f;
  float Yminval = 0f;
  ChartType chartmode;
  
  ColorMode colorMode;
  int colorColumn = -1;  //if <0, we dont have a use for this
  CSV csv = null;
  
  
  boolean drawGridLines = true;
  boolean drawToolTip = true;
  int gridinterval = 5;
  
  float clamp(float val, float min, float max)
  {
    return min(max, max(min,val));
  }
  
  //use the x title and the y title to extract the actual column in the table
  void copyData(chartFrame frame, CSV csv)
  {
    setupData(frame.title,frame.xaxis,
              frame.xcolumndata,frame.yaxis,frame.ycolumndata,
              frame.colorMode,frame.colorColumn, csv,frame.chartmode,
              frame.pointsize,frame.drawGridLines,frame.drawToolTip);
              
  }
  void setupData(String t, String x,int xcol, String y, int ycol, 
    ColorMode cm,int cc,CSV c, ChartType chartType, int pointsize, boolean grid,
                boolean tool)
  {
    title = t;
    xaxis = x;
    ycolumndata = ycol;
    xcolumndata = xcol;
    yaxis = y;
    csv = c;
    this.colorMode = cm;
    this.colorColumn =cc; 
    this.pointsize = pointsize;
    chartmode= chartType;
    drawGridLines = grid;
    drawToolTip = tool;
    Xmaxval = 0f;
    Ymaxval = 0f;
    Xminval = 0f;
    Yminval = 0f;
    if(csv == null)
    {print("failed to save data");}
    else
    {
      rowsize = csv.data.getRowCount();
      
      Xmaxval = max(csv.data.getFloatColumn(xcolumndata));
      
      Ymaxval = max(csv.data.getFloatColumn(ycolumndata));
      
      Xminval = min(csv.data.getFloatColumn(xcolumndata));
      
      Yminval = min(csv.data.getFloatColumn(ycolumndata));
      
       dotAlpha = clamp(map(rowsize, 100, 3500,255,40), 40,255);
    }
  }
  void draw(){
    stroke(0,0,0);
    drawFrame();
    //draw the title
    fill(50);
    if(showTitle){
      textSize(textsize/2);
      text(title, u0+w/2 - textsize*title.length()/8, v0 - titlemargin);
    }
    //draw the xaxis
    if(showXTitle){
      textSize(textsize/2.5);
      text(xaxis, u0+w/2 - textsize*xaxis.length()/8, v0 + h + xmargin);
    }
    
    //draw the yaxis
    if(showYTitle){
      pushMatrix();
      translate(u0 -ymargin,v0 + h/2 -yVertMargin);
      rotate(-PI/2.0);
      text(yaxis, 0, 0);
      popMatrix();
    }
    //now draw the data
    if(csv != null)
   {
      rectMode(CENTER); 
      textSize(textsize/2.5);
      
      
      
      fill(120);
      stroke(120,50);
      
      if(drawGridLines)
      {
        //draw the grid lines in regular intervals
        float xgridinterval = Xmaxval / gridinterval;
        float ygridinterval = Ymaxval / gridinterval;
        
        //draw x lines
        for(int i = 0; i < gridinterval; i++)
        {
          line(u0,v0+h-(i*ygridinterval)*(v0 + frameheight)/Ymaxval,
               (2*u0+framewidth), v0+h-(i*ygridinterval)*(v0 + frameheight)/Ymaxval);
        }
        
        //draw y lines
        for(int i = 0; i < gridinterval; i++)
        {
          line(u0 + i*xgridinterval * (u0 + framewidth)/Xmaxval, v0+h,
               u0 + i*xgridinterval * (u0 + framewidth)/Xmaxval, v0+h-(v0 + frameheight));
        }
      }
      switch(colorMode)
      {
        
        default:
        fill(dotFill,dotAlpha);
        break;
      }
        
      stroke(50);
      //initial pass to place data on the chart
      switch(chartmode)
      {
       
       //note: we might need to be careful with how we consider the x coord here 
        case SCATTER:
        {
           //start with getting xsize # of circles drawn
          for(int i = 0; i < rowsize; i++)
          {
            //float scatterwidth = csv.data.getRow(i).getFloat(xcolumndata) * (u0 + framewidth)/(Xmaxval);
            //float scatterheight = csv.data.getRow(i).getFloat(ycolumndata) * (v0 + frameheight)/Ymaxval;
            // --use this code for rendering line charts
            //println(csv.data.getColumnTitle(ycolumndata));
            float scatterwidth = 0f;
            float scatterheight = 0f;
            if(!Float.isNaN( csv.data.getRow(i).getFloat(xcolumndata)) && !Float.isNaN( csv.data.getRow(i).getFloat(ycolumndata))) 
            {
                scatterwidth = map(csv.data.getRow(i).getFloat(xcolumndata),Xminval,Xmaxval, 10 + pointsize, u0+framewidth - pointsize); 
                scatterheight = map(csv.data.getRow(i).getFloat(ycolumndata),Yminval,Ymaxval, 10 +pointsize, v0+frameheight - pointsize);
                noStroke();
            
                switch(colorMode)
                {
                  case SHOW_TYPE:
                  {
                     colorMode(HSB,360,100,100);
                    if(csv.hasStringColumns())
                    {
                      int curValue = 0;
                      String strvalue = csv.data.getRow(i).getString(csv.stringColumnIndices.get(csv.selectedColorType));
                      for(String str : csv.stringValues.get(csv.selectedColorType))
                      {
                        if(str.equals(strvalue))
                        break;
                        curValue++;
                      }
                      fill(csv.colorList.get(csv.selectedColorType).get(curValue),csv.coloralpha);
                    }
                    colorMode(RGB,255);
                  }
                  break;
                  default:break;
                }
            
                ellipse(u0+scatterwidth,v0+h-scatterheight,pointsize,pointsize);              
            }
            
          }
        }
        break;
        
        default:
        break;
      }
      
      switch(colorMode)
      {
        
        default:
        fill(dotFill,dotAlpha);
        break;
      }
      
      fill(50);
      stroke(0,0,0);
      
      
      rectMode(CORNER);
      
      
      
      //draw the scales (i have my maxval, and i have 0)  
      float scalex = u0;
      
      //position the zeros
      if(showYScale)
      text(str((int)Yminval),scalex  - (textsize/2.5)*str((int)Yminval).length(),v0+h-5);
      
      if(showXScale)
      text(str((int)Xminval),scalex + 5,v0+h + 15);
      
      //position the max values
      if(showYScale)
      text(str((int)Ymaxval),scalex  - (textsize/3)*str((int)Ymaxval).length(),v0+h - (v0 + frameheight) +(textsize/3));
      
      if(showXScale)
      text(str((int)Xmaxval),scalex + (u0+ framewidth) - (textsize/3)*str((int)Xmaxval).length(),v0+h + 15);
      
      //position the halfway points
      if(showHalfway)
      {
        text(str((int)(Ymaxval + Yminval)/2),scalex  - (textsize/3)*str((int)(Ymaxval + Yminval)/2).length(),v0+h - (Ymaxval/2)* (v0 + frameheight)/Ymaxval);
        text(str((int)(Xmaxval + Xminval)/2),scalex + (Xmaxval/2)*(u0+ framewidth)/Xmaxval,v0+h + 15);     
      }
      
      
      if(drawToolTip)
      {
        for(int i = 0; i < rowsize; i++)
          {
            tooltipXData =  csv.data.getRow(i).getString(xcolumndata);
            tooltipYData =  csv.data.getRow(i).getString(ycolumndata);
            if(csv.hasStringColumns())
            tooltipTypeData = csv.data.getRow(i).getString(csv.stringColumnIndices.get(csv.selectedColorType));
            
            //float scatterwidth = csv.data.getRow(i).getFloat(xcolumndata) * (u0 + framewidth)/(Xmaxval); 
            //float scatterheight =  csv.data.getRow(i).getFloat(ycolumndata) * (v0 + frameheight)/Ymaxval;
            
            float scatterwidth = 0f;
            float scatterheight = 0f;
            if(!Float.isNaN( csv.data.getRow(i).getFloat(xcolumndata)) && !Float.isNaN( csv.data.getRow(i).getFloat(ycolumndata))) 
            {
               scatterwidth = map(csv.data.getRow(i).getFloat(xcolumndata),Xminval,Xmaxval, 10 + pointsize, u0+framewidth - pointsize); 
               scatterheight = map(csv.data.getRow(i).getFloat(ycolumndata),Yminval,Ymaxval, 10 + pointsize, v0+frameheight - pointsize);
            
            
                float sx = u0 + scatterwidth;
                float sy = v0 + h - scatterheight;
                
                // --use this code for rendering line charts
                //noStroke();
                
                //ellipse(u0+scatterwidth,v0+h-scatterheight,pointsize,pointsize);   
                float pointradius = pointsize/2;
                
                //if we are in ellipse (check this using ellipse math)
                if( sq(mouseX - sx)/(pointradius*pointradius) + sq(mouseY - sy)/(pointradius*pointradius) < 1)
                {
                  String s1 = tooltipX+" : "+tooltipXData;
                  String s2 = tooltipY+" : "+tooltipYData;
                  String s3 = tooltipType+ " : "+tooltipTypeData;
                  int maxstrlen = 0;
                  int heightmultiplier = 3;
                  if(csv.hasStringColumns())
                  {
                     maxstrlen = max(s1.length(),max(s2.length(),s3.length()));
                     heightmultiplier = 4;
                  }
                  else
                     maxstrlen = max(s1.length(),s2.length());
                  fill(180);
                  stroke(0,0,0);
                  if(mouseX > w/2)
                  {
                    float lengthx = (textsize/2)*maxstrlen;
                    rect(mouseX - lengthx/2,mouseY,lengthx,(textsize/2)*heightmultiplier);
                    
                    fill(0);
                    text(s1,mouseX- lengthx/2+10,mouseY+20);
                    text(s2,mouseX- lengthx/2+10,mouseY+40);
                    if(csv.hasStringColumns())
                    {
                      text(s3,mouseX- lengthx/2+10,mouseY+60);
                    }
                  }
                  else
                  {
                    rect(mouseX,mouseY,(textsize/2)*s1.length(),(textsize/2)*heightmultiplier);
                    fill(0);
                    text(s1,mouseX+20,mouseY+20);
                    text(s2,mouseX+20,mouseY+40);
                    if(csv.hasStringColumns())
                    {
                      text(s3,mouseX+20,mouseY+60);
                    }
                  }
                  
                }
            }
            
          }
      }
    }
    
 
  }
}