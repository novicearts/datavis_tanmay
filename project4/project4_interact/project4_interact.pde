//-----------
/*
*
*      enums
*
*/
//-----------

public enum Direction
{
  UP,
  DOWN,
  LEFT,
  RIGHT
}

public enum Typetype
{
  STRING,
  INT,
  FLOAT,
  MULTI
}

//atm i cant think of many uses for color, so I'm leaving it like this for project 2
enum ColorMode{
  NONE,
  HIGHLIGHT,
  HIGHLIGHT_AND_LINE,
  MAKE_BRIGHT,
  GRADIENT
}

enum ChartType{
  BAR ,
  LINE,
  SCATTER
};

enum KeyboardMode
{
  ATTRIBUTES,
  SCALES
}

//-----------
/*
*
*      Application related classes
*
*
*/
//-----------


class screen_details
{
  void init()
  {
    screen_width = width - screen_margin*2;
    screen_height = height - screen_margin*2;
    key_x = screen_width + screen_margin/2;
    key_y = screen_height + screen_margin/2;
    SetTitle(title);
  }
  void init(String _title)
  {
    title = _title;
    init();
  }
  void SetTitle (String _title)
  {
    title = _title;
    title_x = screen_margin + screen_width/2;
    title_y = screen_margin/6;
  }
  int screen_margin = 100;   int screen_width = 0;   int screen_height = 0;
  int key_x = 0;   int key_y = 0;
  int title_x = 0; int title_y = 0;
  String title = "";
  int textsize = 32;
}



class application_details
{
  boolean isRunning = false;
  boolean showLarge = false;
  boolean showLegend = false;
  ArrayList<frame> charts = new ArrayList<frame>();
  int main_chart_index = 0;
  
  int chart_starting_index = 0;
  int chart_ending_index = 1;
  
  int button_starting_index = 1;
  int button_ending_index = 2;
  
  String currentAction = "";
  
  KeyboardMode keymode = KeyboardMode.ATTRIBUTES;
  
  CSV csv = null;
  void init()
  {
    csv = new CSV();
  }
}
screen_details scr = new screen_details();
application_details app = new application_details();




//-----------
/*
*
*      Processing Setup Code
*
*
*/
//-----------

void setup(){
  size(800,600);
  scr.init("");
  app.init();
  selectInput("Select a csv data file to use","parseCSV");
}




void parseCSV(File csvfile)
{
 if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
 }else{
    
    app.csv.setupData(csvfile);
    scr.init("");
    
    //setup the charts
    
    //these are the chart frames
    chartFrame testframe = new chartFrame();
    testframe.setRect(scr.screen_margin,scr.screen_margin,
                      scr.screen_width, scr.screen_height);

   //so for now thisll be hardcoded, but i really need to figure out how i want csv column selection to work
   testframe.setup(app.csv.name,app.csv,
                   app.csv.GetColumn(0),
                   app.csv.GetColumn(1));
                   
    testframe.Title.direction = Direction.UP;
    testframe.Title.V_margin = testframe.title_margin;
                   
    testframe.XScale.direction = Direction.DOWN;
    testframe.XScale.showHalfway = true;
    testframe.XAxis.direction = Direction.DOWN;
    testframe.XAxis.V_margin = 70;
    
    testframe.YScale.direction = Direction.LEFT;
    testframe.YScale.showHalfway = true;
    testframe.YAxis.direction = Direction.LEFT;
    testframe.YAxis.H_margin = 70;
    testframe.YAxis.V_margin = 0;
    
    testframe.drawToolTip  = true;
    app.charts.add(testframe);
    app.chart_starting_index = app.charts.size()-1;
    app.main_chart_index = app.chart_starting_index; 
    
    app.chart_ending_index = app.charts.size();
    
    
    //these are the button frames
    
    //x-axis buttons first
    app.button_starting_index = app.charts.size();
    int buttonXOffset = 25;
    int buttonYOffset2 = 65;
    //RIGHT BUTTON
    buttonFrame xrbutton = new buttonFrame();
    xrbutton.actionName = "changeX+";
    xrbutton.text = " >";
    
    int buttonwidth = xrbutton.buttontextsize*(int)(xrbutton.text.length()/1.5)+10;

    xrbutton.setRect(testframe.u0+testframe.w - buttonXOffset,
                        testframe.v0 + testframe.h + buttonYOffset2,
                       buttonwidth,30);
    app.charts.add(xrbutton);
    
    
    
    
    //LEFT BUTTON
    buttonFrame xlbutton = new buttonFrame();
    xlbutton.actionName = "changeX-";
    xlbutton.text = "< ";
    
    buttonwidth = xlbutton.buttontextsize*(int)(xlbutton.text.length()/1.5)+10;
    
    xlbutton.setRect(testframe.u0 - buttonwidth + buttonXOffset,
                        testframe.v0 + testframe.h + buttonYOffset2,
                       buttonwidth,30);
    app.charts.add(xlbutton);
    
    
    //y-axis buttons
    int buttonYOffset = 20;
    int buttonXOffset2 = 90;
    //UP BUTTON
    buttonFrame yubutton = new buttonFrame();
    yubutton.actionName = "changeY+";
    yubutton.text = "/\\";
    
    buttonwidth = yubutton.buttontextsize*(int)(yubutton.text.length()/1.5)+10;
    
    yubutton.setRect(testframe.u0 - buttonXOffset2,
                     testframe.v0 + buttonYOffset - 15,
                     buttonwidth,
                     30);
    app.charts.add(yubutton);
    
    //DOWN BUTTON
    buttonFrame ydbutton = new buttonFrame();
    ydbutton.actionName = "changeY-";
    ydbutton.text = "\\/";
    
    buttonwidth = ydbutton.buttontextsize*(int)(ydbutton.text.length()/1.5)+10;
    
    ydbutton.setRect(testframe.u0 - buttonXOffset2,
                     testframe.v0 + testframe.h - buttonYOffset - 15,
                     buttonwidth,
                     30);
    app.charts.add(ydbutton);
    
    
    //buttons to change the type of chart
    int buttonXOffset3 = 200;
    //RIGHT BUTTON
    buttonFrame trbutton = new buttonFrame();
    trbutton.actionName = "changeType+";
    trbutton.text = " >";
    
    buttonwidth = trbutton.buttontextsize*(int)(trbutton.text.length()/1.5)+10;

    trbutton.setRect(testframe.u0+testframe.w - buttonXOffset3,
                        0,
                       buttonwidth,30);
    app.charts.add(trbutton);
    
    
    
    
    //LEFT BUTTON
    buttonFrame tlbutton = new buttonFrame();
    tlbutton.actionName = "changeType-";
    tlbutton.text = "< ";
    
    buttonwidth = tlbutton.buttontextsize*(int)(tlbutton.text.length()/1.5)+10;
    
    tlbutton.setRect(testframe.u0 - buttonwidth + buttonXOffset3,
                        0,
                       buttonwidth,30);
    app.charts.add(tlbutton);
    
    //RELOAD BUTTON
    buttonFrame reloadButton = new buttonFrame();
    reloadButton.actionName = "reload";
    reloadButton.text = "Load...";
    
    buttonwidth = reloadButton.buttontextsize*(int)(reloadButton.text.length()/1.5)+10;
    
    reloadButton.setRect(0,
                     0,
                     buttonwidth,30);
    app.charts.add(reloadButton);
    
    //SHOW GRID BUTTON
    buttonFrame gridButton = new buttonFrame();
    gridButton.actionName = "toggle-grid";
    gridButton.text = "Grid";
    
    buttonwidth = gridButton.buttontextsize*(int)(gridButton.text.length()/1.5)+10;
    
    gridButton.setRect(reloadButton.u0,
                     reloadButton.v0 + reloadButton.h,
                     buttonwidth,30);
    app.charts.add(gridButton);
    
    app.button_ending_index = app.charts.size();
    
    app.showLegend = testframe.usingCategories;
    
    //build out the aggregate columns
    testframe.readyToRender = false;
    testframe.BuildAggregateColumns(testframe.maxElements);
    testframe.readyToRender = true;
      
      
    app.isRunning= true;
 }
}


//-----------
/*
*
*      Processing Application Code
*
*
*/
//-----------

void reload()
{
  app.isRunning = false;
  app = new application_details();
  scr = new screen_details();
  setup();   
}

void mousePressed()
{
  for(int i = app.button_starting_index; 
      i < app.button_ending_index; i++)
  {
    buttonFrame button = (buttonFrame)app.charts.get(i);
    if(button.inBounds())
    {
      button.processClick();
      return;
    }
  }
}

void mouseReleased()
{
  for(int i = app.button_starting_index; 
      i < app.button_ending_index; i++)
  {
    buttonFrame button = (buttonFrame)app.charts.get(i);
    if(button.inBounds())
    {
      button.processRelease();
      return;
    }
  }
}

void keyPressed()
{
  chartFrame mainChart = (chartFrame)app.charts.get(app.main_chart_index);
  switch(key)
  {
    case TAB:
    {
      handleAction("changeType+");
    }
    break;
    case ' ':
    {
      handleAction("changeCategory+");
    }
    break;
    case 'g':
    case 'G':
    {
      handleAction("toggle-grid");
    }
    break;
    case CODED:
    {
      
      if(keyCode == SHIFT)
      {
         switch(app.keymode)
         {
           case ATTRIBUTES: app.keymode = KeyboardMode.SCALES;break;
           case SCALES: app.keymode = KeyboardMode.ATTRIBUTES;break;
         }
         println("Change keyboard mode!",app.keymode);
         return;
      }
     
      
      
      switch(app.keymode)
      {
        case ATTRIBUTES:
        {
          switch(keyCode)
          {
            
            case UP:
            {
              handleAction("changeY+");
            }
            break;
            case DOWN:
            {
              handleAction("changeY-");
            }
            break;
            case LEFT:
            {
              handleAction("changeX-");
            }
            break;
            case RIGHT:
            {
              handleAction("changeX+");
            }
            break;
            default:break;
          }
        }
        break;
        case SCALES:
        {
          switch(keyCode)
          {
            case UP:
            {
              if(mainChart.XScale.direction == Direction.UP)
                 mainChart.XScale.direction = Direction.DOWN;
              else
                 mainChart.XScale.direction = Direction.UP;
            }
            break;
            case DOWN:
            {
              if(mainChart.XScale.direction == Direction.DOWN)
                 mainChart.XScale.direction = Direction.UP;
              else
                 mainChart.XScale.direction = Direction.DOWN;
            }
            break;
            case LEFT:
            {
              if(mainChart.YScale.direction == Direction.LEFT)
                 mainChart.YScale.direction = Direction.RIGHT;
              else
                 mainChart.YScale.direction = Direction.LEFT;
            }
            break;
            case RIGHT:
            {
              if(mainChart.YScale.direction == Direction.RIGHT)
                 mainChart.YScale.direction = Direction.LEFT;
              else
                 mainChart.YScale.direction = Direction.RIGHT;
            }
            break;
            default:break;
          }
        }
        break;
      }
     
    }
    break;
    default:break;
  }
  
}

//void GetNextAttribute()

void handleAction(String action)
{
  //note: almost ALL actions will affect the main chart, so grab a reference to it now!
  chartFrame mainChart = (chartFrame)app.charts.get(app.main_chart_index);
  println(action);
  switch(action)
  {
    
    case "changeType+":
    {
      switch(mainChart.type)
      {
        case BAR: mainChart.type = ChartType.LINE; break;
        case LINE: mainChart.type = ChartType.SCATTER; break;
        case SCATTER: mainChart.type = ChartType.BAR; break;
      }
    }
    break;
    case "changeType-":
    {
      switch(mainChart.type)
      {
        case BAR: mainChart.type = ChartType.SCATTER; break;
        case LINE: mainChart.type = ChartType.BAR; break;
        case SCATTER: mainChart.type = ChartType.LINE; break;
      }
    }
    break;
    case "changeCategory+":
    {
      mainChart.GetNextCategoryColumn();
      //app.showLegend = mainChart.usingCategories;
    }
    break;
    case "changeX+":
    {
      mainChart.XColumn = app.csv.GetNextColumn(mainChart.XColumn,mainChart.YColumn.index);
      mainChart.reset();
      //build out the aggregate columns
      mainChart.readyToRender = false;
      mainChart.BuildAggregateColumns(mainChart.maxElements);
      mainChart.readyToRender = true;
      
    }
    break;
    case "changeX-":
    {
      mainChart.XColumn = app.csv.GetPrevColumn(mainChart.XColumn,mainChart.YColumn.index);
      mainChart.reset();
      //build out the aggregate columns
      mainChart.readyToRender = false;
      mainChart.BuildAggregateColumns(mainChart.maxElements);
      mainChart.readyToRender = true;
    }
    break;
    case "changeY+":
    {
      mainChart.YColumn = app.csv.GetNextColumn(mainChart.YColumn,mainChart.XColumn.index);
      mainChart.reset();
      //build out the aggregate columns
      mainChart.readyToRender = false;
      mainChart.BuildAggregateColumns(mainChart.maxElements);
      mainChart.readyToRender = true;
    }
    break;
    case "changeY-":
    {
      mainChart.YColumn = app.csv.GetPrevColumn(mainChart.YColumn,mainChart.XColumn.index);
      mainChart.reset();
      //build out the aggregate columns
      mainChart.readyToRender = false;
      mainChart.BuildAggregateColumns(mainChart.maxElements);
      mainChart.readyToRender = true;
    }
    break;
    case "reload":
    {
      reload();
      return;
    }
    case "toggle-grid":
    {
      mainChart.drawGridlines = !mainChart.drawGridlines;
    }
    default:break;
  }
}

void draw()
{
  background(255);
  fill(0);
  if(app.isRunning)
  {
    textSize(scr.textsize/2);
    String currentmode = ((chartFrame)app.charts.get(app.main_chart_index)).type.toString();
    text(currentmode,scr.title_x,20);
    
    
    //draw everything
    for(frame chart : app.charts)
    {
      if(chart != null)
      {
        chart.draw();
      }
    }
    
    //check if any of the buttons were pressed
    for(int i = app.button_starting_index; 
        i < app.button_ending_index; i++)
    {
      buttonFrame b = (buttonFrame)app.charts.get(i);
      if(b.wasClicked)
      {
        //handle the button and clear it
        app.currentAction = b.actionName;
        b.wasClicked = false;
        break;
      }
    }
    
    //if action string is non-empty, handle it and then clear it
    if(!app.currentAction.equals(""))
    {
      handleAction(app.currentAction);
      app.currentAction = "";
    }
    
    //any additional logic
    if(app.showLegend)
    {
      chartFrame mainChart = (chartFrame)app.charts.get(app.main_chart_index);
      textSize(10);
      
      String max = mainChart.categoryColumn.name;
      max = max.substring(0, (max.length() >14 ? 14 : max.length())) + (max.length() > 14 ? "." : "");
      
      String legend = max+":";
      text(legend, mainChart.u0+mainChart.w +5, 20);
      
      for(int i = 0; i < mainChart.categoryColumn.uniqueStringValues.size(); i++)
      {
        max = mainChart.categoryColumn.uniqueStringValues.get(i);
        max = max.substring(0, (max.length() > 7 ? 7 : max.length())) + (max.length() > 7 ? "." : "");
        int x =  mainChart.u0+mainChart.w +5;
        int y = 20+20*(i+1);
        fill(0);
        text(max, x, y);
        noStroke();
        fill(mainChart.categoryColumn.colorValues.get(i));
        ellipse( x+ 7*10, y-2, mainChart.pointsize/2,mainChart.pointsize/2);
      }
    }
  }
}