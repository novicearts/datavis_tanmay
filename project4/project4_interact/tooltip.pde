
class TooltipData
{
  String xname = "";
  String yname = "";
  String typename = "";
  String xdata = "";
  String ydata = "";
  String typedata = "";
  String other = "";
  boolean showType = false;
  
  void setNames(String x, String y, String type){xname = x; yname = y; typename = type;}
  void setData(String x, String y, String type){xdata = x; ydata = y; typedata = type;}
  void SetText(String t){other = t;}
  String X(){ return xname + " : " + xdata;}
  String Y(){ return yname + " : " + ydata;}
  String Type(){ return typename + " : " + typedata;}
  String Text(){ return other;}
  
  void draw(boolean cue_drawing, int drawx, int drawy, int w, int h, int textsize, boolean adjustPosition, boolean showTextInstead)
  {
    int textwidth, textheight;  //ill see later on if i need this
    if(cue_drawing)
    {
      int maxstrlen = 0;
      int heightmultiplier = 3;
      if(showType)
      {
        maxstrlen = max(X().length(),max(Y().length(),Type().length()));
        heightmultiplier = 4;
      }
      else
      {
        maxstrlen = max(X().length(),Y().length());
      
      }
      fill(180);
      stroke(0,0,0);
      if(adjustPosition)
      {
        if(drawx > w/2)
        {
          float lengthx = (textsize/2)*maxstrlen;
          rect(drawx - lengthx/2,drawy,lengthx,(textsize/2)*heightmultiplier);
          
          fill(0);
          if(showTextInstead)
          {
            text(Text(),drawx- lengthx/2+10,drawy+20);
          }
          else
          {
            text(X(),drawx- lengthx/2+10,drawy+20);
            text(Y(),drawx- lengthx/2+10,drawy+40);
            if(showType)
            {
              text(Type(),drawx- lengthx/2+10,drawy+60);
            }
          }
          
        }
        else
        {
          rect(drawx,drawy,(textsize/2)*X().length(),(textsize/2)*heightmultiplier);
          fill(0);
          if(showTextInstead)
          {
            text(Text(),drawx+20,drawy+20);
          }
          else
          {
            text(X(),drawx+20,drawy+20);
            text(Y(),drawx+20,drawy+40);
            if(showType)
            {
              text(Type(),drawx+20,drawy+60);
            }  
          }
          
        }
      }
      else
      {
        rect(drawx,drawy,(textsize/2)*X().length(),(textsize/2)*heightmultiplier);
        fill(0);
        
        if(showTextInstead)
        {
          text(Text(),drawx+20,drawy+20);
        }
        else
        {
          text(X(),drawx+20,drawy+20);
          text(Y(),drawx+20,drawy+40);
          if(showType)
          {
            text(Type(),drawx+20,drawy+60);
          }
        }
      }
    }
  }
}