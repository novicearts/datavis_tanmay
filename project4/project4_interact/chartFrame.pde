class chartFrame extends frame
{
  int title_margin = 45;
  int pointsize = 8;
  int maxElements = 45;
  
  ChartType type = ChartType.LINE;
  void draw(){
    stroke(0);
    drawFrame();
    
    //draw title
    Title.draw(textsize,u0,v0,w,h);
    
    String originalTitle = XAxis.title;
    if(( csv.rowsize > maxElements) && (type == ChartType.LINE||type  == ChartType.BAR))
      XAxis.title += " (" + aggMode.toString() + ")";
    //draw xaxis
    XAxis.draw(u0,v0,w,h);
    XAxis.title = originalTitle;
    //draw yaxis
    YAxis.draw(u0,v0,w,h);
    
    //draw grid
    if(drawGridlines)
    drawGrid(XColumn,YColumn);
    
  
    tooltip.showType = usingCategories;
   
   if(!readyToRender) return;
   
    fill(dotFill,dotAlpha);
    stroke(50);
    //draw the data
    switch(type)
    {
      
      case BAR:
      {
       
        if(csv.rowsize > maxElements)
        {
          if(XAgg.aggElementNames.size() == 0) break;
            
            int gapX = w/XAgg.aggElementNames.size();
            int gapY = h/XAgg.aggElementNames.size();
            drawScaleInterval(XScale,XAgg,XAgg.aggElementNames.size());
            
          if(YColumn.type == Typetype.STRING)
          { 
             drawScaleIntervalStrings(YScale,YColumn);
          }
          else
          {
            drawScaleTicks(YScale);
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              if( Float.isNaN( XAgg.aggElementValues.get(i))) break;
                float liney = transformY(XAgg.aggElementValues.get(i), YScale.max,YScale.min);
                float linex = gapX*i+u0+gapX/2;
                
                fill(dotFill,dotAlpha);
                
                rect(linex-gapX/2,liney,gapX,v0+h-liney);
            }
          }
        }
        else if(csv.rowsize > 1)
        {
          if(YColumn.type == Typetype.STRING)
          {
            drawScaleIntervalStrings(YScale,YColumn);
            drawScaleInterval(XScale,XColumn,csv.rowsize);
            
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            for(int i = 0; i < csv.rowsize; i++)
            {
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
                
               //rect(linex, h+v0-liney/2 ,gap-15, liney);
               rect(linex-gapX/2,liney,gapX,v0+h-liney);
            }
          }
          else
          {
            //draw y scale
            drawScaleTicks(YScale);
            drawScaleInterval(XScale,XColumn,csv.rowsize);
            float gap = w/(float)csv.rowsize;
            for(int i = 0; i < csv.rowsize;i++)
            {
              if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;
                
                if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
                
               //rect(linex, h+v0-liney/2 ,gap-15, liney);
               rect(linex-gap/2,liney,gap,v0+h-liney);
            }
          }
        }
        
      }
      break;
      
      
      
      
      
      case LINE:
      {
        if(csv.rowsize > maxElements)
        {
            if(XAgg.aggElementNames.size() == 0) break;
            
            int gapX = w/XAgg.aggElementNames.size();
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            drawScaleInterval(XScale,XAgg,XAgg.aggElementNames.size());
            
          if(YColumn.type == Typetype.STRING)
          { 
            drawScaleIntervalStrings(YScale,YColumn);
            
            //draw the line segments
            for(int i = 0; i < XAgg.aggElementNames.size()-1; i++)
            {
              String value = XAgg.aggElementStringValues.get(i);
              String value2 = XAgg.aggElementStringValues.get(i+1);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney1 = -gapY*(valindex+1)-gapY/2+v0+h;
              
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value2.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney2 = -gapY*(valindex+1)-gapY/2+v0+h;
              if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
              
              stroke(0);
              fill(dotFill,dotAlpha);
              
              ellipse(linex2, liney2 , pointsize, pointsize);  
              //if(i==0)
              ellipse(linex1, liney1 , pointsize, pointsize);   
            }
          }
          else
          {
            //draw y scale
            drawScaleTicks(YScale);
            
            //draw the line segments
            for(int i = 0; i < XAgg.aggElementNames.size()-1; i++)
            {
               
              float f = XAgg.aggElementValues.get(i);
              float liney1 = transformY(f, YScale.max,YScale.min);
              f = XAgg.aggElementValues.get(i+1);
              float liney2 = transformY(f, YScale.max,YScale.min);
              if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
            }
            
            //iterate over the number of elements
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              //if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
              float f = XAgg.aggElementValues.get(i);
              float liney = transformY(f, YScale.max,YScale.min);
               if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              noStroke();
   
              stroke(0);
              fill(dotFill,dotAlpha);
              
              ellipse(linex, liney , pointsize, pointsize);    
            }
          }
            
        }else if(csv.rowsize > 1)
        {
          //for now, while we debug all drawing code for aggregate column, first start off by mimicking project 2. just use the test.csv files to check if at the very least the basic
          // bar chart and line chart code are still functional
          //note this seems to break down depending on the y-axis being a float or string. perhaps a compromise for now is to go ahead and allow interval math for string types and just allow positioning regardless
          
          //so basically, the y-axis will typecheck if we have a string or a float. if float, do usual scaling, otherwise break up the yaxis into intervals for each unique value, and plot all dots accordingly
          //if this works well enough,we'll apply it to the scatterplot too
          
          //for now, we will not deal with string for y columns. until mode and median are implemented
          
          drawScaleInterval(XScale,XColumn,csv.rowsize);
          
          if(YColumn.type == Typetype.STRING)
          {
            drawScaleIntervalStrings(YScale,YColumn);
            
            //because this size is small, we should just allow local assignment
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
             //draw the line segments
            for(int i = 0; i < csv.rowsize-1; i++)
            {
              String value = csv.data.getRow(i).getString(YColumn.index);
              String value2 = csv.data.getRow(i+1).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney1 = -gapY*(valindex+1)-gapY/2+v0+h;
              
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value2.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney2 = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
              
               if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
              
              ellipse(linex2, liney2 , pointsize, pointsize);  
              //if(i==0)
              ellipse(linex1, liney1 , pointsize, pointsize);   
            }
          }
          else
          {
              drawScaleTicks(YScale); //lets test the scale interval
              //drawScaleInterval(YScale,YColumn,csv.rowsize);
             float gap = w/(float)csv.rowsize;
               //draw the line segments
              for(int i = 0; i < csv.rowsize-1; i++)
              {
                if(Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                stroke(0);
                fill(0);
                float liney1 = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float liney2 = transformY(csv.data.getRow(i+1).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex1 = gap*i+u0+gap/2;
                float linex2 = gap*(i+1)+u0+gap/2;
                line(linex1,liney1,linex2,liney2);
              }
              
              //iterate over the number of elements
              for(int i = 0; i < csv.rowsize; i++)
              {
                if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;/*transformX(gap*i,u0+w,u0);*/
                noStroke();
                
                if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
                
                ellipse(linex, liney , pointsize, pointsize);    
              }
              
             
            }
          }
          //now that we have conditional scale drawing working, apply it to conditional line chart drawing
      }
      break;
      
      
      
      
      
      case SCATTER:
      {
        
        //draw x scale
        drawScaleTicks(XScale);
        
        //draw y scale
        drawScaleTicks(YScale);
        for(int i = 0; i < csv.rowsize; i++)
        {
          if(!Float.isNaN( csv.data.getRow(i).getFloat(XColumn.index)) && !Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) 
          {
            
            float scatterwidth = transformX(csv.data.getRow(i).getFloat(XColumn.index),XScale.max,XScale.min);
            float scatterheight =transformY(csv.data.getRow(i).getFloat(YColumn.index),YScale.max,YScale.min);
            
            if(usingCategories)
            {
              int curval = 0;
              String strval = csv.data.getRow(i).getString(categoryColumn.index);
              for(String str : categoryColumn.uniqueStringValues)
              {
                if(strval.equals(str))
                break;
                curval++;
              }
              fill(categoryColumn.colorValues.get(curval));
            }
            else
            fill(dotFill,dotAlpha);
            
            noStroke();
            ellipse(scatterwidth,scatterheight,pointsize,pointsize);
          }
        }
      }
      break;
      default:break;
    }
    
    //draw the tooltip
    if(drawToolTip)
    switch(type)
    {
      case BAR:
      {
        if(csv.rowsize > maxElements)
        {
          tooltip.showType = false;
          if(XAgg.aggElementNames.size() == 0) break;
            
            int gapX = w/XAgg.aggElementNames.size();
            int gapY = h/XAgg.aggElementNames.size();
            
          if(YColumn.type == Typetype.STRING)
          { 
          }
          else
          {
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              float f = XAgg.aggElementValues.get(i);
              String yi =  str(XAgg.aggElementValues.get(i));
              float liney = transformY(f, YScale.max,YScale.min);
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
                
                fill(dotFill,dotAlpha);
                
               tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              tooltip.draw(inRectBounds(mouseX,mouseY,(int)(linex-gapX/2),(int)liney,(int)gapX, (int)(v0+h - liney)),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
        }
        else if(csv.rowsize > 1)
        {
          if(YColumn.type == Typetype.STRING)
          { 
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi = csv.data.getRow(i).getString(XColumn.index);
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(usingCategories)
              {
                tooltip.setData(xi,value,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE",categoryColumn.name);
              }else{
                tooltip.setData(xi,value,"");
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              }
                
              tooltip.showType = usingCategories;
              tooltip.draw(inRectBounds(mouseX,mouseY,(int)(linex-gapX/2),(int)liney,(int)gapX, (int)(v0+h - liney)),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
          else
          {
            //boolean inRectBounds( int x, int y, int u0, int v0, int w, int h)
            float gap = w/(float)csv.rowsize;
            for(int i = 0; i < csv.rowsize;i++)
            {
              if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;
                
                String xi = csv.data.getRow(i).getString(XColumn.index);
                String yi = csv.data.getRow(i).getString(YColumn.index);
                
               if(usingCategories)
                {
                  tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
                  tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
                }else{
                  tooltip.setData(xi,yi,"");
                  tooltip.setNames(XAxis.title, YAxis.title,"");
                }
              tooltip.showType = usingCategories;
              tooltip.draw(inRectBounds(mouseX,mouseY,(int)(linex-gap/2),(int)liney,(int)gap, (int)(v0+h - liney)),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
        }
      }
      break;
      
      
      case LINE:
      {
         if(csv.rowsize > maxElements)
        {
          
           
            
          tooltip.showType = false;
          int radius = pointsize/2;
          if(XAgg.aggElementNames.size() == 0) break;
          if(YColumn.type == Typetype.STRING)
          { 

            int gapX = w/XAgg.aggElementNames.size();
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              String value = XAgg.aggElementStringValues.get(i);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
             
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
               tooltip.setData(xi,value,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              tooltip.draw(inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
            
          }
          else
          {
            
            
            int gapX = w/XAgg.aggElementNames.size();
            int gapY = h/XAgg.aggElementNames.size();
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              float f = XAgg.aggElementValues.get(i);
              String yi =  str(XAgg.aggElementValues.get(i));
              float liney = transformY(f, YScale.max,YScale.min);
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " "+aggMode.toString(),"");
              tooltip.draw(inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
        }
        else if(csv.rowsize > 1)
        {
          if(YColumn.type == Typetype.STRING)
          { 
            int gapX = w/csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            int radius = pointsize/2;
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi =  csv.data.getRow(i).getString(XColumn.index);
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
             
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(usingCategories)
              {
                tooltip.setData(xi,value,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE",categoryColumn.name);
              }else{
                tooltip.setData(xi,value,"");
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              }
              
              tooltip.draw(inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
          else
          {
            float gap = w/(float)csv.rowsize;
            int radius = pointsize/2;
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi =  csv.data.getRow(i).getString(XColumn.index);
              String yi =  csv.data.getRow(i).getString(YColumn.index);
              if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;/*transformX(gap*i,u0+w,u0);*/
                
                
              if(usingCategories)
              {
                tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
              }else{
                tooltip.setData(xi,yi,"");
                tooltip.setNames(XAxis.title, YAxis.title,"");
              }
              tooltip.showType = usingCategories;
              tooltip.draw(inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius),
                         mouseX,mouseY,w,h,textsize,true,false);
            }
          }
        }
      }
      break;
      case SCATTER:
      {
        int radius = pointsize/2;
        for(int i = 0; i < csv.rowsize; i++)
        {
          String xi =  csv.data.getRow(i).getString(XColumn.index);
          String yi =  csv.data.getRow(i).getString(YColumn.index);
          if(!Float.isNaN( csv.data.getRow(i).getFloat(XColumn.index)) && !Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) 
          {
            float scatterwidth = transformX(csv.data.getRow(i).getFloat(XColumn.index),XScale.max,XScale.min);
            float scatterheight =transformY(csv.data.getRow(i).getFloat(YColumn.index),YScale.max,YScale.min);
            
            if(usingCategories)
            {
              tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
              tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
            }else{
              tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title, YAxis.title,"");
            }
            tooltip.showType = usingCategories;
            tooltip.draw(inCircleBounds(mouseX,mouseY,(int)scatterwidth,(int)scatterheight,radius),
                         mouseX,mouseY,w,h,textsize,true,false);
          }
        }
      }
      break;
      default:break;
    }

}
  @Override void processClick()
  {
    println(Title.title);
  }
}