
//-----------
/*
*
*      Frame and Data classes
*
*
*/
//-----------

import java.util.*;
import java.util.Comparator;
abstract class frame{
  //border variables
  int u0,v0,w,h = 0;
  int frame_width, frame_height = 0;
  int textsize = 32;
  int maxCharLength = 5;
  
  boolean readyToRender = true;
  
  TooltipData tooltip = new TooltipData();
  CSVColumn XColumn = null;
  AggregateColumn XAgg = null;
  int xcsv_index = 0;
  CSVColumn YColumn = null;
  AggregateColumn YAgg = null;   //i dont think we need this but maybe in the future?
  int ycsv_index = 1;
  
  AggMode aggMode= AggMode.AVG;  //typically we will use average if dealing with a float column, and count if dealing with a string column...mileage on this varies
  
  //this will be used to determine which column we are using, if any, for categorical coloring
  //this column will EXCLUSIVELY use the csv's stringcolumns
  CSVColumn categoryColumn = null;
  int category_csv_index = 0;
  
  CSV csv = null;
  
  FrameScale XScale = new FrameScale();
  FrameScale YScale = new FrameScale();
  
  FrameTitle XAxis = new FrameTitle();
  FrameTitle YAxis = new FrameTitle();
  FrameTitle Title = new FrameTitle(); 
  
  
  
  //selection variables
  boolean isSelected = false;  
  color selectFill = color(120,50);
  float dotAlpha = 60f;
  color dotFill = color(0,200,200);
  color selectedDotFill = color(200,0,100);
  int min_border_margin = 0;
  int frame_border_offset = 2;
  
  //other drawing variables
  int gridinterval = 10;
  boolean drawGridlines = true;
  boolean drawToolTip = false;
  boolean drawTicks = false;
  boolean usingCategories = false;
  
  boolean inBounds()
  {
     return inRectBounds(mouseX,mouseY, u0,v0,w,h);
  }
  
  //we will always build aggregate columns  regardless. its just a matter of deciding whether to use it
  void BuildAggregateColumns(int maxElements)
  {
    
    XAgg = new AggregateColumn();
    YAgg = new AggregateColumn();
    XAgg.bucketsize = csv.rowsize/maxElements;
    //the idea for aggregate columns, is that for the x-axis, we are trying to get the ideal element arrangement setup
    //such that enough information is displayed in some accurate manner (ranges of floats, or unique strings), but not too much to flood the screen
    
    //go ahead and sort the 2 lists. trying to juggle the data with them being unsorted is just unfeasible
    if(XColumn.type == Typetype.STRING)
    {
      XAgg.uniqueStringValues = XColumn.uniqueStringValues;
      XAgg.stringHistogram = XColumn.stringHistogram;
      XAgg.type = Typetype.STRING;
      
      if(YColumn.type == Typetype.STRING)
      {
        YAgg.uniqueStringValues = YColumn.uniqueStringValues;
        YAgg.stringHistogram = YColumn.stringHistogram;
        YAgg.type = Typetype.STRING;
        
         String[]  xlist = csv.data.getStringColumn(XColumn.index);
         String[]  ylist = csv.data.getStringColumn(YColumn.index);
         
         ArrayList<AGG_S_S_PAIR> pairs = new ArrayList<AGG_S_S_PAIR>();
         for(int i = 0; i < csv.rowsize; i++)
         {
           pairs.add(new AGG_S_S_PAIR(xlist[i], ylist[i]));
         }
         Collections.sort(pairs,new ASSPCOMP());
         
         switch(aggMode)
         {
           case AVG:
           case COUNT_AVG:
           case MEDIAN:
           case MODE:
          {
             int cur_element = 0;
             String start = ""; String end = "";
             int ci = 0;
             int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
             IntList histogram = new IntList();
             for(int i = 0; i < YAgg.uniqueStringValues.size(); i++)
             {
               histogram.append(0);
             }
           
            for(AGG_S_S_PAIR pair:pairs)
            {
              if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
              {
                int len = (pair.x.length() < 4 ? pair.x.length() : 4);
                 end = pair.x.substring(0,len);
                 
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   int histi = 0;
                    for(String s : YAgg.uniqueStringValues)
                    {
                      if(s.equals(pair.y)) break;
                      histi++;
                    }
                    if(histi<histogram.size())
                    histogram.add(histi,1);
                    else
                    {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                 }
                 else
                 {
                   int histi = 0;
                  for(String s : YAgg.uniqueStringValues)
                  {
                    if(s.equals(pair.y)) break;
                    histi++;
                  }
                  if(histi<histogram.size())
                  histogram.add(histi,1);
                  else
                  {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                  
                 }
                 
                 //the histogram is now prepared, select the appropriate index
                 int selectedIndex =0;
                 int maxhist = max(histogram.array());
                 for(selectedIndex = 0; selectedIndex < histogram.size();selectedIndex++)
                 {
                   if(histogram.get(selectedIndex) == maxhist)break;
                 }
                 
                 String value = YAgg.uniqueStringValues.get(selectedIndex);
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementStringValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 ci++;
                 
                continue;
              }
              else if(cur_element == 0)
              {
                
                int len = (pair.x.length() < 4 ? pair.x.length() : 4);
                start = pair.x.substring(0,len);
                
                int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                
                cur_element++;
                ci++;
                continue;
              }
              //take the current value and add it to the histogram
              int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
             
              ci++;
              cur_element++;
            }
          }
          break;
         }
         

      }
      else //YColumn type is FLOAT
      {
         YAgg.uniqueFloatValues = YColumn.uniqueFloatValues;
         YAgg.floatHistogram = YColumn.floatHistogram;
         YAgg.type = Typetype.FLOAT;
         YAgg.maxVal = YColumn.maxVal;
         YAgg.minVal = YColumn.minVal;
         
         String[] xlist = csv.data.getStringColumn(XColumn.index);
         float[]  ylist = csv.data.getFloatColumn (YColumn.index);
  
         ArrayList<AGG_S_F_PAIR> pairs = new ArrayList<AGG_S_F_PAIR>();
         for(int i = 0; i < csv.rowsize; i++)
         {
           pairs.add(new AGG_S_F_PAIR(xlist[i], ylist[i]));
         }
         Collections.sort(pairs,new ASFPCOMP());
         
         switch(aggMode)
         {
           case AVG:
           {
             
             int cur_element = 0;
             String start = ""; String end = "";
             float value = 0; //for now we will just do average, but depending on the aggregation method, how we compute this could change
             int ci = 0;
             int bucketsize = XAgg.bucketsize;
             for(AGG_S_F_PAIR pair : pairs)
             {
               if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
               {
                 //we've reached the end of the bucket, process the current element and put it in
                 end = pair.x;
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                 }
                 else
                 {
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                   else
                   {
                     bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   }
                   value /= XAgg.bucketsize;
                 }
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                 result = start;
                 else
                 result = start+"-"+end;
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementValues.append(value);
                 bucketsize = XAgg.bucketsize;
                 cur_element = 0;
                 value = 0;
                 ci++;
                 continue;
               }
               else if(cur_element == 0)
               {
                 if(Float.isNaN(pair.y))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                 start = pair.x;
                 value += pair.y;
                 cur_element++;
                 ci++;
                 continue;
               }
               if( !Float.isNaN(pair.y))
                 value += pair.y;
               else
               {
                 bucketsize --; if(bucketsize <= 0) bucketsize = 1;
               }
               cur_element++;
               ci++;
             }
             
           }
           break;

           case MODE:
           case MEDIAN:           
           case COUNT_AVG:
           {
             //first, start by initializing our list with all unique instances
             int i = 0;
             for(String s : XAgg.uniqueStringValues)
             {
               XAgg.aggElementNames.append(s);
               float val = 0;
               int j = 0;
               for(float f : ylist)
               {
                   if(xlist[j] == s  && !Float.isNaN(f))
                     val+= f;
                   
                   j++;  
               }
               val /= XAgg.stringHistogram.get(i);  //be sure to only divide it by the actual number of times it occurs. by definition, this must be AT LEAST ONCE
               XAgg.aggElementValues.append(val);
               i++;
             }
           }
           break;
           default:
           break;
         }
         
         
         
      }
    }
    else //XColumn type is FLOAT
    {
      XAgg.uniqueFloatValues = XColumn.uniqueFloatValues;
      XAgg.floatHistogram = XColumn.floatHistogram;
      XAgg.type = Typetype.FLOAT;
      XAgg.maxVal = XColumn.maxVal;
      XAgg.minVal = XColumn.minVal;
      
      
      if(YColumn.type == Typetype.STRING)
      {
        YAgg.uniqueStringValues = YColumn.uniqueStringValues;
        YAgg.stringHistogram = YColumn.stringHistogram;
        YAgg.type = Typetype.STRING;
        
       float[]  xlist = csv.data.getFloatColumn(XColumn.index);
       String[] ylist = csv.data.getStringColumn(YColumn.index);

       ArrayList<AGG_F_S_PAIR> pairs = new ArrayList<AGG_F_S_PAIR>();
       for(int i = 0; i < csv.rowsize; i++)
       {
         pairs.add(new AGG_F_S_PAIR(xlist[i], ylist[i]));
       }
       
        Collections.sort(pairs,new AFSPCOMP());
        
        //because strings arent computable on their own, and median itself is abit too arbitrary, we will use mode as our selection basis
        switch(aggMode)
        {
          case AVG:
          case COUNT_AVG:
          case MEDIAN:
          case MODE:
          {
             int cur_element = 0;
             String start = ""; String end = "";
             int ci = 0;
             int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
             IntList histogram = new IntList();
             for(int i = 0; i < YAgg.uniqueStringValues.size(); i++)
             {
               histogram.append(0);
             }
           
            for(AGG_F_S_PAIR pair:pairs)
            {
              if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
              {
                int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 end = str(pair.x).substring(0,len);
                 
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   int histi = 0;
                    for(String s : YAgg.uniqueStringValues)
                    {
                      if(s.equals(pair.y)) break;
                      histi++;
                    }
                    if(histi<histogram.size())
                    histogram.add(histi,1);
                    else
                    {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                 }
                 else
                 {
                   int histi = 0;
                  for(String s : YAgg.uniqueStringValues)
                  {
                    if(s.equals(pair.y)) break;
                    histi++;
                  }
                  if(histi<histogram.size())
                  histogram.add(histi,1);
                  else
                  {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                  
                 }
                 
                 //the histogram is now prepared, select the appropriate index
                 int selectedIndex =0;
                 int maxhist = max(histogram.array());
                 for(selectedIndex = 0; selectedIndex < histogram.size();selectedIndex++)
                 {
                   if(histogram.get(selectedIndex) == maxhist)break;
                 }
                 
                 String value = YAgg.uniqueStringValues.get(selectedIndex);
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementStringValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 ci++;
                 
                continue;
              }
              else if(cur_element == 0)
              {
                if(Float.isNaN(pair.x))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                start = str(pair.x).substring(0,len);
                
                int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                
                cur_element++;
                ci++;
                continue;
              }
              //take the current value and add it to the histogram
              int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
             
              ci++;
              cur_element++;
            }
          }
          break;
          default:
          break;
          
        }
          
      }
      else //YColumn type is FLOAT
      {
        YAgg.uniqueFloatValues = YColumn.uniqueFloatValues;
        YAgg.floatHistogram = YColumn.floatHistogram;
        YAgg.type = Typetype.FLOAT;
        YAgg.maxVal = YColumn.maxVal;
        YAgg.minVal = YColumn.minVal;
         
       float[]  xlist = csv.data.getFloatColumn(XColumn.index);
       float[] ylist = csv.data.getFloatColumn(YColumn.index); 
       
       ArrayList<AGG_F_F_PAIR> pairs = new ArrayList<AGG_F_F_PAIR>();
       for(int i = 0; i < csv.rowsize; i++)
       {
         pairs.add(new AGG_F_F_PAIR(xlist[i], ylist[i]));
       }
       Collections.sort(pairs,new AFFPCOMP());
       switch(aggMode)
       {
         case AVG:
         {
         
           int cur_element = 0;
           String start = ""; String end = "";
           float value = 0; //for now we will just do average, but depending on the aggregation method, how we compute this could change
           int ci = 0;
           int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
           for(AGG_F_F_PAIR pair : pairs)
             {
               if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
               {
                 //we've reached the end of the bucket, process the current element and put it in
                 int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 end = str(pair.x).substring(0,len);
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                 }
                 else
                 {
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                   else
                   {
                     bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   }
                   value /= XAgg.bucketsize;
                 }
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 value = 0;
                 ci++;
                 continue;
               }
               else if(cur_element == 0)
               {
                 if(Float.isNaN(pair.x) || Float.isNaN(pair.y))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                 int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 start = str(pair.x).substring(0,len);
                 value += pair.y;
                 cur_element++;
                 ci++;
                 continue;
               }
               if( !Float.isNaN(pair.y))
                 value += pair.y;
               else
               {
                 bucketsize --; if(bucketsize <= 0) bucketsize = 1;
               }
               cur_element++;
               ci++;
             }
           
         }
         break;

         
         case MODE:
         case MEDIAN:
         case COUNT_AVG:
         {
           //first, start by initializing our list with all unique instances
             int i = 0;
             for(float s : XAgg.uniqueFloatValues)
             {
               int len = (str(s).length() < 4 ? str(s).length() : 4);
               XAgg.aggElementNames.append(str(s).substring(0,len));
               float val = 0;
               int j = 0;
               for(float f : ylist)
               {
                 if(xlist[j] == s && !Float.isNaN(f))
                   val+= f;
                  
                 j++;
               }
               val /= XAgg.floatHistogram.get(i);  //be sure to only divide it by the actual number of times it occurs. by definition, this must be AT LEAST ONCE
               XAgg.aggElementValues.append(val);
               i++;
             }
         }
         break;
         
         default:
         break;
       } 
      }
    }
    
    //the y-axis in this case will depend largely on the aggregation mode, the result of the x-axis, the type arrangement of the 2 axes, and its own distribution
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  void GetNextCategoryColumn()
  {
    if(csv.hasCategoricalData)
    {
      category_csv_index = (category_csv_index+1)%csv.stringColumns.columns.size();
      categoryColumn = csv.stringColumns.getIthColumn(category_csv_index);
      println(categoryColumn.name);
    }
  }
  
  void setRect(int a, int b, int c, int d)
  {
    u0 = a; v0 = b; w = c; h = d; 
    frame_width = w - u0;
    frame_height = h - v0;
  }
  void drawFrame()
  {
    stroke(0);
    if(isSelected)
      fill(selectFill);
    else
      fill(255,0);
    rect(u0,v0,w,h);
  }
  
  void drawFrame(color col)
  {
    stroke(0);
    fill(col);
    rect(u0,v0,w,h);
  }
  
  void drawFrameText(color col, String str, int tsize)
  {
    stroke(0);
    fill(col);
    rect(u0,v0,w,h);
    textSize(tsize);
    fill(0);
    text(str,(u0 + w/2 - tsize*str.length()/4), v0 +h/2);
  }
  

  void drawGrid(CSVColumn X, CSVColumn Y)
  {
    fill(120);
    stroke(120,50);
    float xinterval = (X.maxVal /* + abs(X.minVal) */)/gridinterval;
    float yinterval = (Y.maxVal /* + abs(Y.minVal) */)/gridinterval;
    
    //draw y lines
    for(int i = 0; i < gridinterval; i++)
    {
       line(u0,v0+h-(i*yinterval)*(v0 + frame_height)/Y.maxVal,
          (2*u0+frame_width), v0+h-(i*yinterval)*(v0 + frame_height)/Y.maxVal);
     //line(transformX(0), transformY(i*yinterval),transformX(100),transformY(i*yinterval));
    }
    
    //draw x lines
    for(int i = 0; i < gridinterval; i++)
    {
      line(u0 + i*xinterval * (u0 + frame_width)/X.maxVal, v0+h,
           u0 + i*xinterval * (u0 + frame_width)/X.maxVal, v0+h-(v0 + frame_height));
      //line(transformX(i*xinterval),transformY(0),transformX(i*xinterval),transformY(100));
    }
  }

  void drawScaleMin(FrameScale scale)
  {
    if(!scale.showDetails) return;
    fill(0);
    //TODO(tj): make sure to process these through the switch case so that we have the right transformations
    float scalex = u0;
    float scaley = v0+h;
    float halfX = transformX(50);
    float halfY = transformY(50);
    float maxx = u0 + w - 5*str(scale.max).length()/2;
    float maxy = v0 + 20;
    
    switch(scale.direction)
    {
      case DOWN:
      {
        scaley = v0+h+20;
        maxy = halfY = scaley;
      }
      break;
      case UP:
      {
        scaley = v0 - 10;
        maxy = halfY = scaley;
      }
      break;
      case LEFT:
      {
        int len = (str(round(scale.max)).length() > str(round(scale.min)).length() ? str(round(scale.max)).length() : str(round(scale.min)).length());
        //println(len);
        scalex = u0 - 12*(len);
        maxx = halfX = scalex;
        
      }break;
      case RIGHT:
      {
        scalex = u0 + w + 10;
        maxx = halfX = scalex;
      }
      break;
      default:break;
    }
    
    textSize(textsize/3);
    //position the minimum
    text(str((int) scale.min), scalex, scaley);
    //position any halfway point
    if(scale.showHalfway)
    {
      text(str((int) scale.halfway), halfX, halfY); 
    }
    //position the max
    text(str((int) scale.max), maxx, maxy);
  }



  void drawScaleTicks(FrameScale scale)
  {
    if(!scale.showDetails || scale.max == scale.min) return;

    float interval = (scale.max - scale.min)/scale.divNumber;
    //make sure we dont go into an infinite loop
    if(interval <= 0)
      interval = 1;
    
    textSize(textsize/3);
    fill(0);
    stroke(0);  
    switch(scale.direction)
    {
      case DOWN:
      {
        float maxx = transformX(100);
        float scaley = transformY(0)+5;
        
        float currentPoint = scale.min;
        for(int i = 0; i < scale.divNumber; i++)
        {
          float width = transformX(currentPoint, scale.max, scale.min);
          String cpoint = str(currentPoint);
          int maxLength = (cpoint.length() < maxCharLength)?cpoint.length(): maxCharLength;
          
          text(cpoint.substring(0,maxLength), width, scaley + 25);
          line(width,scaley-2,width,scaley+12);

          currentPoint += interval;
          if(currentPoint > scale.max) currentPoint = scale.max;
        }
        
        String mpoint = str(scale.max);
        int maxLength = (mpoint.length() < maxCharLength)?mpoint.length(): maxCharLength;
        text(mpoint.substring(0,maxLength), maxx, scaley + 25);
        line(maxx,scaley-2,maxx,scaley + 12);
      }
      break;
      
      case UP:
      {
        float maxx = transformX(100);
        float scaley = transformY(100)-5;

        float currentPoint = scale.min;
        
        for(int i = 0; i < scale.divNumber; i++)
        {
          float width = transformX(currentPoint,scale.max,scale.min);
          String cpoint = str(currentPoint);
          int maxLength = (cpoint.length() < maxCharLength)?cpoint.length():maxCharLength;
          
          text(cpoint.substring(0,maxLength),width,scaley - 25);
          line(width, scaley+2,width,scaley-12);
          
          currentPoint += interval;
          if(currentPoint > scale.max) currentPoint = scale.max;
        }
        
        String mpoint = str(scale.max);
        int maxLength = (mpoint.length() < maxCharLength)?mpoint.length():maxCharLength;
        text(mpoint.substring(0,maxLength),maxx,scaley-25);
        line(maxx,scaley+2,maxx,scaley-12);
      }
      break;
      
      case LEFT:
      {
        float maxy = transformY(100);
        float scalex = transformX(0)-5;
        
        float currentPoint = scale.min;
        
        for(int i = 0; i < scale.divNumber; i++)
        {
          float height = transformY(currentPoint,scale.max,scale.min);
          
          String cpoint = str(currentPoint);
          int maxLength = (cpoint.length() < maxCharLength)? cpoint.length():maxCharLength;
          
          text(cpoint.substring(0,maxLength),scalex - 9*maxLength,height);
          line(scalex - 2, height, scalex-8, height);
          
          currentPoint += interval;
          if(currentPoint > scale.max) currentPoint = scale.max;
        }
        
        String mpoint = str(scale.max);
        int maxLength = (mpoint.length() < maxCharLength)?mpoint.length():maxCharLength;
        text(mpoint.substring(0,maxLength),scalex - 9*maxLength, maxy);
        line(scalex -2, maxy, scalex - 8, maxy);
      }
      break;
      
      case RIGHT:
      {
        float maxy = transformY(100);
        float scalex = transformX(100)+5;
        
        float currentPoint = scale.min;
        
        for(int i = 0; i < scale.divNumber; i++)
        {
          float height = transformY(currentPoint,scale.max,scale.min);
          
          String cpoint = str(currentPoint);
          int maxLength = (cpoint.length() < maxCharLength) ? cpoint.length():maxCharLength;
          
          text(cpoint.substring(0,maxLength),scalex+10,height);
          line(scalex +2,height,scalex+8,height);
          
          currentPoint += interval;
          if(currentPoint > scale.max) currentPoint = scale.max;
        }
        
        String mpoint = str(scale.max);
        int maxLength = (mpoint.length() < maxCharLength)?mpoint.length():maxCharLength;
        text(mpoint.substring(0,maxLength),scalex+10,maxy);
        line(scalex+2,maxy,scalex+8,maxy);
      }
      break;
    }
    
    strokeWeight(1);
  }
  
  void drawScaleInterval(FrameScale scale, AggregateColumn column, int size)
  {
      textSize(textsize/3);
      fill(0);
      stroke(0);
      
      switch(scale.direction)
      {
        case DOWN:{
          float gapX = w/(float)(size);
          for(int i = 0; i < size; i++)
          {
            String s = column.aggElementNames.get(i);
            String sprev = "";
            if(i-1 >= 0) sprev = column.aggElementNames.get(i-1);
            if(s.equals(sprev)) s = "^";
            int len = (s.length() < 15? s.length() : 15);
            pushMatrix();
            translate(gapX*(i)+u0+gapX/2,
                  v0+h  + textsize + len+9);
            rotate(-PI/2.0);
            text(s.substring(0,len),0,0);
            popMatrix();
          }
        } break;
        default:break;
      }
  }
  
  void drawScaleIntervalStrings(FrameScale scale, CSVColumn col)
  {
      textSize(textsize/3);
      fill(0);
      stroke(0);
      
      int stringsize = col.uniqueStringValues.size();
      
      switch(scale.direction)
      {
        case DOWN:{
          float gapX = w/(float)(stringsize+2);
          for(int i = 0; i < stringsize; i++)
          {
            String s = col.uniqueStringValues.get(i);
            int len = (s.length() < 7 ? s.length() : 6);
            pushMatrix();
            translate(gapX*(i+1)+u0+gapX/2,
                  v0+h  + textsize + len);
            rotate(-PI/2.0);
            text(s.substring(0,len),0,0);
            popMatrix();
          }
        }break;
        
        case UP: {
          float gapX = w/(float)(stringsize+2);
          for(int i = 0; i < stringsize; i++)
          {
            String s = col.uniqueStringValues.get(i);
            int len = (s.length() < 7 ? s.length() : 6);
            pushMatrix();
            translate(gapX*(i+1)+u0+gapX/2,
                  v0   - len);
            rotate(-PI/2.0);
            text(s.substring(0,len),0,0);
            popMatrix();
          }
        }break;
        case LEFT:{
          float gapY = h/(float)(stringsize+2);
          float scalex = transformX(0);
          for(int i = 0; i < stringsize; i++)
          {
            String s = col.uniqueStringValues.get(i);
            int len = (s.length() < 7 ? s.length() : 6);
            
            
            text(s.substring(0,len),
              scalex - textsize - len,
              -gapY*(i+1)-gapY/2+v0+h);
          }
        }break;
        case RIGHT:{
          float gapY = h/(float)(stringsize+2);
          float scalex = transformX(100);
          for(int i = 0; i < stringsize; i++)
          {
            String s = col.uniqueStringValues.get(i);
            int len = (s.length() < 7 ? s.length() : 6);
  
            text(s.substring(0,len),
              scalex + len,
              -gapY*(i+1)-gapY/2+v0+h);
          }
        }break;
      }
      
     
  }
  
  
  void drawScaleInterval(FrameScale scale,CSVColumn column, int size)
  {
      textSize(textsize/3);
      fill(0);
      stroke(0);
      
      
      switch(scale.direction)
      {
        case DOWN:
        { 
          float gapX = w/(float)csv.rowsize;
          for(int i = 0; i < csv.rowsize; i++)
          {
            String s = csv.data.getRow(i).getString(column.index);
            String sprev = "";
            if(i-1 >= 0) sprev = csv.data.getRow(i-1).getString(column.index);
            if(s.equals(sprev)) s = "^";
            int len = (s.length() < 7 ? s.length() : 6);
            pushMatrix();
            translate(gapX*i+u0+gapX/2,
                  v0+h  + textsize + len);
            rotate(-PI/2.0);
            text(s.substring(0,len),0,0);
            popMatrix();
          }
        }
        break;
        case UP:
        {
          float gapX = w/(float)csv.rowsize;
          for(int i = 0; i < csv.rowsize; i++)
          {
            String s = csv.data.getRow(i).getString(column.index);
            String sprev = "";
            if(i-1 >= 0) sprev = csv.data.getRow(i-1).getString(column.index);
            if(s.equals(sprev)) s = "^";
            int len = (s.length() < 7 ? s.length() : 6);
            pushMatrix();
            translate(gapX*i+u0+gapX/2,
                  v0  - len);
            rotate(-PI/2.0);
            text(s.substring(0,len),0,0);
            popMatrix();
          }
        }
        break;
        case LEFT:
        {
          float gapY = h/(float)csv.rowsize;
          float scalex = transformX(0);
          for(int i = 0; i < csv.rowsize; i++)
          {
             String s =csv.data.getRow(i).getString(column.index);
             String sprev = "";
            if(i-1 >= 0) sprev = csv.data.getRow(i-1).getString(column.index);
            if(s.equals(sprev)) s = "^";
             int len = (s.length() < 7 ? s.length() : 6);
            text(s.substring(0,len),
              scalex - textsize - len,
              -gapY*(i)-gapY/2+v0+h);
          }
        }
        break;
        case RIGHT:
        {
          float gapY = h/(float)csv.rowsize;
          float scalex = transformX(100);
          for(int i = 0; i < csv.rowsize; i++)
          {
            String s =csv.data.getRow(i).getString(column.index);
            String sprev = "";
            if(i-1 >= 0) sprev = csv.data.getRow(i-1).getString(column.index);
            if(s.equals(sprev)) s = "^";
             int len = (s.length() < 7 ? s.length() : 6);
            text(s.substring(0,len),
              scalex + len,
              -gapY*(i)-gapY/2+v0+h);
          }
        }
        break;
      }
      
     
  }

  //takes a coordinate in the screen space, and translates it relative to the frame
  int getPointX(int x) {return x - u0;}
  int getPointY(int y) {return y - v0;}
  
  //takes a desired coordinate and translates it accordingly (the default is 0 to 100)
  float transformX (float x)
  { 
    float temp = map(x, 0,100, min_border_margin+frame_border_offset, u0+frame_width -frame_border_offset);
    return u0 + temp;
  }
  
  float transformX (float x, int border_offset) 
  {
    float temp = map(x, 0,100, min_border_margin+border_offset, u0+frame_width-border_offset); 
    return u0 + temp;
  }
  
  float transformX (float x, float max, float min) 
  {
    float temp = map(x,min,max,min_border_margin+frame_border_offset, u0 +frame_width-frame_border_offset);
    return u0+temp;
  }
  
  float transformX (float x, float max,float min, int border_offset) 
  {
    float temp = map(x,min,max,min_border_margin+border_offset, u0 +frame_width-border_offset);
    return u0+temp;
  }
  
  float transformY (float y) 
  {
    float temp = map(y, 0,100, min_border_margin+frame_border_offset, v0+frame_height-frame_border_offset); 
    return v0 + h - temp;
  }
  
  float transformY (float y, int border_offset) {
    float temp = map(y, 0,100, min_border_margin+border_offset, v0+frame_height-border_offset);
    return v0+h-temp;
  }

  float transformY (float y, float max, float min) 
  {
    float temp = map(y,min,max, min_border_margin+frame_border_offset, v0+frame_height -frame_border_offset);
    return v0 + h - temp;
  }
  
  float transformY (float y,float max, float min, int border_offset) 
  {
    float temp = map(y,min,max, min_border_margin+border_offset, v0+frame_height -border_offset);
    return v0 + h - temp;
  }
  
  void reset()
  {
    //setup(Title.title,csv, XColumn, YColumn);
    xcsv_index = XColumn.index;
    ycsv_index = YColumn.index;
    
    XAxis.title = XColumn.name;
    XScale.scaleMode = (XColumn.type == Typetype.FLOAT ? ScaleMode.FLOAT_VALUE : ScaleMode.INTERVAL);
    XScale.max = XColumn.maxVal;
    XScale.min = XColumn.minVal;
    XScale.halfway = (XScale.max + XScale.min)/2;
    
    YAxis.title = YColumn.name;
    YScale.scaleMode = (YColumn.type == Typetype.FLOAT ? ScaleMode.FLOAT_VALUE : ScaleMode.INTERVAL);
    YScale.max = YColumn.maxVal;
    YScale.min = YColumn.minVal;
    YScale.halfway = (YScale.max + YScale.min)/2;
    
    
    //tooltip.setNames(XAxis.title, YAxis.title,"");
  }
  
  
  void setup(String _title,CSV _csv, CSVColumn _xc, CSVColumn _yc)
  {
    csv = _csv; 
    XColumn = _xc; 
    YColumn = _yc; 
    xcsv_index = _xc.index;
    ycsv_index = _yc.index;

    Title.title = _title;
    
    XAxis.title = _xc.name;
    XAxis.direction = Direction.DOWN;
    
    YAxis.title = _yc.name;
    YAxis.direction = Direction.LEFT;
    
    XScale.scaleMode = (_xc.type == Typetype.FLOAT ? ScaleMode.FLOAT_VALUE : ScaleMode.INTERVAL);
    XScale.direction = Direction.DOWN;
    
    XScale.max = _xc.maxVal;
    XScale.min = _xc.minVal;
    XScale.halfway = (XScale.max + XScale.min)/2;
    
    YScale.scaleMode = (_yc.type == Typetype.FLOAT ? ScaleMode.FLOAT_VALUE : ScaleMode.INTERVAL);
    YScale.max = _yc.maxVal;
    YScale.min = _yc.minVal;
    YScale.halfway = (YScale.max + YScale.min)/2;
    
    
    
    dotAlpha = clamp(map(csv.rowsize, 100, 3500,255,40), 40,255);
    
    if(csv.hasCategoricalData)
    {
      category_csv_index = 0;
      usingCategories = true;
      categoryColumn = csv.stringColumns.getIthColumn(category_csv_index);
    }
  }
  abstract void draw();
  void processClick(){}
  void processRelease(){}
  void processOver(){}
  
}






class FrameScale
{
  ScaleMode scaleMode = ScaleMode.FLOAT_VALUE;
  Direction direction = Direction.LEFT;
  float min = 0f;
  float max = 10f;
  float halfway = 5f;
  int divNumber = 10; 
  boolean showDetails = true;  //this will be used to determine if we want to show any interval data, or just min/max
  boolean showHalfway = false;
}
public enum ScaleMode
{
  FLOAT_VALUE,
  INTERVAL
}

class FrameTitle
{
  String title = "";
  int H_margin = 35;
  int V_margin = 35;
  int reduction = 8;
  int text_size = 16;
  Direction direction = Direction.UP;
  boolean show = true; 
  void draw(int u0, int v0, int w, int h)
  {
    draw(text_size,u0,v0,w,h);
  }
  void draw(int textsize, int u0, int v0, int w, int h)
  {
    if(!show) return;
    fill(50);
    textSize(textsize);
    switch(direction)
    {
      case UP:
      {
        text(title, u0 + w/2 - textsize*title.length()/reduction, v0 - V_margin);
      }
      break;
      case DOWN:
      {
        text(title, u0 + w/2 - textsize*title.length()/reduction, v0 + h + textsize + V_margin);
      }
      break;
      case LEFT:
      {
        pushMatrix();
        translate(u0 - H_margin, v0+ h/2 -V_margin);
        rotate(-PI/2.0);
        text(title,0,0);
        popMatrix();
      }
      break;
      case RIGHT:
      {
        pushMatrix();
        translate(u0 + w + H_margin, v0+ h/2 -V_margin);
        rotate(PI/2.0);
        text(title,0,0);
        popMatrix();
      }
      break;
    }
  }
}