//-----------
/*
*
*      CSV classes
*
*
*/
//-----------
import java.util.Set;
import java.util.HashSet;

class CSV
{
  String path = "";
  String name = "";
  int rowsize = 0;
  int columnsize = 0;
  Table data = null;
  boolean hasCategoricalData = false;  //this will be used to determine if we worry about rendering color types
  
  TypeColumnList floatColumns = new TypeColumnList();
  TypeColumnList stringColumns = new TypeColumnList();
  TypeColumnList allColumns = new TypeColumnList();

  //basic column get functions
  CSVColumn GetColumn(int i)
  {
    return allColumns.getIthColumn(i);
  }
  
  //given the current csvcolumn, determine what the next column should be
  CSVColumn GetNextColumnNoRepeat(CSVColumn _c)
  {
    int index = (_c.index+1)%columnsize;
    while(allColumns.getIthColumn(index).repeatedValues && index != _c.index)
    {
      index = (index + 1) % columnsize;
    }
    
     return allColumns.getIthColumn(index);
  }
  
  CSVColumn GetPrevColumnNoRepeat(CSVColumn _c)
  {
    int index = _c.index-1;
    if(index < 0) index = columnsize -1;
    while(allColumns.getIthColumn(index).repeatedValues && index != _c.index)
    {
      index--;
      if(index < 0) index = columnsize -1;
    }
    
     return allColumns.getIthColumn(index);
  }
  
  
  
  //column retrieval functions with an additional exclude column parameter (which can be useful for preventing XAxis=YAxis overlap
  CSVColumn GetNextColumn(CSVColumn _c, int skipIndex)
  {
    int peekIndex = (_c.index+1)%columnsize;
    if(peekIndex == skipIndex) peekIndex = (peekIndex+1)%columnsize;
    
    return allColumns.getIthColumn(peekIndex);
  }
  
  //note: optimize this later. for now we just want it to be correct
  CSVColumn GetPrevColumn(CSVColumn _c, int skipIndex)
  {
    int peekIndex = _c.index-1;
    if(peekIndex < 0) peekIndex = columnsize -1;
    if(peekIndex == skipIndex) peekIndex = peekIndex-1;
    if(peekIndex < 0) peekIndex = columnsize -1;
    return allColumns.getIthColumn(peekIndex);
  }
  
  
  //much simpler column retrieval functions if we dont care about repeated values
  CSVColumn GetNextColumn(CSVColumn _c)
  {
    return allColumns.getIthColumn((_c.index + 1)%columnsize);
  }
  CSVColumn GetPrevColumn(CSVColumn _c)
  {
    int newindex = _c.index -1;
    if(newindex < 0) newindex = columnsize -1;  //i know % should work here, but apparently it's not... and im not about to debug it
    return allColumns.getIthColumn(newindex);
  }
  
  
  float alpha = 260;
  
  //this is where we deal with special cases regarding csv headings. For example, treating any YEAR column as a string column is likely more ideal
  boolean TitleExceptions(int i )
  {
    String t = data.getColumnTitle(i);
    return t.equalsIgnoreCase("NAME") || t.equalsIgnoreCase("ID");//t.equalsIgnoreCase("YEAR")
  }
  
  
  void setupData(File file)
  {
     path = file.getAbsolutePath();
     name = file.getName();
     int pos = name.lastIndexOf(".");
     if (pos > 0) {
         name = name.substring(0, pos);
     }
     data = loadTable(path,"header");
     rowsize = data.getRowCount();
     columnsize = data.getColumnCount();
     
     //prepare categorization of the columns
     colorMode(HSB,360,100,100);
     for(int i = 0; i < columnsize; i++)
     {
       boolean repeatedValues = HasRepeatedValues(data.getStringColumn(i));
       //for float columns
       if(!Float.isNaN( data.getRow( 0 ).getFloat(i)) && !TitleExceptions(i))
       {
         floatColumns.add(i,data.getColumnTitle(i));
         
         //set inner details
         CSVColumn fc = floatColumns.getColumn(i);
         fc.type = Typetype.FLOAT;
         fc.maxVal = max(data.getFloatColumn(i));
         fc.minVal = min(data.getFloatColumn(i));
         fc.repeatedValues = repeatedValues;
         allColumns.add(i,fc);
         
         //get unique values
         FloatList values = new FloatList();
         for(float f : data.getFloatColumn(i))
         {
           if(!values.hasValue(f))
           {
             values.append(f);
             fc.floatHistogram.append(1);
           }
           else
           {
             //update histogram
             int i1 = 0;
             for(float f1: values)
             {
               if(f == f1)
               {
                 fc.floatHistogram.add(i1,1);
                 break;
               }
               i1++;
             }
           }
         }
        fc.uniqueFloatValues = values;
        
        int maxval = max(fc.floatHistogram.array());
        fc.modevalueindex = 0;
        for(int j : fc.floatHistogram)
        {
          if(j == maxval) break;
          fc.modevalueindex++;
        }
       }
       //for string columns
       else
       {
         stringColumns.add(i,data.getColumnTitle(i));
         CSVColumn sc = stringColumns.getColumn(i);
         //now get unique values
         StringList values = new StringList();
         IntList colorvalues = new IntList();
         for(String s : data.getStringColumn(i))
         {
           if(!values.hasValue(s))
           {
             values.append(s);
             sc.stringHistogram.append(1);
           }
           else
           {
             //update histogram
             int i1 = 0;
             for(String s1 : values)
             {
               if(s1.equals(s))
               {
                 sc.stringHistogram.add(i1,1);
                 break;
               }
               i1++;
             }
           }
         }
         
         for(int cci = 0; cci < values.size(); cci ++)
        {
          color ci = color(map(cci,0,values.size(),20,360),65,85,alpha);
          colorvalues.append(ci);
        }
        
        
        int maxval = max(sc.stringHistogram.array());
        sc.modevalueindex = 0;
        for(int j : sc.stringHistogram)
        {
          if(j == maxval) break;
          sc.modevalueindex++;
        }
        
        sc.type = Typetype.STRING;
        sc.uniqueStringValues = values;
        sc.colorValues = colorvalues;
        sc.repeatedValues = repeatedValues;
        allColumns.add(i,sc);
       }
     }
     colorMode(RGB,255);
     if(stringColumns.columns.size() > 0) hasCategoricalData = true;
  }
}




class TypeColumnList
{
  ArrayList<CSVColumn> columns = new ArrayList<CSVColumn>();
  IntList indices = new IntList();
  
  CSVColumn getColumn(int i){
    for(CSVColumn col : columns) if(i == col.index) return col;
    return null;//columns.get(0);//this feels like really bad practice, should I just return null and cause the program to fail?
  }
  
  CSVColumn getIthColumn(int i)
  {
    if(i >= 0 && i < columns.size()){ return columns.get(i);}
    else return null;//columns.get(0); //this feels like really bad practice, should I just return null and cause the program to fail?
  }
  
  boolean isEmpty(){return columns.size() > 0;}
  void add(int i, String name)
  {
    CSVColumn col = new CSVColumn();
    col.name = name;
    col.index = i;
    columns.add(col);
    indices.append(i);
  }
  
  void add(int i,CSVColumn col)
  {
    columns.add(col);
    indices.append(i);
  }
}


//aggregate helper classes (the pair is ALWAYS sorted by the first element)
class AFSPCOMP implements Comparator<AGG_F_S_PAIR>
{
  @Override
  public int compare(AGG_F_S_PAIR a, AGG_F_S_PAIR b){
    if(Float.isNaN(a.x) || Float.isNaN(b.x)) return -1;
    else
    return (a.x < b.x) ? -1 : (a.x==b.x) ? 0 : 1;
  }
}
class AGG_F_S_PAIR 
{
  float x; String y;
  AGG_F_S_PAIR(float s, String ss) { x = s; y = ss;}
  
}



class AFFPCOMP implements Comparator<AGG_F_F_PAIR>
{
  @Override
  public int compare(AGG_F_F_PAIR a, AGG_F_F_PAIR b){
      if(Float.isNaN(a.x) || Float.isNaN(b.x)) return -1;
    else
      return (a.x < b.x) ? -1 : (a.x==b.x) ? 0 : 1;
  }
}
class AGG_F_F_PAIR
{
  float x; float y;
  AGG_F_F_PAIR(float s, float ss) { x = s; y = ss;}
}

class ASSPCOMP implements Comparator<AGG_S_S_PAIR>
{
  @Override
  public int compare(AGG_S_S_PAIR a, AGG_S_S_PAIR b){
    return a.x.compareTo(b.x);
  }
}
class AGG_S_S_PAIR
{
  String x; String y;
  AGG_S_S_PAIR(String s, String ss) { x = s; y = ss;}
}

class ASFPCOMP implements Comparator<AGG_S_F_PAIR>
{
  @Override
  public int compare(AGG_S_F_PAIR a, AGG_S_F_PAIR b){
    return a.x.compareTo(b.x);
  }
}
class AGG_S_F_PAIR
{
  String x; float y;
  AGG_S_F_PAIR(String s, float ss) { x = s; y = ss;}
}

enum AggMode{ AVG, MODE, MEDIAN, COUNT_AVG }
class AggregateColumn
{
  Typetype type = Typetype.STRING;
  int bucketsize= 10;  //how many 'rows' would fit into each entry in this aggregate column?
  StringList aggElementNames = new StringList();  //for example, an aggregate element from 1992-2006 would contain the elements 1992,1993,1994...,2006 and so on for the entire list
  FloatList  aggElementValues = new FloatList();  //depending on the aggregation method, this list is matched with the list above, and its value corresponds to the aggregation for those elements
  StringList aggElementStringValues = new StringList();
  
  float maxVal = 0;
  float minVal = 0;
  
  StringList uniqueStringValues = new StringList();
  IntList stringHistogram = new IntList();
  
  FloatList uniqueFloatValues = new FloatList();
  IntList floatHistogram = new IntList();

}

class CSVColumn
{
  Typetype type = Typetype.STRING;
  boolean repeatedValues = false; //this is used to determine if the column is unique values only
  String name = "";
  int index = 0;
  float maxVal = 0;
  float minVal = 0;
  boolean showDetails = false;
  
  int modevalueindex = 0;
  //this corresponds only with the stringlist
  IntList colorValues = new IntList();
   
  StringList uniqueStringValues = new StringList();
  IntList stringHistogram = new IntList();
  
  //add the ability to consider aggregate data in the case of repeated v  FloatList uniqueFloatValues = new FloatList();
  FloatList uniqueFloatValues = new FloatList();
  IntList floatHistogram = new IntList();

  //current ideas: average, count
  //the new list we work on, the aggregate list, must be a unique value list, where the y value is the result of the aggregate function
  //do we also want to sort the aggregate list?
  //for that aggregate list, do we want to aggregate the columns themselves further so we have a more digestible number (in the event of too many x elements on screen)?
}