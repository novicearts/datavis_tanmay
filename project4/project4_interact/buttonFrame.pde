class buttonFrame extends frame
{
  boolean wasClicked = false;
  String text = "click me";
  int buttontextsize = 16;
  String actionName = "cmd";
  color fillColor = color(210);
  void draw()
  {
    stroke(0);
    drawFrameText(fillColor,text,buttontextsize);
  }
  
  @Override
  void processClick()
  {  fillColor = color(160);}
  
  @Override
  void processRelease()
  {
    fillColor = color(210);
    wasClicked = true;
  }
  

}