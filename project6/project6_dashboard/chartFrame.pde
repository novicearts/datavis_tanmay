//for now, this is the main class we use for the parallel chart

class chartFrame extends frame
{
  int title_margin = 30;
  int pointsize = 8;
  int maxElements = 45;
  int id = -1;
  
  //note: if i decide to allow charts to maintain different axes rather than the one selected by the scatter plot matrix, uncomment the aggregate lines
  
  CSVColumn XColumn = null;
  //AggregateColumn XAgg = null;
  int xcsv_index = 0;
  CSVColumn YColumn = null;
  //AggregateColumn YAgg = null;   //i dont think we need this but maybe in the future?
  int ycsv_index = 1;
  
   boolean showXAxis = true;
   boolean showYAxis = true;
  
  AggMode aggMode= AggMode.AVG;  //typically we will use average if dealing with a float column, and count if dealing with a string column...mileage on this varies
  
  
  void draw(){
    if(!drawFrame) return;
    stroke(0);
    if(drawBorder)
      drawFrame();
    
    //draw title
    Title.draw(textsize/2,u0,v0 + title_margin,w,h);
    

    //draw xaxis
    if(showXAxis)
    XAxis.draw(u0,v0,w,h);
    //draw yaxis
    if(showYAxis)
    YAxis.draw(u0,v0,w,h);
    

    tooltip.showType = usingCategories;
   if(!readyToRender) return;
    
    if(drawGridlines)
    drawGrid(XColumn,YColumn);
    
    fill(dotFill,dotAlpha);
    stroke(50);
    //draw the data
    chartDraw();
    
    //draw the tooltip
    if(drawToolTip)
    {
      chartToolTipDraw();
    }

}
  @Override void processClick()
  {
    println(Title.title);
  }
  @Override void reset(){}
  
  void chartMouseInteract()
  {
    //println(mouseX,mouseY,Title.title);
  }
  void chartDraw(){}
  void chartToolTipDraw(){}
  void setupColumns(int xcol, int ycol)
  {
    //let the subclass charts determine if they need to do more
    XColumn = app.csv.GetColumn(xcol);
    YColumn = app.csv.GetColumn(ycol);
  }
  
}