class lineChart extends chartFrame
{
  @Override void chartDraw()
  {
    AggregateColumn XAgg = app.csv.XAgg;
    if(csv.rowsize > maxElements)
        {
            if(XAgg.aggElementNames.size() == 0) return;
            
            int gapX = w/XAgg.aggElementNames.size();
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            float oldmargin = XScale.margin;
            XScale.margin = -10;
            drawScaleInterval(XScale,XAgg,XAgg.aggElementNames.size());
            XScale.margin = oldmargin;
            
          if(YColumn.type == Typetype.STRING)
          { 
            drawScaleIntervalStrings(YScale,YColumn);
            
            //draw the line segments
            for(int i = 0; i < XAgg.aggElementNames.size()-1; i++)
            {
              String value = XAgg.aggElementStringValues.get(i);
              String value2 = XAgg.aggElementStringValues.get(i+1);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney1 = -gapY*(valindex+1)-gapY/2+v0+h;
              
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value2.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney2 = -gapY*(valindex+1)-gapY/2+v0+h;
              if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
              
              stroke(0);
              if((app.selection.currentRowValue <= XAgg.aggElementEnds.get(i) && app.selection.currentRowValue >= XAgg.aggElementStarts.get(i)) || i == app.selection.currentAggID)
              fill(app.selection.selectionFill,dotAlpha);
              else
              fill(dotFill,dotAlpha);
              
              ellipse(linex2, liney2 , pointsize, pointsize);  
              //if(i==0)
              ellipse(linex1, liney1 , pointsize, pointsize);   
            }
          }
          else
          {
            //draw y scale
            drawScaleMin(YScale);
            
            //draw the line segments
            for(int i = 0; i < XAgg.aggElementNames.size()-1; i++)
            {
               
              float f = XAgg.aggElementValues.get(i);
              float liney1 = transformY(f, YScale.max,YScale.min);
              f = XAgg.aggElementValues.get(i+1);
              float liney2 = transformY(f, YScale.max,YScale.min);
              if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
            }
            
            //iterate over the number of elements
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              //if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
              float f = XAgg.aggElementValues.get(i);
              float liney = transformY(f, YScale.max,YScale.min);
               if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              noStroke();
   
              stroke(0);
              if((app.selection.currentRowValue <= XAgg.aggElementEnds.get(i) && app.selection.currentRowValue >= XAgg.aggElementStarts.get(i)) ||i == app.selection.currentAggID)
              fill(app.selection.selectionFill,dotAlpha);
              else
              fill(dotFill,dotAlpha);
              
              ellipse(linex, liney , pointsize, pointsize);    
            }
          }
            
        }else if(csv.rowsize > 1)
        {
          //for now, while we debug all drawing code for aggregate column, first start off by mimicking project 2. just use the test.csv files to check if at the very least the basic
          // bar chart and line chart code are still functional
          //note this seems to break down depending on the y-axis being a float or string. perhaps a compromise for now is to go ahead and allow interval math for string types and just allow positioning regardless
          
          //so basically, the y-axis will typecheck if we have a string or a float. if float, do usual scaling, otherwise break up the yaxis into intervals for each unique value, and plot all dots accordingly
          //if this works well enough,we'll apply it to the scatterplot too
          
          //for now, we will not deal with string for y columns. until mode and median are implemented
          
          drawScaleInterval(XScale,XColumn,csv.rowsize);
          
          if(YColumn.type == Typetype.STRING)
          {
            drawScaleIntervalStrings(YScale,YColumn);
            
            //because this size is small, we should just allow local assignment
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
             //draw the line segments
            for(int i = 0; i < csv.rowsize-1; i++)
            {
              String value = csv.data.getRow(i).getString(YColumn.index);
              String value2 = csv.data.getRow(i+1).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney1 = -gapY*(valindex+1)-gapY/2+v0+h;
              
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value2.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney2 = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney2 > v0+h || liney1 > v0+h) continue;
              float linex1 = gapX*i+u0+gapX/2;
              float linex2 = gapX*(i+1)+u0+gapX/2;
              line(linex1,liney1,linex2,liney2);  
              
              if(app.selection.currentRowID == i)
                fill(app.selection.selectionFill,dotAlpha);
              else{
               if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
              }
              
              ellipse(linex2, liney2 , pointsize, pointsize);  
              //if(i==0)
              ellipse(linex1, liney1 , pointsize, pointsize);   
            }
          }
          else
          {
              drawScaleMin(YScale); //lets test the scale interval
              //drawScaleInterval(YScale,YColumn,csv.rowsize);
             float gap = w/(float)csv.rowsize;
               //draw the line segments
              for(int i = 0; i < csv.rowsize-1; i++)
              {
                if(Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                stroke(0);
                fill(0);
                float liney1 = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float liney2 = transformY(csv.data.getRow(i+1).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex1 = gap*i+u0+gap/2;
                float linex2 = gap*(i+1)+u0+gap/2;
                line(linex1,liney1,linex2,liney2);
              }
              
              //iterate over the number of elements
              for(int i = 0; i < csv.rowsize; i++)
              {
                if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;/*transformX(gap*i,u0+w,u0);*/
                noStroke();
                
                
                if(app.selection.currentRowID == i)
                  fill(app.selection.selectionFill,dotAlpha);
                else{
                if(usingCategories)
                {
                  int curval = 0;
                  String strval = csv.data.getRow(i).getString(categoryColumn.index);
                  for(String str : categoryColumn.uniqueStringValues)
                  {
                    if(strval.equals(str))
                    break;
                    curval++;
                  }
                  fill(categoryColumn.colorValues.get(curval));
                }
                else
                fill(dotFill,dotAlpha);
                }
                
                ellipse(linex, liney , pointsize, pointsize);    
              }
              
             
            }
          }
  }
  
  @Override void chartToolTipDraw()
  {
    AggregateColumn XAgg = app.csv.XAgg;
     if(csv.rowsize > maxElements)
        {
            
          tooltip.showType = false;
          int radius = pointsize/2;
          if(XAgg.aggElementNames.size() == 0) return;
          if(YColumn.type == Typetype.STRING)
          { 

            int gapX = w/XAgg.aggElementNames.size();
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              String value = XAgg.aggElementStringValues.get(i);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
             
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
               tooltip.setData(xi,value,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              boolean inbounds = inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius);
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateAggID(i, XAgg.aggElementStarts.get(i), XAgg.aggElementEnds.get(i));
              }
            }
            
          }
          else
          {
            
            
            int gapX = w/XAgg.aggElementNames.size();
            int gapY = h/XAgg.aggElementNames.size();
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              float f = XAgg.aggElementValues.get(i);
              String yi =  str(XAgg.aggElementValues.get(i));
              float liney = transformY(f, YScale.max,YScale.min);
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " "+aggMode.toString(),"");
              boolean inbounds = inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius);
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateAggID(i, XAgg.aggElementStarts.get(i), XAgg.aggElementEnds.get(i));
              }
            }
          }
        }
        else if(csv.rowsize > 1)
        {
          if(YColumn.type == Typetype.STRING)
          { 
            int gapX = w/csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            int radius = pointsize/2;
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi =  csv.data.getRow(i).getString(XColumn.index);
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
             
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(usingCategories)
              {
                tooltip.setData(xi,value,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE",categoryColumn.name);
              }else{
                tooltip.setData(xi,value,"");
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              }
              boolean inbounds = inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius);
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateSelectionRowID(i,csv.data.getRow(i).getFloat(app.selection.currentXColumn));
              }
            }
          }
          else
          {
            float gap = w/(float)csv.rowsize;
            int radius = pointsize/2;
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi =  csv.data.getRow(i).getString(XColumn.index);
              String yi =  csv.data.getRow(i).getString(YColumn.index);
              if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;/*transformX(gap*i,u0+w,u0);*/
                
                
              if(usingCategories)
              {
                tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
              }else{
                tooltip.setData(xi,yi,"");
                tooltip.setNames(XAxis.title, YAxis.title,"");
              }
              tooltip.showType = usingCategories;
              boolean inbounds = inCircleBounds(mouseX,mouseY,(int)linex,(int)liney,radius);
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateSelectionRowID(i,csv.data.getRow(i).getFloat(app.selection.currentXColumn));
              }
            }
          }
        }
  }
}