enum SelectionMode
{
  none,
  single,
  multi
}

class SelectionModel
{
  int currentRowID = -1;
  int currentAggID = -1;
  float currentRowValue = 0f;
  float selectedAggregateXMin = 0f;
  float selectedAggregateXMax = 0f;
  SelectionMode mode = SelectionMode.none;
  
  boolean usingSelection = false;
  
  int lastTouchedChart = -1;
  
  
  color selectionFill = color(255,0,0);
  
  
  //these get updated when the splom is clicked
  int currentXColumn = -1;
  int currentYColumn = -1;
  
  
  void UpdateSelectionRowID(int rowid,float value)
  {
      currentRowID = rowid;
      if(!Float.isNaN(value))
      currentRowValue = value;
  }
  
  void UpdateAggID(int id, float min, float max)
  {
    currentAggID = id;
    selectedAggregateXMin = min;
    selectedAggregateXMax = max;
  }
  
  void UpdateSelection(float min, float max)
  {
    if(mode == SelectionMode.multi)
    {
      selectedAggregateXMin = min;
      selectedAggregateXMax = max;
    }
  }
  
  void clear()
  {
     currentRowID = -1;
     currentAggID = -1;
     currentRowValue = 0f;
     selectedAggregateXMin = 0f;
     selectedAggregateXMax = 0f;
     mode = SelectionMode.none;
    
     usingSelection = false;
    
     lastTouchedChart = -1;
  
  }
  
}