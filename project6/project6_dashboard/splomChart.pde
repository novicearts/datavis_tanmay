class splomChart extends chartFrame
{
  ArrayList<scatterPlot> charts = new ArrayList<scatterPlot>();
  void setupSplom()
  {
    //unlike previously, we will actually iterate through ALL columns. because now it may be necessary to do so
   int framewidthinterval = w/app.csv.columnsize;
   int frameheightinterval = h/app.csv.columnsize;
   //println("scrn dim: ", framewidthinterval,frameheightinterval);
   int framex = u0+20;
   int framey = v0 ;
   
   for(int i = 0; i < app.csv.columnsize; i ++)
   {
     for(int j = i; j < app.csv.columnsize; j++)
     {
       if(i != j)
       {
         scatterPlot plot = new scatterPlot();
         plot.setup("",app.csv);
         plot.drawToolTip = false;
         plot.showYAxis = false;
         plot.YAxis.direction = Direction.LEFT;
         plot.showXAxis = false;
         plot.showscales = false;
         plot.Title.title = "";
         plot.Title.direction = Direction.UP;
         
         plot.xcsv_index = i;
         plot.ycsv_index = j;
         framex = u0+20+(framewidthinterval)*i;
         framey = v0+ (frameheightinterval)*j;
         
         String ytitle = csv.data.getColumnTitle(j);
         String xtitle = csv.data.getColumnTitle(i);
         
         ytitle = ytitle.substring(0, (ytitle.length() > 6 ? 6 : ytitle.length()));
         xtitle = xtitle.substring(0, (xtitle.length() > 7 ? 7 : xtitle.length()));
         
         
         if(j-1 == i) 
           {
             plot.Title.title = xtitle;
           }
           if(i==0)
           {
             plot.YAxis.title = ytitle;
             plot.YAxis.V_margin = -15;
             plot.YAxis.H_margin = 0;
             plot.showYAxis = true;
           }
          plot.setupColumns(i, j);
          plot.XScale.min = plot.XColumn.minVal;
          plot.XScale.max = plot.XColumn.maxVal;
          plot.XScale.halfway = (plot.XScale.min+plot.XScale.max)/2;
          
          plot.YScale.min = plot.YColumn.minVal;
          plot.YScale.max = plot.YColumn.maxVal;
          plot.YScale.halfway = (plot.YScale.min+plot.YScale.max)/2;
         plot.pointsize = 2;
         plot.setRect(framex,framey,framewidthinterval,frameheightinterval);
         
         charts.add(plot);
       }
     }
   }
  }
  
  @Override void chartDraw()
  {
    for(scatterPlot plot : charts)
    {
      if(plot != null) 
      {
           plot.isSelected =
             (plot.xcsv_index == app.selection.currentXColumn &&
             plot.ycsv_index == app.selection.currentYColumn);
        plot.draw();
      }
    }
  }
  void resetCategories(CSVColumn categoryColumn)
  {
    for(scatterPlot plot : charts)
    {
      plot.categoryColumn = categoryColumn;
    }
  }
  
  @Override void processClick()
  {
    for(chartFrame chart:charts){chart.isSelected = false;}
    for(chartFrame chart : charts)
    {
      if(mouseX > chart.u0 && mouseX < chart.u0 + chart.w
        &&mouseY > chart.v0 && mouseY < chart.v0 + chart.h)
      {
       
  
        chart.isSelected = true;
        app.selection.currentXColumn = chart.xcsv_index;
        app.selection.currentYColumn = chart.ycsv_index;
        app.csv.BuildAggregateColumns(mainChart.maxElements,app.csv.GetColumn(app.selection.currentXColumn), app.csv.GetColumn(app.selection.currentYColumn), AggMode.AVG);
        for(int i = 1; i < app.charts.size()-2;i++)
        {
          chartFrame c = app.charts.get(i);
          c.setupColumns(app.selection.currentXColumn, app.selection.currentYColumn);
          c.XAxis.title = c.XColumn.name;
         // chart.XAxis.direction = Direction.DOWN;
          c.XAxis.V_margin = 15;
          
          if(i == 3) //if we know we have the scatter plot
          {
            c.XScale.min = c.XColumn.minVal;
            c.XScale.max = c.XColumn.maxVal;
            c.XScale.halfway = (c.XScale.min+c.XScale.max)/2;
            c.XScale.showHalfway = true;
            c.XScale.direction = Direction.DOWN;
          }
          
          c.YAxis.title = c.YColumn.name;
          c.YAxis.direction = Direction.LEFT;
          c.YAxis.H_margin = 20;
          
          
          c.YScale.min = c.YColumn.minVal;
          c.YScale.max = c.YColumn.maxVal;
          c.YScale.halfway = (c.YScale.min+c.YScale.max)/2;
          c.YScale.showHalfway = true;
        }
        return;
      }
    }
  }
}