//project6 - dashboard

enum APP_ACTION
{
  NONE,
  LOAD_FILE,
  HELP,
  MOVE_CAT,
  FLIP_AXIS
}
screen_details scr = new screen_details();
application_details app = new application_details();

chartFrame mainChart = null;
barChart barchart = null;
lineChart linechart = null;
scatterPlot scatterplot = null;
splomChart splom = null;
parallelChart parallel = null;

//-----------
/*
*
*      Processing Setup Code
*
*
*/
//-----------
void setup(){
  size(1200,800);
  scr.init("");
  app.init();
  selectInput("Select a csv data file to use","parseCSV");
}




void parseCSV(File csvfile)
{
 if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
 }else{
    
    app.csv.setupData(csvfile);
    scr.init("");
    app.selection.currentXColumn = 0;
    app.selection.currentYColumn = 1;
    
    int marginspace = 30;
    
    //setup the charts
    mainChart = app.AddChart(app.csv,"",scr.screen_margin,scr.screen_margin,scr.screen_width-scr.screen_margin, scr.screen_height);
    app.GetMainChart();
    app.mainChart.drawBorder = false;
    
    barchart = app.AddBarChart(app.csv,"",scr.screen_margin,scr.screen_margin,325,325);
    barchart.title_margin = 30;
    barchart.drawGridlines = true;
    barchart.drawHorizontal = true;
    barchart.XAxis.direction = Direction.UP;
    barchart.XScale.direction = Direction.DOWN;
    
    linechart = app.AddLineChart(app.csv,"",barchart.u0,barchart.v0+barchart.h+barchart.title_margin+marginspace,barchart.w,barchart.h);
    linechart.title_margin = 30;
    linechart.drawGridlines = true;
    linechart.drawHorizontal = true;
    linechart.XScale.direction = Direction.DOWN;
    linechart.showXAxis = false;
    
    scatterplot = app.AddScatterPlot(app.csv,"",linechart.u0+linechart.w+linechart.title_margin/2+marginspace,linechart.v0,barchart.w,barchart.h);
    scatterplot.title_margin = 30;
    scatterplot.drawGridlines = true;
    scatterplot.drawHorizontal = true;
    scatterplot.drawVertical = true;
    scatterplot.showYAxis = false;
    scatterplot.showXAxis = false;
    scatterplot.XAxis.direction = Direction.UP;
    scatterplot.XAxis.V_margin = -5;
    
    splom = app.AddSplomChart(app.csv,"",scatterplot.u0+scatterplot.w+barchart.title_margin/2+marginspace,linechart.v0,barchart.w,barchart.h);
    splom.setupSplom();
    splom.title_margin = 30;
    
    
    
    parallel = app.AddParallelChart(app.csv,"",scatterplot.u0,barchart.v0,2*barchart.w+barchart.title_margin/2+marginspace,barchart.h);
    parallel.setupParallel();
    
    app.AddButton(" ?",APP_ACTION.HELP,0,0,30);
    
    for(int i = 1; i < app.charts.size()-2;i++)
    {
      chartFrame chart = app.charts.get(i);
      chart.setupColumns(app.selection.currentXColumn, app.selection.currentYColumn);
      chart.XAxis.title = chart.XColumn.name;
     // chart.XAxis.direction = Direction.DOWN;
      chart.XAxis.V_margin = 15;
      
      if(i == 3) //if we know we have the scatter plot
      {
        chart.XScale.min = chart.XColumn.minVal;
        chart.XScale.max = chart.XColumn.maxVal;
        chart.XScale.halfway = (chart.XScale.min+chart.XScale.max)/2;
        chart.XScale.showHalfway = true;
        chart.XScale.direction = Direction.DOWN;
      }
      
      chart.YAxis.title = chart.YColumn.name;
      chart.YAxis.direction = Direction.LEFT;
      chart.YAxis.H_margin = 20;
      
      
      chart.YScale.min = chart.YColumn.minVal;
      chart.YScale.max = chart.YColumn.maxVal;
      chart.YScale.halfway = (chart.YScale.min+chart.YScale.max)/2;
      chart.YScale.showHalfway = true;
    }
    
    if(app.csv.hasCategoricalData)
    {
      app.categoryColumn = app.csv.stringColumns.columns.get(app.catindex);
      app.usingcategories = true;
      app.showLegend = true;
      if(app.csv.CategoryCount() > 1)
      app.AddButton("Next Category",APP_ACTION.MOVE_CAT,scr.screen_margin+scr.screen_width-200,0,30);
    }
    
     app.csv.BuildAggregateColumns(mainChart.maxElements,app.csv.GetColumn(app.selection.currentXColumn), app.csv.GetColumn(app.selection.currentYColumn), AggMode.AVG);
     app.isRunning= true;
 }
}
void reload()
{
  app.isRunning = false;
  app = new application_details();
  scr = new screen_details();
  setup();   
}



//-----------
/*
*
*      Processing Application Code
*
*
*/
//-----------

void mousePressed()
{
  for(chartFrame cf : app.charts)
  {
    if(cf.id != 0 && cf.id != 5 && cf.inBounds())
    {
      cf.processClick();
      app.selection.lastTouchedChart = cf.id;
      return;
    }
  }
  
  for(buttonFrame b : app.buttons)
  {
    if(b.inBounds())
    {
      b.processClick();
      app.currentButtonID = b.id;
      return;
    }
  }
    for(dragFrame d : app.drag_frames)
  {
     /*if(d.inBounds())
     {
       d.processClick();
       d.lastX = mouseX; d.lastY = mouseY;
       d.offsetX = mouseX - d.u0; d.offsetY = mouseY - d.v0;
       app.currentDragID = d.id;
       return;
     }
     else*/ if(inRectBounds(mouseX,mouseY, app.hotzones.get(d.id)-app.hotzoneradius,parallel.v0,app.hotzoneradius*2,parallel.h))
     {
       d.processClick();
       d.lastX = mouseX; d.lastY = mouseY;
       d.offsetX = d.w/2; d.offsetY = mouseY - d.v0;
       app.currentDragID = d.id;
       return;
     }
  }
  
}

void mouseReleased(){
  for(buttonFrame b : app.buttons)
  {
    if(app.currentButtonID == b.id)
    {
      b.processRelease();
      app.currentButtonID = -1;
      return;
    }
  }
  for(dragFrame d : app.drag_frames)
  {
    if(app.currentDragID == d.id)
    {
      d.processRelease();
      if(d.id ==app.hotzoneSwapID || app.hotzoneSwapID == -1)
      {
        d.u0 = app.hotzones.get(d.id);
        d.v0 = app.mainChart.v0;
      }
      else
      {
        //swap the relevant data first, then snap both pieces to their new homes
        int swapid = app.hotzoneSwapID;
        int tempid = d.id;
        int tempu0 = app.hotzones.get(tempid);
        int tempv0 = app.mainChart.v0;
        //swap the positions and frame ids first
        dragFrame swapframe = app.drag_frames.get(app.hotzoneSwapID);
        d.id = swapframe.id;
        swapframe.id = tempid;
        
        d.u0 = swapframe.u0;
        d.v0 = tempv0;
        swapframe.u0 = tempu0;
        swapframe.v0 = tempv0;
        
        //then swap their list locations
        Collections.swap(app.drag_frames,swapid,tempid);
        
      }
      app.currentDragID = -1;
      app.hotzoneDragID = -1;
      app.hotzoneSwapID = -1;
      return;
    }
  }
}
void mouseDragged(){
  for(buttonFrame b : app.buttons)
  {
    if(!b.inBounds() && b.id == app.currentButtonID)
    {
      b.justRelease();
      app.currentButtonID = -1;
      return;
    }
  }
  for(dragFrame d : app.drag_frames)
  {
    if(d.id == app.currentDragID)
    {
      d.processDrag(mouseX,mouseY);
      for(int i =0; i < app.hotzones.size();i++)
      {
        //if(i == d.id) continue;
        if(d.u0 < app.hotzones.get(i)+app.hotzoneradius && 
           d.u0 > app.hotzones.get(i)-app.hotzoneradius &&
          app.hotzoneSwapID != i)
        {
          app.hotzoneDragID = d.id;
          app.hotzoneSwapID = i;
          //println("swap "+d.text+" with "+app.drag_frames.get(i).text+"?");
        }
      }
    }
  }
}
void keyPressed()
{
  switch(key)
  {
    case '/':
    {
      handleAction(APP_ACTION.HELP);
    }
    break;
    case TAB:
    case ' ':
    case 'g':
    case 'G':
    case CODED:
    {
      if(keyCode == SHIFT){}
      switch(keyCode)
      {
        case UP:
        case DOWN:
        case LEFT:
        case RIGHT:
        default:break;
      }
    }
    break;
    default:break;
  }
  
}

void handleAction(APP_ACTION action)
{
  //note: almost ALL actions will affect the main chart, so grab a reference to it now!
  switch(action)
  {
    case LOAD_FILE:
    {
      reload();
    }
    break;
    case HELP:
    {
      app.showHelp = !app.showHelp;
    }
    break;
    case MOVE_CAT:
    {
      app.getNextCategory();
      splom.resetCategories(splom.categoryColumn);
    }
    break;
    case FLIP_AXIS:
    {
      app.drag_frames.get(app.affectid).flipAxis = !app.drag_frames.get(app.affectid).flipAxis;
      app.affectid = -1;
    }
    break;
    default:
    {
      println(action.toString());
    }break;
  }
}


void draw()
{
  if(app.isRunning)
  {
    background(255);
    fill(0);
    
    //draw the frames
    for(frame chart : app.charts)
    {
      if(chart != null)
      {
        chart.draw();
      }
    }
    {
    int id = -1;
    for(chartFrame chart : app.charts)
    {
      
      if(chart != null && chart.id != 0 && chart.inBounds())
      {
        app.selection.lastTouchedChart = id = chart.id;
        chart.chartMouseInteract();
        break;
      }
    }
      switch(id)
      {
        case 1:
        {
          if(app.csv.rowsize > barchart.maxElements) app.selection.mode = SelectionMode.multi;
          else app.selection.mode = SelectionMode.single;
        }break;
        case 2:
        {
          if(app.csv.rowsize > linechart.maxElements) app.selection.mode = SelectionMode.multi;
          else app.selection.mode = SelectionMode.single;
        }break;
        case 3:
        case 5:
        {
          app.selection.mode = SelectionMode.single;
        }break;
        default: app.selection.clear(); break;
      }
    }
    
    for(buttonFrame b : app.buttons)
    {
      if(b != null)
        b.draw();
    }
    for(dragFrame d : app.drag_frames)
    {
      if(d != null)
      {
        d.draw();
     
        if(d.id == app.hotzoneSwapID)
        {
          fill(100,100);
          rect(app.hotzones.get(d.id)-app.hotzoneradius, parallel.v0, app.hotzoneradius*2, parallel.h);
        }
      }
    }
    parallel.HandleParallel();
    //any additional logic
    //check if any of the buttons were pressed
    for(buttonFrame b : app.buttons)
    {
      if(b.wasClicked)
      {
        //handle the button and clear it
        app.currentAction = b.action;
        b.wasClicked = false;
        
        if(b.affectorid != -1)
        {
          //this button does something too!
          app.affectid = b.affectorid;
        }
        
        break;
      }
    }
    
    //if action string is non-empty, handle it and then clear it
    if(app.currentAction != APP_ACTION.NONE)
    {
      handleAction(app.currentAction);
      app.currentAction = APP_ACTION.NONE;
    }
    
    if(app.showLegend)
    {
      app.drawLegend();
    }
    
    if(app.showHelp)
    {
      fill(180);
      rect(scr.screen_margin,scr.screen_margin,scr.screen_width,scr.screen_height);
      fill(0);
      text(app.helptext,scr.screen_margin*2,scr.screen_margin*2);
    }
  }
}