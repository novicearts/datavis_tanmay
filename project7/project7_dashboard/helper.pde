
//-----------
/*
*
*      Helper functions
*
*
*/
//-----------
import java.util.Set;
import java.util.HashSet;
boolean HasRepeatedValues(String[] array)
{
  Set<String> foundElements = new HashSet<String>();
  for( String s : array)
  {
    if(foundElements.contains(s))
    {
      return true;
    }
    foundElements.add(s);
  }
  return false;
}

float clamp(float val,float min, float max)
{
  return min(max, max(min,val));
}

boolean inRectBounds( int x, int y, int u0, int v0, int w, int h)
{
  return  x > u0 && x < u0 + w
        &&y > v0 && y < v0 + h;
}
boolean inCircleBounds(int x, int y, int sx, int sy, int radius)
{
  return sq(x - sx)/(radius*radius) + sq(y - sy)/(radius*radius) < 1 ;
}
boolean inEllipseBounds(int x, int y, int sx, int sy, int radiusA, int radiusB)
{
  return sq(x - sx)/(radiusA*radiusA) + sq(y - sy)/(radiusB*radiusB) < 1 ;
}

float mean(float[] x){
  float mean = 0f;
  for(float f:x)
  {
    if(Float.isNaN(f)) f = 0; //should i just fail the calculation?
    mean += f/x.length;
  }
  return mean;
}

float covariance(float[] x, float[]y,float xb, float yb)
{
  float cv = 0;
  if(x.length != y.length) return -999f;
  for(int i = 0; i < x.length; i++)
  {
    if(Float.isNaN(x[i])) x[i] = 0; //should i just fail the calculation?
    if(Float.isNaN(y[i])) y[i] = 0; //should i just fail the calculation?
    cv += ((x[i] - xb)*(y[i] - yb))/x.length;
  }
  return cv;
}

float stddev(float[] x,float mean){
  float variance = 0f;
  for(float f:x)
  {
    if(Float.isNaN(f)) f = 0; //should i just fail the calculation?
    variance += (f*f - mean*mean)/x.length;
  }
  return sqrt(variance);
}

float pearsonCorrelation(float[] x, float[] y){
  
  float xb = mean(x);
  float yb = mean(y);
  return covariance(x,y,xb,yb)/(stddev(x,xb)*stddev(y,yb));
}

int getRank(float[] x,float y){
  int i = 0;
  float fprev = -99f;
  for(float f:x)
  {
    if(f == y) break;
    if(fprev !=f)
      i++;
     
    fprev = f;
  }
  return i;
}

float[] rank(float[] x)
{
  FloatList xsorted = new FloatList(x);
  xsorted.sort();
  float[] xs = xsorted.array();
  float[] ranks = new float[x.length];
  for(int i = 0; i < ranks.length;i++)
  {
    ranks[i] = getRank(xs,x[i]);
  }
  return ranks;
}

float spearmanCorrelation(float[] x, float[]y)
{
  float[] xp = rank(x);
  float[] yp = rank(y);
  return pearsonCorrelation(xp,yp);
}