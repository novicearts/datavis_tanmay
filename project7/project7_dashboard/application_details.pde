

class application_details
{
  boolean isRunning = false;
  boolean showLegend = false;
  ArrayList<chartFrame> charts = new ArrayList<chartFrame>();
  chartFrame mainChart = null;
  ArrayList<buttonFrame> buttons = new ArrayList<buttonFrame>();
  ArrayList<dragFrame> drag_frames = new ArrayList<dragFrame>();
  boolean redraw = false;
  
  int linealpha = 80;
  int affectid = -1;
  int highlighted_row = -1;
  int highlightMoveID = 0;
  int highlightX = 0;
  int highlightY = 0;
  
  CSVColumn categoryColumn = null;
  boolean usingcategories = false;
  int catindex = 0;
  
  IntList hotzones = new IntList();  //these are used for drag frame positioning
  int hotzoneradius = 15;
  int dragradius = 6;
  
  APP_ACTION currentAction = APP_ACTION.NONE;
  int currentButtonID = -1;
  int currentDragID = -1;
  
  //note: when a swap occurs, the actual data lists involved will reorder themselves, but the hotzone data will not, that remains constant
  //the same reordering logic will always apply regardless of the order of the data (as they will always be based on position in array)
  int hotzoneDragID = -1;
  int hotzoneSwapID = -1;
  
  KeyboardMode keymode = KeyboardMode.ATTRIBUTES;
  
  boolean showHelp = false;
  String helptext = 
        "Hi there!\n" +
        "This is a dashboard that features the 5 basic plots we've done in class so far\n"+
        "Simply mouse over a data point in one of the charts that interests you to see it link with the other charts.\n"+
        "To use the parallel Axis chart, simply click and drag near the bar, and move it to the place you want to swap.\n"+
        "If you click on a box in the scatterplot matrix, you can swap out the columns you want to look at on the other charts of the dashboard.\n"+
        "If you click in the histogram charts, it will show you the mean and standard deviation for the selected attribute.\n"+
        "Mouse over any cell in the corrgram to see its corresponding cell and the value of the coefficients. \nThe Bottom half of the diagonal contains the Spearman coefficients, while the Upper half contain the Pearson coefficients.\n"+
        "In the corrgram, the coefficient values ranges from blue to red in a divergent color map scheme. Blue is the value -1 and red is the value 1";
  //the drag frames keep track of the relevant columns for us
  //public ArrayList<CSVColumn> axes = new ArrayList<CSVColumn>();
  
  CSV csv = null;
  SelectionModel selection = null;
  void init()
  {
    csv = new CSV();
    selection = new SelectionModel();
  }
  
  void getNextCategory()
  {
    catindex = (catindex+1)%csv.stringColumns.columns.size();
    categoryColumn = csv.stringColumns.columns.get(catindex);
    for(int i = 1; i < charts.size(); i++)
    {
      charts.get(i).categoryColumn = categoryColumn;
      
    }
  }
  
  chartFrame AddChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    chartFrame chart = new chartFrame();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  barChart AddBarChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    barChart chart = new barChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  barChart SetBarChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    barChart chart = new barChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    return chart;
  }
  
  histogramChart AddHistogramChart(CSV csv, String name, int u0, int v0, int w, int h)
  {
    histogramChart chart = new histogramChart();
    chart.setRect(u0,v0,w,h);
    
    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
    chart.drawToolTip = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  corrgramChart AddCorrgramChart(CSV csv, String name, int u0, int v0, int w, int h)
  {
    corrgramChart chart = new corrgramChart();
    chart.setRect(u0,v0,w,h);
    
    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
    chart.drawToolTip = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  lineChart AddLineChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    lineChart chart = new lineChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
   lineChart SetLineChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    lineChart chart = new lineChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    return chart;
  }
  
  
  scatterPlot AddScatterPlot(CSV csv, String name,int u0, int v0, int w, int h)
  {
    scatterPlot chart = new scatterPlot();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  } 
  
  splomChart AddSplomChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    splomChart chart = new splomChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  parallelChart AddParallelChart(CSV csv, String name,int u0, int v0, int w, int h)
  {
    parallelChart chart = new parallelChart();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(name,csv);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    chart.id = charts.size();
    charts.add(chart);
    return chart;
  }
  
  
  
  
  void GetMainChart()
  {
    if(charts.size() > 0) mainChart = charts.get(0);
    else 
    {
      charts.add(new chartFrame());
      mainChart = charts.get(0);
      mainChart.drawFrame = false;
    }
  }
  buttonFrame AddButton(String text, APP_ACTION action, int u0, int v0, int buttonheight)
  {
    buttonFrame button = new buttonFrame();
    button.action = action;
    button.text = text;
    button.id = buttons.size();
    int buttonwidth = button.buttontextsize*(int)(button.text.length()/1.5)+10;

    button.setRect(    u0,
                       v0,
                       buttonwidth,buttonheight);
    buttons.add(button);
    return button; 
  }
  
  dragFrame AddDrag(String text, APP_ACTION action, int u0, int v0, int buttonheight)
  {
    dragFrame drag = new dragFrame();
    drag.action = action;
    drag.text = text;
    drag.id = drag_frames.size();
    int buttonwidth = drag.buttontextsize*(int)(drag.text.length()/1.5)+10;
    drag.setRect(  u0,v0,buttonwidth,buttonheight);
    drag_frames.add(drag);
    return drag;
  }
  
  dragFrame AddDrag(String text,APP_ACTION action, int u0, int v0, int buttonwidth, int buttonheight)
  {
    dragFrame drag = new dragFrame();
    drag.action = action;
    drag.text = text;
    drag.id = drag_frames.size();
    drag.setRect(  u0,v0,buttonwidth,buttonheight);
    drag_frames.add(drag);
    return drag;
  }
  
  void drawLegend()
  {
    String max = categoryColumn.name;
    max = max.substring(0, (max.length() >14 ? 14 : max.length())) + (max.length() > 14 ? "." : "");
    String legend = max+":";
    text(legend, mainChart.u0+mainChart.w +5, 20);
    for(int i = 0; i < categoryColumn.uniqueStringValues.size(); i++)
      {
        max = categoryColumn.uniqueStringValues.get(i);
        max = max.substring(0, (max.length() > 7 ? 7 : max.length())) + (max.length() > 7 ? "." : "");
        int x =  mainChart.u0+mainChart.w +5;
        int y = 20+20*(i+1);
        fill(0);
        text(max, x, y);
        noStroke();
        fill(mainChart.categoryColumn.colorValues.get(i));
        ellipse( x+ 60, y-2, mainChart.pointsize/2,mainChart.pointsize/2);
      }
  }
}