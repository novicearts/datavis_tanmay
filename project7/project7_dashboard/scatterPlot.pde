class scatterPlot extends chartFrame
{
  boolean showscales = true;
  int pointsize = 5;
  
  @Override void chartDraw()
  {
    
    if(showscales)
    {
      drawScaleMin(YScale);
      drawScaleMin(XScale);
    }
    for(int i = 0; i < csv.rowsize; i++)
        {
          int selectedpointsize = pointsize;
          if(!Float.isNaN( csv.data.getRow(i).getFloat(XColumn.index)) && !Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) 
          {
            
            float scatterwidth = transformX(csv.data.getRow(i).getFloat(XColumn.index),XScale.max,XScale.min);
            float scatterheight =transformY(csv.data.getRow(i).getFloat(YColumn.index),YScale.max,YScale.min);
            float ival = csv.data.getRow(i).getFloat(app.selection.currentXColumn);
            if(app.selection.currentRowID == i || (!Float.isNaN(ival) && ival <= app.selection.selectedAggregateXMax && ival >= app.selection.selectedAggregateXMin))
            {
                fill(app.selection.selectionFill,dotAlpha);
                selectedpointsize *= 2;
            }
            else{
            if(usingCategories)
            {
              int curval = 0;
              String strval = app.csv.data.getRow(i).getString(categoryColumn.index);
              for(String str : categoryColumn.uniqueStringValues)
              {
                if(strval.equals(str))
                break;
                curval++;
              }
              fill(categoryColumn.colorValues.get(curval));
            }
            else
            fill(dotFill,dotAlpha);
              }
              
            noStroke();
            ellipse(scatterwidth,scatterheight,selectedpointsize,selectedpointsize);
          }
        }
  }
  
  @Override void chartToolTipDraw()
  {
    int radius = pointsize/2;
        for(int i = 0; i < csv.rowsize; i++)
        {
          String xi =  csv.data.getRow(i).getString(XColumn.index);
          String yi =  csv.data.getRow(i).getString(YColumn.index);
          if(!Float.isNaN( csv.data.getRow(i).getFloat(XColumn.index)) && !Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) 
          {
            float scatterwidth = transformX(csv.data.getRow(i).getFloat(XColumn.index),XScale.max,XScale.min);
            float scatterheight =transformY(csv.data.getRow(i).getFloat(YColumn.index),YScale.max,YScale.min);
            
            if(usingCategories)
            {
              tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
              tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
            }else{
              tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title, YAxis.title,"");
            }
            tooltip.showType = usingCategories;
            boolean inbounds = inCircleBounds(mouseX,mouseY,(int)scatterwidth,(int)scatterheight,radius);
            tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
            if(showscales && inbounds)
            {
              app.selection.UpdateSelectionRowID(i,csv.data.getRow(i).getFloat(app.selection.currentXColumn));
            }
          }
        }
  }
}