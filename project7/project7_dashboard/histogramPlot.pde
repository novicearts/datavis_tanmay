class histogramPlot extends chartFrame
{
  boolean isStringColumn = false;
  boolean shouldDraw = false;
  int binsize = 10;
  int columnid = -1;
  float maxVal;
  float minVal;
  int maxCount = 0;
  IntList histogram = new IntList();
  StringList uniqueStrings = new StringList();
  float mean = 0f;
  int meanbin = 0;
  float stddev = 0f;
  int stddevbin1 = 0;
  int stddevbin2 = 0;
  void calcHistogram()
  {
    mean = 0f;
    stddev = 0f;
    if(isStringColumn)
    {
      histogram = new IntList();
      for(String i : uniqueStrings) histogram.append(0);
      for(String s: app.csv.data.getStringColumn(columnid))
      {
        int i = 0;
        for(String x : uniqueStrings)
        {
          if(x.equals(s))break;
          i++;
        }
        histogram.add(i,1);
      }
    }
    else
    {
       histogram = new IntList();
       for(int i = 0; i < binsize; i++) histogram.append(0);
       for(float f: app.csv.data.getFloatColumn(columnid))
      {
        mean+=f/app.csv.rowsize;
        if(f <= minVal)
        {
          histogram.add(0,1);
        }
        else if(f >= maxVal)
        {
          histogram.add(binsize-1,1);
        }
        else
        {
          int b = int((f - minVal) / (maxVal - minVal) * binsize);
          histogram.add(b,1);
        }
      }
      meanbin = int((mean - minVal) / (maxVal - minVal) * binsize);
      for(float f: app.csv.data.getFloatColumn(columnid))
      {
        stddev += (f*f - mean*mean)/app.csv.rowsize;
      }
      stddev = sqrt(stddev);
      stddevbin1 = int(((mean - stddev) - minVal) / (maxVal - minVal) * binsize);
      stddevbin2 = int(((mean + stddev) - minVal) / (maxVal - minVal) * binsize);
    }
    maxCount = max(histogram.array());
    YScale.min = 0;
    YScale.max = maxCount;
    
  }
  
  void setupColumn(){
    isStringColumn = app.csv.allColumns.columns.get(columnid).type == Typetype.STRING;
    XAxis.title = app.csv.allColumns.columns.get(columnid).name;
    
    if(isStringColumn)
    {
      uniqueStrings = app.csv.allColumns.columns.get(columnid).uniqueStringValues;
      uniqueStrings.sort();
    }
    else
    {
       maxVal = app.csv.allColumns.columns.get(columnid).maxVal;
       minVal = app.csv.allColumns.columns.get(columnid).minVal;
    }
  }
  
  
  
  
  
  @Override void chartDraw()
  {
    
    drawScaleMin(YScale);
    
    int gapX = w/histogram.size();
    if(isStringColumn)
    {
      for(int i = 0; i < histogram.size(); i++)
      {
        
        float liney = transformY(histogram.get(i), YScale.max,YScale.min);
        float linex = gapX*i+u0+gapX/2;
        if(isSelected)
        fill(200,0,200,100);
        else
        fill(0,200,200,100);
        rect(linex-gapX/2,liney,gapX,v0+h-liney);
      }
    }
    else
    {
      for(int i = 0; i < histogram.size(); i++)
      {
        float liney = transformY(histogram.get(i), YScale.max,YScale.min);
        float linex = gapX*i+u0+gapX/2;
        if(isSelected)
        fill(200,0,200,100);
        else
        fill(0,200,200,100);
        rect(linex-gapX/2,liney,gapX,v0+h-liney);
        
        if(isSelected){
          if(meanbin == i)
          {
            stroke(0,200,0,240);
            line(linex,v0+h,linex,v0);
          }
          if(stddevbin1  == i || stddevbin2 == i)
          {
            stroke(0,0,200,240);
            line(linex,v0+h,linex,v0);
          }
          
         stroke(0); 
        }
      }
    }
  }
  
  
  @Override void chartToolTipDraw()
  {
    if(isStringColumn)
    {
      
    }
    else
    {
    }
  }
}