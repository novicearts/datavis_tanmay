class histogramChart extends chartFrame
{
  ArrayList<histogramPlot> plots = new ArrayList<histogramPlot>();
  int startingindex = 0;
  void setupHistogram()
  {
    
    int x = 0;
    int y = 0;
    for(int i = 0; i < 4; i++)
    {
      histogramPlot plot = new histogramPlot();
      switch(i){
        case 0: x = 0; y = 0;break;
        case 1: x = 1; y = 0;plot.YScale.direction = Direction.RIGHT;break;
        case 2: x = 0; y = 1; break;
        case 3: x = 1; y = 1;plot.YScale.direction = Direction.RIGHT;break;
        default:break;
      }
      
      plot.setRect(u0+(w/2*x),v0+(h/2*y),w/2,h/2);
      plot.columnid = i;
      plot.setupColumn();
      plot.calcHistogram();
      plot.XAxis.direction = Direction.UP;
      plot.XAxis.V_margin = -10;
      
      app.AddButton("+ ",APP_ACTION.CHANGE_BINSIZE_P, (x==0 ? u0+5 : u0+w/2),  (y==0? v0-20 : v0+h),20).affectorid = i;
      app.AddButton("- ",APP_ACTION.CHANGE_BINSIZE_M, (x==0 ? u0+w/2-35 : u0+w-20),  (y==0? v0-20 : v0+h),20).affectorid = i;
      
      plots.add(plot);
    }
  }
  
  void cycleHistogram()
  {
    startingindex += 4;
    if(startingindex > app.csv.allColumns.columns.size()) startingindex = 0;
    for(int i = 0; i < 4; i++)
    {
      histogramPlot plot = plots.get(i);
      plot.columnid = (startingindex + i)%app.csv.allColumns.columns.size();
      plot.setupColumn();
      plot.calcHistogram();
      
    }
  }
  
  void changeBinSize(int i,int amount)
  {
    plots.get(i).binsize += amount;
    if(plots.get(i).binsize < 0) plots.get(i).binsize = 1;
    plots.get(i).calcHistogram();
  }
  
  @Override void chartDraw()
  {
   for(histogramPlot plot : plots)
    {
      if(plot != null) 
      {
           plot.isSelected =
             (plot.columnid == app.selection.currentHistColumn);
        plot.draw();
      }
    }
  }
  
  
  
  @Override void chartToolTipDraw()
  {
  }
  
  @Override void processClick()
  {
    for(chartFrame chart:plots){chart.isSelected = false;}
     for(histogramPlot chart : plots)
    {
      if(mouseX > chart.u0 && mouseX < chart.u0 + chart.w
        &&mouseY > chart.v0 && mouseY < chart.v0 + chart.h)
      {
       
  
        chart.isSelected = true;
        app.selection.currentHistColumn = chart.columnid;
        println(app.csv.allColumns.columns.get(chart.columnid).name);
        return;
      }
    }
  }
}