class corrgramChart extends chartFrame
{
  ArrayList<FloatList> corrgramMatrix = new ArrayList<FloatList>();
  void setupCorrgram()
  {
    //setup matrix
    
    for(int i = 0; i < app.csv.allColumns.columns.size(); i++)
    {
      corrgramMatrix.add(new FloatList());
      for(int j = 0; j < app.csv.allColumns.columns.size(); j++)
      {
        corrgramMatrix.get(i).append(0);
      }
    }
    for(int i = 0; i < app.csv.allColumns.columns.size(); i++)
    {
      for(int j = 0; j < app.csv.allColumns.columns.size(); j++)
      {
        if(i == j)
        {
          corrgramMatrix.get(i).set(j,1);
        }
        else if(app.csv.allColumns.getIthColumn(i).type == Typetype.FLOAT &&
                app.csv.allColumns.getIthColumn(j).type == Typetype.FLOAT)
        {
          //now calculate the correlation
          
          //if i > j, use pearson
          if(i > j)
          {
            corrgramMatrix.get(i).set(j, pearsonCorrelation( app.csv.data.getFloatColumn(i), app.csv.data.getFloatColumn(j) ));
          }
          //if i < j, use spearman
          else
          {
            corrgramMatrix.get(i).set(j, spearmanCorrelation( app.csv.data.getFloatColumn(i), app.csv.data.getFloatColumn(j) ));
          }
        }
        else
        {
          //literally any other column combination cannot reasonably yield a correlation, FS, SS, nor SF
          continue;
        }
      }
    }
  }
  
    
  @Override void chartDraw()
  {
    int colcount = app.csv.allColumns.columns.size();
    int rw = w/colcount;
    int rh = h/colcount;
   //try to draw the matrix
   for(int i = 0; i <  colcount; i++)
   {
     for(int j = 0; j <  colcount; j++)
     {
       int bx =u0+rw*i;
       int by =v0+rh*j; 
       color cellcolor = 0;
       float value = corrgramMatrix.get(i).get(j);
       if(i == j)
       {
         fill(0);
         //label the corresponding axes
         text(app.csv.data.getColumnTitle(i),bx+rw/16,by+rh/2);
         fill(255);
       
       }
       else
       {
         //color lerp - divergent from red to blue
         
         
         int brightness = 80;
         if(value == 0f)
         {
           colorMode(RGB);
           cellcolor = color(128);
         }
         else if(value < 0f)
         {
           colorMode(HSB,360,100,100);
           cellcolor = color(225,map(value,0,-1,5,100),brightness);
         }
         else if(value > 0f)
         {
           colorMode(HSB,360,100,100);
           cellcolor = color(7,map(value,0,1,5,100),brightness);
         }
         colorMode(RGB,255,255,255);
       }
       if(i != j)
       {
         boolean isselected =   (((i == app.selection.selectedi && j == app.selection.selectedj) ||(j == app.selection.selectedi && i == app.selection.selectedj)) && i!= j);
         if(isselected)
         {
           stroke(0,255,0);
           strokeWeight(2);
         }
         colorMode(HSB,360,100,100);
         fill(cellcolor);
         colorMode(RGB,255,255,255);
         rect(bx,by,rw,rh);
         fill(255);
         
         
         //draw the ellipse by default, and show text when highlighted
         if(isselected){
         text(str(corrgramMatrix.get(i).get(j)),bx+rw/16,by+rh/2);
         }
         else
         {
           //if spearman
           if(j > i)
           {
             fill(0,0);
             stroke(255);
             pushMatrix();
             translate(bx+rw/2,by+rh/2);
             rotate(map(value,-1,1,-PI/4.0,PI/4.0));
             ellipse(0,0,(rw-5)*(1-abs(value)),rw-5);
             popMatrix();
           }
           //if pearson
           else
           {
             fill(0,0);
             stroke(255);
             float leftpos = map(value,-1,1,by,by+rh);
             float rightpos = map(value,1,-1,by,by+rh);
             //we need a way to map the slant
             line(bx, leftpos, bx+rw, rightpos);
             /*
             pushMatrix();
             translate(bx,by);
             rotate(map(value, -1,1, PI/4.0,-PI/4.0));
             
             rectMode(CENTER);
             rect(rw/2,rh/2,rw-10,1);
             rectMode(CORNER);
             
             popMatrix();
             */
             

             
           }
         }
         strokeWeight(1);
         
       }
       
       
       stroke(0);
     }
   }
   
  }
  
  
  
  @Override void chartToolTipDraw()
  {
    int colcount = app.csv.allColumns.columns.size();
    int rw = w/colcount;
    int rh = h/colcount;
         
    for(int i = 0; i <  colcount; i++)
     {
       for(int j = 0; j <  colcount; j++)
       {
         int bx =u0+rw*i;
         int by =v0+rh*j; 
         float spearval = corrgramMatrix.get(j).get(i);
         float pearsval = corrgramMatrix.get(i).get(j);
         tooltip.setData(str(spearval),str(pearsval),"");
         tooltip.setNames("PEARSON", "SPEARMAN",app.csv.data.getColumnTitle(i)+" x "+ app.csv.data.getColumnTitle(j));
         boolean inbounds = inRectBounds(mouseX,mouseY,bx,by,rw, rh)&& i!=j;
         tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
         //setup and draw the tooltip if you want to
         if(inbounds)
         {
           app.selection.selectedi = i;
           app.selection.selectedj = j;
         }
       }
     }
  }
  
   @Override void processClick()
  {
    
  }
}