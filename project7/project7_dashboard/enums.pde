//-----------
/*
*
*      enums
*
*/
//-----------

public enum Direction
{
  UP,
  DOWN,
  LEFT,
  RIGHT,
  CENTER
}

public enum Typetype
{
  STRING,
  INT,
  FLOAT,
  MULTI
}

//atm i cant think of many uses for color, so I'm leaving it like this for project 2
enum ColorMode{
  NONE,
  HIGHLIGHT,
  HIGHLIGHT_AND_LINE,
  MAKE_BRIGHT,
  GRADIENT
}

enum ChartType{
  BAR ,
  LINE,
  SCATTER
};

enum KeyboardMode
{
  ATTRIBUTES,
  SCALES
}