class buttonFrame extends frame
{
  boolean wasClicked = false;
  int id = -1;
  String text = "click me";
  int affectorid = -1;  //this is an additional parameter we can use if needed
  int buttontextsize = 16;
  APP_ACTION action = APP_ACTION.NONE;
  color fillColor = color(210);
  void draw()
  {
    stroke(0);
    drawFrameText(fillColor,text,buttontextsize);
  }
  
  @Override
  void processClick()
  {  fillColor = color(160);}
  
  @Override
  void processRelease()
  {
    fillColor = color(210);
    wasClicked = true;
  }
  
  void justRelease()
  {
    fillColor = color(210);
  }
  

}