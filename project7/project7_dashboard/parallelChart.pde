class parallelChart extends chartFrame
{
  void setupParallel()
  {
    int colcount = app.csv.allColumns.columns.size();
    float gapX = w/(float)(colcount);
    app.hotzoneradius = (int)gapX/2;
    int i = 0;
    
    //setup the axes for dragging
    for(CSVColumn col : app.csv.allColumns.columns)
    {
      //app.axes.add(col);
      int hotzone = u0 + (int)gapX*i+(int)gapX/2;
      app.hotzones.append(hotzone);
      dragFrame drag = app.AddDrag(col.name,APP_ACTION.NONE, hotzone ,v0, 4 ,h+3);
      drag.minX = u0;
      drag.minY = v0;
      drag.maxX = u0+w;
      drag.maxY = v0 + h+3;
      drag.column = col;
      drag.textDirection = Direction.DOWN;
      drag.topVal = col.maxVal;
      drag.bottomVal = col.minVal;
      //add a flip button
      
        buttonFrame button = app.AddButton("Flip",APP_ACTION.FLIP_AXIS,hotzone-60 ,drag.maxY,30);
        button.affectorid = i;
      
      i++;
    }
  }
  
  @Override void processClick()
  {
    println("PARALLEL");
  }
  void HandleParallel()
  {
    //if mouse is obviously outside of the frame, clear the highlighted row
    if(app.currentDragID == -1)
    {
      if(!inBounds())
      {
        app.highlighted_row = -1;
      }
       else
       {
         //determine where we should highlight
       
         //determine which hotzone we're in
         int hotzone = 0;
         for(int hzone : app.hotzones)
         {
           if(mouseX > hzone-app.hotzoneradius && mouseX < hzone+app.hotzoneradius) {break;}
           hotzone++;
         }
         if(hotzone < app.drag_frames.size()-1)
         {
           //prepare the things we need
           dragFrame next = app.drag_frames.get(hotzone+1);
           int curx = app.hotzones.get(hotzone);
           int nextx = app.hotzones.get(hotzone+1);
           app.drag_frames.get(hotzone).determineHighlight(next,curx,nextx,this);
         }
         else if(hotzone < app.drag_frames.size())
         {
           dragFrame next = app.drag_frames.get(hotzone);
           int curx = app.hotzones.get(hotzone-1);
           int nextx = app.hotzones.get(hotzone);
           app.drag_frames.get(hotzone-1).determineHighlight(next,curx,nextx,this);
         }
       }
    }
    //deal with any edge cases regarding the last drag frame
    {
      app.drag_frames.get(app.drag_frames.size()-1).flipAxis();
    }
    
    //now draw the data
    for(int i = 0; i < app.drag_frames.size()-1;i++)
    {
      dragFrame d = app.drag_frames.get(i);
      dragFrame d1 = app.drag_frames.get(i+1);
      d.drawLines(d1,this);  
      
    }
    
    
    
    //draw any final details
    for(dragFrame d : app.drag_frames)
    {
      d.drawDetails(this);
    }
    
    //draw mouse details
    if(inBounds())
    {
      if(app.highlighted_row != -1)
      {
        stroke(255,0,0,255);
        strokeWeight(2);
        int x = app.drag_frames.get(app.highlightMoveID).u0 + app.drag_frames.get(app.highlightMoveID).w/2;
        line(mouseX,mouseY,x,app.highlightY);
        strokeWeight(1);
        stroke(0);
      }
    }
    
    //update selection
    if(app.highlighted_row != -1)
    {
      app.selection.UpdateSelectionRowID(app.highlighted_row, app.csv.data.getRow(app.highlighted_row).getFloat(app.selection.currentXColumn));
    }
  }
}