class barChart extends chartFrame
{
  @Override void chartDraw()
  {
    AggregateColumn XAgg = app.csv.XAgg;
    if(csv.rowsize > maxElements)
    {
      if(XAgg.aggElementNames.size() == 0) return;
      int gapX = w/XAgg.aggElementNames.size();
      int gapY = h/XAgg.aggElementNames.size();
      drawScaleInterval(XScale,XAgg,XAgg.aggElementNames.size());
      
      if(YColumn.type == Typetype.STRING)
      {
        drawScaleIntervalStrings(YScale,YColumn);
      }
      else
      {
        drawScaleMin(YScale);
        for(int i = 0; i < XAgg.aggElementNames.size();i++)
        {
          if( Float.isNaN( XAgg.aggElementValues.get(i))) break;
          float liney = transformY(XAgg.aggElementValues.get(i), YScale.max,YScale.min);
          float linex = gapX*i+u0+gapX/2;
   
              if((app.selection.currentRowValue <= XAgg.aggElementEnds.get(i) && app.selection.currentRowValue >= XAgg.aggElementStarts.get(i)) || app.selection.currentAggID == i)
              fill(app.selection.selectionFill,dotAlpha);
              else
              fill(dotFill,dotAlpha);      
          
          rect(linex-gapX/2,liney,gapX,v0+h-liney);
        }
      }
       
    }
    else if(csv.rowsize > 1)
    {
      if(YColumn.type == Typetype.STRING)
      {
        drawScaleIntervalStrings(YScale,YColumn);
            drawScaleInterval(XScale,XColumn,csv.rowsize);
            
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            for(int i = 0; i < csv.rowsize; i++)
            {
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(app.selection.currentRowID == i)
                fill(app.selection.selectionFill,dotAlpha);
              else
              {
                  
                  if(usingCategories)
                    {
                      int curval = 0;
                      String strval = csv.data.getRow(i).getString(categoryColumn.index);
                      for(String str : categoryColumn.uniqueStringValues)
                      {
                        if(strval.equals(str))
                        break;
                        curval++;
                      }
                      fill(categoryColumn.colorValues.get(curval));
                    }
                    else
                    fill(dotFill,dotAlpha);
              }
                
               //rect(linex, h+v0-liney/2 ,gap-15, liney);
               rect(linex-gapX/2,liney,gapX,v0+h-liney);
            }
      }
      else
      {
        drawScaleMin(YScale);
        drawScaleInterval(XScale,XColumn,csv.rowsize);
        float gap = w/(float)csv.rowsize;
        for(int i = 0; i < csv.rowsize;i++)
        {
          if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
            float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
            float linex = gap*i+u0+gap/2;
            if(app.selection.currentRowID == i)
                fill(app.selection.selectionFill,dotAlpha);
            else
            {
            if(usingCategories)
            {
              int curval = 0;
              String strval = csv.data.getRow(i).getString(categoryColumn.index);
              for(String str : categoryColumn.uniqueStringValues)
              {
                if(strval.equals(str))
                break;
                curval++;
              }
              fill(categoryColumn.colorValues.get(curval));
            }
            else
            fill(dotFill,dotAlpha);
            }
            
           //rect(linex, h+v0-liney/2 ,gap-15, liney);
           rect(linex-gap/2,liney,gap,v0+h-liney);
        }
      }
    }
  }
  
  
  @Override void chartToolTipDraw()
  {
    AggregateColumn XAgg = app.csv.XAgg;
    if(csv.rowsize > maxElements)
        {
          tooltip.showType = false;
          if(XAgg.aggElementNames.size() == 0) return;
            
            int gapX = w/XAgg.aggElementNames.size();
            int gapY = h/XAgg.aggElementNames.size();
            
          if(YColumn.type == Typetype.STRING)
          { 
          }
          else
          {
            for(int i = 0; i < XAgg.aggElementNames.size(); i++)
            {
              String xi =  XAgg.aggElementNames.get(i);
              float f = XAgg.aggElementValues.get(i);
              String yi =  str(XAgg.aggElementValues.get(i));
              float liney = transformY(f, YScale.max,YScale.min);
              if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
                
                fill(dotFill,dotAlpha);
                
               tooltip.setData(xi,yi,"");
              tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              boolean inbounds = inRectBounds(mouseX,mouseY,(int)(linex-gapX/2),(int)liney,(int)gapX, (int)(v0+h - liney));
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                  app.selection.UpdateAggID(i, XAgg.aggElementStarts.get(i), XAgg.aggElementEnds.get(i));
              }
            }
          }
        }
        else if(csv.rowsize > 1)
        {
          if(YColumn.type == Typetype.STRING)
          { 
            float gapX = w/(float)csv.rowsize;
            float gapY = h/(float)(YColumn.uniqueStringValues.size()+2);
            
            for(int i = 0; i < csv.rowsize; i++)
            {
              String xi = csv.data.getRow(i).getString(XColumn.index);
              String value = csv.data.getRow(i).getString(YColumn.index);
              int valindex = 0;
              for(valindex = 0; valindex < YColumn.uniqueStringValues.size();valindex++)
              {
                if(value.equals(YColumn.uniqueStringValues.get(valindex))) break;
              }
              float liney = -gapY*(valindex+1)-gapY/2+v0+h;
              //if(liney > v0+h) continue;
              float linex = gapX*i+u0+gapX/2;
              
              if(usingCategories)
              {
                tooltip.setData(xi,value,csv.data.getRow(i).getString(categoryColumn.index));
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE",categoryColumn.name);
              }else{
                tooltip.setData(xi,value,"");
                tooltip.setNames(XAxis.title + " range", YAxis.title+ " MODE","");
              }
                
              tooltip.showType = usingCategories;
              boolean inbounds = inRectBounds(mouseX,mouseY,(int)(linex-gapX/2),(int)liney,(int)gapX, (int)(v0+h - liney));
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateSelectionRowID(i,csv.data.getRow(i).getFloat(app.selection.currentXColumn));
              }
            }
          }
          else
          {
            //boolean inRectBounds( int x, int y, int u0, int v0, int w, int h)
            float gap = w/(float)csv.rowsize;
            for(int i = 0; i < csv.rowsize;i++)
            {
              if( Float.isNaN( csv.data.getRow(i).getFloat(YColumn.index))) break;
                float liney = transformY(csv.data.getRow(i).getFloat(YColumn.index), YScale.max,YScale.min);
                float linex = gap*i+u0+gap/2;
                
                String xi = csv.data.getRow(i).getString(XColumn.index);
                String yi = csv.data.getRow(i).getString(YColumn.index);
                
               if(usingCategories)
                {
                  tooltip.setData(xi,yi,csv.data.getRow(i).getString(categoryColumn.index));
                  tooltip.setNames(XAxis.title, YAxis.title,categoryColumn.name);
                }else{
                  tooltip.setData(xi,yi,"");
                  tooltip.setNames(XAxis.title, YAxis.title,"");
                }
              tooltip.showType = usingCategories;
              boolean inbounds = inRectBounds(mouseX,mouseY,(int)(linex-gap/2),(int)liney,(int)gap, (int)(v0+h - liney));
              tooltip.draw(inbounds,
                         mouseX,mouseY,w,h,textsize,true,false);
              if(inbounds)
              {
                app.selection.UpdateSelectionRowID(i,csv.data.getRow(i).getFloat(app.selection.currentXColumn));
              }
            }
          }
        }
  }
}