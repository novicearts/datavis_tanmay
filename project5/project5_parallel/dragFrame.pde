class dragFrame extends frame
{
  boolean wasClicked = false;
  String text = "dragger";
  int id = -1;
  int buttontextsize = 10;
  APP_ACTION action = APP_ACTION.NONE;
  color fillColor = color(50);
  
  Direction textDirection = Direction.DOWN;
  
  int lastX = 0;
  int lastY = 0;
  int offsetX = 0;
  int offsetY = 0;
  
  int textX = 0;
  int textY = 0;
  
  CSVColumn column;
  boolean flipAxis = false;
  
  //bounded rect variables?
  int minX = 0;
  int minY = 0;
  int maxX = width;
  int maxY = height;
  
  float topVal = 0;
  float bottomVal = 0;
  
  void draw()
  {
    stroke(0);
    switch(textDirection)
    {
      case DOWN:
        if(flipAxis)
          drawFrameText(color(255,0,0),text/*+" "+str(id)*/,buttontextsize,u0- text.length(),v0+h+40);
        else
          drawFrameText(fillColor,text/*+" "+str(id)*/,buttontextsize,u0- text.length(),v0+h+40);
      break;
      default:
        drawFrameText(fillColor,text,buttontextsize);
      break;
    }
    
  if(column.type == Typetype.FLOAT)
  {
   //note:if we want to do +/- direction swapping, we'll need a more dynamic version of this
   if(flipAxis)
   {
     text("min: "+str(topVal),u0-text.length(),v0-10);
     text("max: "+str(bottomVal),u0-text.length(),v0+h+20);
   }
   else
   {
     text("max: "+str(topVal),u0-text.length(),v0-10);
     text("min: "+str(bottomVal),u0-text.length(),v0+h+20);
   }
  }
    
  }
  
  @Override
  void processClick()
  {  fillColor = color(160);}
  
  @Override
  void processRelease()
  {
    fillColor = color(50);
    wasClicked = true;
  }
  
  void processOver()
  { fillColor = color(100);}
  
  void justRelease()
  {
    fillColor = color(50);
  }
  
 
   void flipAxis()
   {
     if(flipAxis)
    {
      topVal = column.minVal;
      bottomVal = column.maxVal;
    }
    else
    {
      topVal = column.maxVal;
      bottomVal = column.minVal;
    }
   }

   void drawLines(dragFrame nextframe)
  {
    flipAxis();
    if(app.csv.rowsize <= 1) return;
    for(int i = 0; i < app.csv.rowsize; i++)
    {
      int alpha = app.linealpha;
      color c = color(0,200,200,app.linealpha);
      if(app.usingcategories)
      {
        String catvalue = app.csv.data.getRow(i).getString(app.categoryColumn.index);
        int catvalindex = app.categoryColumn.uniqueStringValues.index(catvalue);
        c = app.categoryColumn.colorValues.get(catvalindex);
      }
      if(i == app.highlighted_row)
      {
        strokeWeight(2);
        c = color(255,0,0);
        alpha = 255;
      }
      if(column.type == Typetype.FLOAT)
      {
        float d0 = app.csv.data.getRow(i).getFloat(column.index);
        
        
        if(nextframe.column.type == Typetype.FLOAT)
        {
          float d1 = app.csv.data.getRow(i).getFloat(nextframe.column.index);
          if(Float.isNaN(d0) || Float.isNaN(d1)) continue;
          //for now, test with a single line
          int x0 = u0+w/2;//app.hotzones.get(i);
          int y0 = (int)map(d0,
                       bottomVal,
                       topVal,
                       app.mainChart.v0+app.mainChart.h,
                       app.mainChart.v0);
                       
          int x1 = nextframe.u0+nextframe.w/2;
          int y1 = (int)map(d1,
                       nextframe.bottomVal,
                       nextframe.topVal,
                       app.mainChart.v0+app.mainChart.h,
                       app.mainChart.v0);
          stroke(c,alpha);
          line(x0,y0,x1,y1);  
        }
        else if(nextframe.column.type == Typetype.STRING)
        {
          int strindex = nextframe.column.uniqueStringValues.index(app.csv.data.getRow(i).getString(nextframe.column.index));
          int x0 = u0+w/2;
          int y0 = (int)map(d0,
                       bottomVal,
                       topVal,
                       app.mainChart.v0+app.mainChart.h,
                       app.mainChart.v0);
          int stringsize = nextframe.column.uniqueStringValues.size();
          float gapY = app.mainChart.h/(float)(stringsize+2);
          
          int x1 = nextframe.u0+nextframe.w/2;
          int y1 = (int)(-gapY*strindex  -gapY +app.mainChart.v0 +app.mainChart.h);
          
          stroke(c,alpha);
          line(x0,y0,x1,y1);  
          
        }
      }
      else if(column.type == Typetype.STRING)
      {
        int dstr = column.uniqueStringValues.index(app.csv.data.getRow(i).getString(column.index));
        int x0 = u0+w/2;
        int stringsize = column.uniqueStringValues.size();
        float gapY = app.mainChart.h/(float)(stringsize+2);
        int y0 = (int)(-gapY*dstr  -gapY +app.mainChart.v0 +app.mainChart.h);
        
        if(nextframe.column.type == Typetype.FLOAT)
        {
          float d1 = app.csv.data.getRow(i).getFloat(nextframe.column.index);
          if(Float.isNaN(d1)) continue;
          int x1 = nextframe.u0+nextframe.w/2;
          int y1 = (int)map(d1,
                       nextframe.bottomVal,
                       nextframe.topVal,
                       app.mainChart.v0+app.mainChart.h,
                       app.mainChart.v0);
          stroke(c,alpha);
          line(x0,y0,x1,y1);
          
        }
        else if(nextframe.column.type == Typetype.STRING)
        {
          int strindex = nextframe.column.uniqueStringValues.index(app.csv.data.getRow(i).getString(nextframe.column.index));
          
          stringsize = nextframe.column.uniqueStringValues.size();
          gapY = app.mainChart.h/(float)(stringsize+2);
          
          int x1 = nextframe.u0+nextframe.w/2;
          int y1 = (int)(-gapY*strindex  -gapY +app.mainChart.v0 +app.mainChart.h);
          
          stroke(c,alpha);
          line(x0,y0,x1,y1);
        }
        
      }
      strokeWeight(1);
    }
    
  }


  
  void drawDetails()
  {
    if(column.type == Typetype.STRING)
    {
      int stringsize = column.uniqueStringValues.size();
      float gapY = app.mainChart.h/(float)(stringsize+2);
      for(int i = 0; i < stringsize;i++)
      {
        String val = column.uniqueStringValues.get(i);
        int x = u0-text.length();
        int y = (int)(-gapY*i  -gapY +app.mainChart.v0 +app.mainChart.h);
        fill(240,160);
        int maxlen = (val.length() < 8 ? val.length() : 8);
        rect(x,y-10,(maxlen+1)*8,15);
        fill(0);
        text(val.substring(0,maxlen),x,y);
                        
      }
    }
  }

  
   void determineHighlight(dragFrame next,int x1, int x2)
  {
    if(column.type == Typetype.STRING) return;  //due to vague nature of the string column, we will not try to determine highlight
    float x0 = mouseX;
    float y0 = mouseY;
    int min_row_id = 0;
    float curminvalue = 999999999;
    for(int i = 0; i < app.csv.rowsize; i++)
    {
      float d0 = app.csv.data.getRow(i).getFloat(column.index);
      float d1 = app.csv.data.getRow(i).getFloat(next.column.index);
      if(Float.isNaN(d0) || Float.isNaN(d1)) continue;
      int y1 = (int)map(d0,
                       bottomVal,
                       topVal,
                       app.mainChart.v0+app.mainChart.h,
                       app.mainChart.v0);
      
      int y2 = (int)map(d1,
                        next.bottomVal,
                        next.topVal,
                        app.mainChart.v0+app.mainChart.h,
                        app.mainChart.v0);
      
      float dist = sqrt((y2-y1)^2 + (x2-x1)^2);
      float lerpval = 
          abs((y2-y1)*x0 - (x2-x1)*y0 +x2*y1 -y2*x1)/dist;
      float chooseval = 0;
      if(x0 > (x1+x2)/2)
      {
        chooseval = y2;
        app.highlightX = x2;
        app.highlightMoveID = next.id;
      }
      else
      {
        chooseval = y1;
        app.highlightX = x1;
        app.highlightMoveID = id;
      }
      float f = (abs(mouseY - chooseval));
      if(f < curminvalue)
      {
        curminvalue = f;
        min_row_id = i;
        app.highlightY = (int)chooseval;
      }
      /*
      if(chooseval > mouseY -.05 && chooseval < mouseY + .05)
      {  
        app.highlighted_row = i;
        return;  //this may cause certain lines to never be highlighted due to overlapping, but this is sadly an imperfect system
      } 
      */
    }
    app.highlighted_row = min_row_id;
  }

  void processDrag(int X, int Y)
  {
    //int xdelta = X - lastX;
    //int ydelta = Y - lastY;
    u0 = X - offsetX;
    v0 = Y - offsetY;
    if(X-offsetX <= minX) u0 = minX;
    if(Y-offsetY <= minY) v0 = minY;
    if(v0 + h >= maxY) v0 = maxY - h;
    if(u0 + w >= maxX) u0 = maxX - w;

  }

}