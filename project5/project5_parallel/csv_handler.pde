//-----------
/*
*
*      CSV classes
*
*
*/
//-----------

import java.util.*;
import java.util.Comparator;
import java.util.Set;
import java.util.HashSet;

class CSV
{
  String path = "";
  String name = "";
  int rowsize = 0;
  int columnsize = 0;
  Table data = null;
  boolean hasCategoricalData = false;  //this will be used to determine if we worry about rendering color types
  
  TypeColumnList floatColumns = new TypeColumnList();
  TypeColumnList stringColumns = new TypeColumnList();
  TypeColumnList allColumns = new TypeColumnList();
  
  AggregateColumn XAgg = new AggregateColumn();
  AggregateColumn YAgg = new AggregateColumn();

  //basic column get functions
  CSVColumn GetColumn(int i)
  {
    return allColumns.getIthColumn(i);
  }
  
  //given the current csvcolumn, determine what the next column should be
  CSVColumn GetNextColumnNoRepeat(CSVColumn _c)
  {
    int index = (_c.index+1)%columnsize;
    while(allColumns.getIthColumn(index).repeatedValues && index != _c.index)
    {
      index = (index + 1) % columnsize;
    }
    
     return allColumns.getIthColumn(index);
  }
  
  CSVColumn GetPrevColumnNoRepeat(CSVColumn _c)
  {
    int index = _c.index-1;
    if(index < 0) index = columnsize -1;
    while(allColumns.getIthColumn(index).repeatedValues && index != _c.index)
    {
      index--;
      if(index < 0) index = columnsize -1;
    }
    
     return allColumns.getIthColumn(index);
  }
  
  
  
  //column retrieval functions with an additional exclude column parameter (which can be useful for preventing XAxis=YAxis overlap
  CSVColumn GetNextColumn(CSVColumn _c, int skipIndex)
  {
    int peekIndex = (_c.index+1)%columnsize;
    if(peekIndex == skipIndex) peekIndex = (peekIndex+1)%columnsize;
    
    return allColumns.getIthColumn(peekIndex);
  }
  
  //note: optimize this later. for now we just want it to be correct
  CSVColumn GetPrevColumn(CSVColumn _c, int skipIndex)
  {
    int peekIndex = _c.index-1;
    if(peekIndex < 0) peekIndex = columnsize -1;
    if(peekIndex == skipIndex) peekIndex = peekIndex-1;
    if(peekIndex < 0) peekIndex = columnsize -1;
    return allColumns.getIthColumn(peekIndex);
  }
  
  
  //much simpler column retrieval functions if we dont care about repeated values
  CSVColumn GetNextColumn(CSVColumn _c)
  {
    return allColumns.getIthColumn((_c.index + 1)%columnsize);
  }
  CSVColumn GetPrevColumn(CSVColumn _c)
  {
    int newindex = _c.index -1;
    if(newindex < 0) newindex = columnsize -1;  //i know % should work here, but apparently it's not... and im not about to debug it
    return allColumns.getIthColumn(newindex);
  }
  
  
  float alpha = 260;
  
  //this is where we deal with special cases regarding csv headings. For example, treating any YEAR column as a string column is likely more ideal
  boolean TitleExceptions(int i )
  {
    String t = data.getColumnTitle(i);
    return t.equalsIgnoreCase("NAME") || t.equalsIgnoreCase("ID");//t.equalsIgnoreCase("YEAR")
  }
  
  
  void setupData(File file)
  {
     path = file.getAbsolutePath();
     name = file.getName();
     int pos = name.lastIndexOf(".");
     if (pos > 0) {
         name = name.substring(0, pos);
     }
     data = loadTable(path,"header");
     rowsize = data.getRowCount();
     columnsize = data.getColumnCount();
     
     //prepare categorization of the columns
     colorMode(HSB,360,100,100);
     for(int i = 0; i < columnsize; i++)
     {
       boolean repeatedValues = HasRepeatedValues(data.getStringColumn(i));
       //for float columns
       if(!Float.isNaN( data.getRow( 0 ).getFloat(i)) && !TitleExceptions(i))
       {
         floatColumns.add(i,data.getColumnTitle(i));
         
         //set inner details
         CSVColumn fc = floatColumns.getColumn(i);
         fc.type = Typetype.FLOAT;
         fc.maxVal = max(data.getFloatColumn(i));
         fc.minVal = min(data.getFloatColumn(i));
         fc.repeatedValues = repeatedValues;
         allColumns.add(i,fc);
         
         //get unique values
         FloatList values = new FloatList();
         for(float f : data.getFloatColumn(i))
         {
           if(!values.hasValue(f))
           {
             values.append(f);
             fc.floatHistogram.append(1);
           }
           else
           {
             //update histogram
             int i1 = 0;
             for(float f1: values)
             {
               if(f == f1)
               {
                 fc.floatHistogram.add(i1,1);
                 break;
               }
               i1++;
             }
           }
         }
        fc.uniqueFloatValues = values;
        
        int maxval = max(fc.floatHistogram.array());
        fc.modevalueindex = 0;
        for(int j : fc.floatHistogram)
        {
          if(j == maxval) break;
          fc.modevalueindex++;
        }
       }
       //for string columns
       else
       {
         stringColumns.add(i,data.getColumnTitle(i));
         CSVColumn sc = stringColumns.getColumn(i);
         //now get unique values
         StringList values = new StringList();
         IntList colorvalues = new IntList();
         for(String s : data.getStringColumn(i))
         {
           if(!values.hasValue(s))
           {
             values.append(s);
             sc.stringHistogram.append(1);
           }
           else
           {
             //update histogram
             int i1 = 0;
             for(String s1 : values)
             {
               if(s1.equals(s))
               {
                 sc.stringHistogram.add(i1,1);
                 break;
               }
               i1++;
             }
           }
         }
         
         for(int cci = 0; cci < values.size(); cci ++)
        {
          color ci = color(map(cci,0,values.size(),20,360),65,85,alpha);
          colorvalues.append(ci);
        }
        
        
        int maxval = max(sc.stringHistogram.array());
        sc.modevalueindex = 0;
        for(int j : sc.stringHistogram)
        {
          if(j == maxval) break;
          sc.modevalueindex++;
        }
        
        sc.type = Typetype.STRING;
        sc.uniqueStringValues = values;
        sc.colorValues = colorvalues;
        sc.repeatedValues = repeatedValues;
        allColumns.add(i,sc);
       }
     }
     colorMode(RGB,255);
     if(stringColumns.columns.size() > 0) hasCategoricalData = true;
  }
  
  //we will always build aggregate columns  regardless. its just a matter of deciding whether to use it
  void BuildAggregateColumns(int maxElements,CSVColumn XColumn, CSVColumn YColumn, AggMode aggMode)
  {
    
    XAgg = new AggregateColumn();
    YAgg = new AggregateColumn();
    XAgg.bucketsize = rowsize/maxElements;
    //the idea for aggregate columns, is that for the x-axis, we are trying to get the ideal element arrangement setup
    //such that enough information is displayed in some accurate manner (ranges of floats, or unique strings), but not too much to flood the screen
    
    //go ahead and sort the 2 lists. trying to juggle the data with them being unsorted is just unfeasible
    if(XColumn.type == Typetype.STRING)
    {
      XAgg.uniqueStringValues = XColumn.uniqueStringValues;
      XAgg.stringHistogram = XColumn.stringHistogram;
      XAgg.type = Typetype.STRING;
      
      if(YColumn.type == Typetype.STRING)
      {
        YAgg.uniqueStringValues = YColumn.uniqueStringValues;
        YAgg.stringHistogram = YColumn.stringHistogram;
        YAgg.type = Typetype.STRING;
        
         String[]  xlist = data.getStringColumn(XColumn.index);
         String[]  ylist = data.getStringColumn(YColumn.index);
         
         ArrayList<AGG_S_S_PAIR> pairs = new ArrayList<AGG_S_S_PAIR>();
         for(int i = 0; i < rowsize; i++)
         {
           pairs.add(new AGG_S_S_PAIR(xlist[i], ylist[i]));
         }
         Collections.sort(pairs,new ASSPCOMP());
         
         switch(aggMode)
         {
           case AVG:
           case COUNT_AVG:
           case MEDIAN:
           case MODE:
          {
             int cur_element = 0;
             String start = ""; String end = "";
             int ci = 0;
             int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
             IntList histogram = new IntList();
             for(int i = 0; i < YAgg.uniqueStringValues.size(); i++)
             {
               histogram.append(0);
             }
           
            for(AGG_S_S_PAIR pair:pairs)
            {
              if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
              {
                int len = (pair.x.length() < 4 ? pair.x.length() : 4);
                 end = pair.x.substring(0,len);
                 
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   int histi = 0;
                    for(String s : YAgg.uniqueStringValues)
                    {
                      if(s.equals(pair.y)) break;
                      histi++;
                    }
                    if(histi<histogram.size())
                    histogram.add(histi,1);
                    else
                    {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                 }
                 else
                 {
                   int histi = 0;
                  for(String s : YAgg.uniqueStringValues)
                  {
                    if(s.equals(pair.y)) break;
                    histi++;
                  }
                  if(histi<histogram.size())
                  histogram.add(histi,1);
                  else
                  {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                  
                 }
                 
                 //the histogram is now prepared, select the appropriate index
                 int selectedIndex =0;
                 int maxhist = max(histogram.array());
                 for(selectedIndex = 0; selectedIndex < histogram.size();selectedIndex++)
                 {
                   if(histogram.get(selectedIndex) == maxhist)break;
                 }
                 
                 String value = YAgg.uniqueStringValues.get(selectedIndex);
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementStringValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 ci++;
                 
                continue;
              }
              else if(cur_element == 0)
              {
                
                int len = (pair.x.length() < 4 ? pair.x.length() : 4);
                start = pair.x.substring(0,len);
                
                int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                
                cur_element++;
                ci++;
                continue;
              }
              //take the current value and add it to the histogram
              int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
             
              ci++;
              cur_element++;
            }
          }
          break;
         }
         

      }
      else //YColumn type is FLOAT
      {
         YAgg.uniqueFloatValues = YColumn.uniqueFloatValues;
         YAgg.floatHistogram = YColumn.floatHistogram;
         YAgg.type = Typetype.FLOAT;
         YAgg.maxVal = YColumn.maxVal;
         YAgg.minVal = YColumn.minVal;
         
         String[] xlist = data.getStringColumn(XColumn.index);
         float[]  ylist = data.getFloatColumn (YColumn.index);
  
         ArrayList<AGG_S_F_PAIR> pairs = new ArrayList<AGG_S_F_PAIR>();
         for(int i = 0; i < rowsize; i++)
         {
           pairs.add(new AGG_S_F_PAIR(xlist[i], ylist[i]));
         }
         Collections.sort(pairs,new ASFPCOMP());
         
         switch(aggMode)
         {
           case AVG:
           {
             
             int cur_element = 0;
             String start = ""; String end = "";
             float value = 0; //for now we will just do average, but depending on the aggregation method, how we compute this could change
             int ci = 0;
             int bucketsize = XAgg.bucketsize;
             for(AGG_S_F_PAIR pair : pairs)
             {
               if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
               {
                 //we've reached the end of the bucket, process the current element and put it in
                 end = pair.x;
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                 }
                 else
                 {
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                   else
                   {
                     bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   }
                   value /= XAgg.bucketsize;
                 }
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                 result = start;
                 else
                 result = start+"-"+end;
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementValues.append(value);
                 bucketsize = XAgg.bucketsize;
                 cur_element = 0;
                 value = 0;
                 ci++;
                 continue;
               }
               else if(cur_element == 0)
               {
                 if(Float.isNaN(pair.y))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                 start = pair.x;
                 value += pair.y;
                 cur_element++;
                 ci++;
                 continue;
               }
               if( !Float.isNaN(pair.y))
                 value += pair.y;
               else
               {
                 bucketsize --; if(bucketsize <= 0) bucketsize = 1;
               }
               cur_element++;
               ci++;
             }
             
           }
           break;

           case MODE:
           case MEDIAN:           
           case COUNT_AVG:
           {
             //first, start by initializing our list with all unique instances
             int i = 0;
             for(String s : XAgg.uniqueStringValues)
             {
               XAgg.aggElementNames.append(s);
               float val = 0;
               int j = 0;
               for(float f : ylist)
               {
                   if(xlist[j] == s  && !Float.isNaN(f))
                     val+= f;
                   
                   j++;  
               }
               val /= XAgg.stringHistogram.get(i);  //be sure to only divide it by the actual number of times it occurs. by definition, this must be AT LEAST ONCE
               XAgg.aggElementValues.append(val);
               i++;
             }
           }
           break;
           default:
           break;
         }
         
         
         
      }
    }
    else //XColumn type is FLOAT
    {
      XAgg.uniqueFloatValues = XColumn.uniqueFloatValues;
      XAgg.floatHistogram = XColumn.floatHistogram;
      XAgg.type = Typetype.FLOAT;
      XAgg.maxVal = XColumn.maxVal;
      XAgg.minVal = XColumn.minVal;
      
      
      if(YColumn.type == Typetype.STRING)
      {
        YAgg.uniqueStringValues = YColumn.uniqueStringValues;
        YAgg.stringHistogram = YColumn.stringHistogram;
        YAgg.type = Typetype.STRING;
        
       float[]  xlist = data.getFloatColumn(XColumn.index);
       String[] ylist = data.getStringColumn(YColumn.index);

       ArrayList<AGG_F_S_PAIR> pairs = new ArrayList<AGG_F_S_PAIR>();
       for(int i = 0; i < rowsize; i++)
       {
         pairs.add(new AGG_F_S_PAIR(xlist[i], ylist[i]));
       }
       
        Collections.sort(pairs,new AFSPCOMP());
        
        //because strings arent computable on their own, and median itself is abit too arbitrary, we will use mode as our selection basis
        switch(aggMode)
        {
          case AVG:
          case COUNT_AVG:
          case MEDIAN:
          case MODE:
          {
             int cur_element = 0;
             String start = ""; String end = "";
             int ci = 0;
             int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
             IntList histogram = new IntList();
             for(int i = 0; i < YAgg.uniqueStringValues.size(); i++)
             {
               histogram.append(0);
             }
           
            for(AGG_F_S_PAIR pair:pairs)
            {
              if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
              {
                int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 end = str(pair.x).substring(0,len);
                 
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   int histi = 0;
                    for(String s : YAgg.uniqueStringValues)
                    {
                      if(s.equals(pair.y)) break;
                      histi++;
                    }
                    if(histi<histogram.size())
                    histogram.add(histi,1);
                    else
                    {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                 }
                 else
                 {
                   int histi = 0;
                  for(String s : YAgg.uniqueStringValues)
                  {
                    if(s.equals(pair.y)) break;
                    histi++;
                  }
                  if(histi<histogram.size())
                  histogram.add(histi,1);
                  else
                  {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                  
                 }
                 
                 //the histogram is now prepared, select the appropriate index
                 int selectedIndex =0;
                 int maxhist = max(histogram.array());
                 for(selectedIndex = 0; selectedIndex < histogram.size();selectedIndex++)
                 {
                   if(histogram.get(selectedIndex) == maxhist)break;
                 }
                 
                 String value = YAgg.uniqueStringValues.get(selectedIndex);
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementStringValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 ci++;
                 
                continue;
              }
              else if(cur_element == 0)
              {
                if(Float.isNaN(pair.x))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                start = str(pair.x).substring(0,len);
                
                int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
                
                cur_element++;
                ci++;
                continue;
              }
              //take the current value and add it to the histogram
              int histi = 0;
                for(String s : YAgg.uniqueStringValues)
                {
                  if(s.equals(pair.y)) break;
                  histi++;
                }
                if(histi<histogram.size())
                histogram.add(histi,1);
                else
                {bucketsize --; if(bucketsize <= 0) bucketsize = 1;}
             
              ci++;
              cur_element++;
            }
          }
          break;
          default:
          break;
          
        }
          
      }
      else //YColumn type is FLOAT
      {
        YAgg.uniqueFloatValues = YColumn.uniqueFloatValues;
        YAgg.floatHistogram = YColumn.floatHistogram;
        YAgg.type = Typetype.FLOAT;
        YAgg.maxVal = YColumn.maxVal;
        YAgg.minVal = YColumn.minVal;
         
       float[]  xlist = data.getFloatColumn(XColumn.index);
       float[] ylist =  data.getFloatColumn(YColumn.index); 
       
       ArrayList<AGG_F_F_PAIR> pairs = new ArrayList<AGG_F_F_PAIR>();
       for(int i = 0; i < rowsize; i++)
       {
         pairs.add(new AGG_F_F_PAIR(xlist[i], ylist[i]));
       }
       Collections.sort(pairs,new AFFPCOMP());
       switch(aggMode)
       {
         case AVG:
         {
         
           int cur_element = 0;
           String start = ""; String end = "";
           float value = 0; //for now we will just do average, but depending on the aggregation method, how we compute this could change
           int ci = 0;
           int bucketsize = XAgg.bucketsize; //in the case of NaNs, we want our bucket to be adaptive
           for(AGG_F_F_PAIR pair : pairs)
             {
               if(cur_element == XAgg.bucketsize-1 || ci == pairs.size()-1)
               {
                 //we've reached the end of the bucket, process the current element and put it in
                 int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 end = str(pair.x).substring(0,len);
                 if(start == "" && ci == pairs.size()-1)
                 {
                   start = end;
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                 }
                 else
                 {
                   if( !Float.isNaN(pair.y))
                     value += pair.y;
                   else
                   {
                     bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   }
                   value /= XAgg.bucketsize;
                 }
                 
                 //now that values are calculated, put them in our bucket
                 String result = "";
                 if(start.equals(end))
                   result = start;
                  else
                    result = start+"-"+end;
                 
                 XAgg.aggElementNames.append(result); start = ""; end = "";
                 XAgg.aggElementValues.append(value);
                 cur_element = 0;
                 bucketsize = XAgg.bucketsize;
                 value = 0;
                 ci++;
                 continue;
               }
               else if(cur_element == 0)
               {
                 if(Float.isNaN(pair.x) || Float.isNaN(pair.y))
                 {
                   bucketsize --; if(bucketsize <= 0) bucketsize = 1;
                   continue;
                 }
                 int len = (str(pair.x).length() < 4 ? str(pair.x).length() : 4);
                 start = str(pair.x).substring(0,len);
                 value += pair.y;
                 cur_element++;
                 ci++;
                 continue;
               }
               if( !Float.isNaN(pair.y))
                 value += pair.y;
               else
               {
                 bucketsize --; if(bucketsize <= 0) bucketsize = 1;
               }
               cur_element++;
               ci++;
             }
           
         }
         break;

         
         case MODE:
         case MEDIAN:
         case COUNT_AVG:
         {
           //first, start by initializing our list with all unique instances
             int i = 0;
             for(float s : XAgg.uniqueFloatValues)
             {
               int len = (str(s).length() < 4 ? str(s).length() : 4);
               XAgg.aggElementNames.append(str(s).substring(0,len));
               float val = 0;
               int j = 0;
               for(float f : ylist)
               {
                 if(xlist[j] == s && !Float.isNaN(f))
                   val+= f;
                  
                 j++;
               }
               val /= XAgg.floatHistogram.get(i);  //be sure to only divide it by the actual number of times it occurs. by definition, this must be AT LEAST ONCE
               XAgg.aggElementValues.append(val);
               i++;
             }
         }
         break;
         
         default:
         break;
       } 
      }
    }
    
    //the y-axis in this case will depend largely on the aggregation mode, the result of the x-axis, the type arrangement of the 2 axes, and its own distribution
  }
}




class TypeColumnList
{
  ArrayList<CSVColumn> columns = new ArrayList<CSVColumn>();
  IntList indices = new IntList();
  
  CSVColumn getColumn(int i){
    for(CSVColumn col : columns) if(i == col.index) return col;
    return null;//columns.get(0);//this feels like really bad practice, should I just return null and cause the program to fail?
  }
  
  CSVColumn getIthColumn(int i)
  {
    if(i >= 0 && i < columns.size()){ return columns.get(i);}
    else return null;//columns.get(0); //this feels like really bad practice, should I just return null and cause the program to fail?
  }
  
  boolean isEmpty(){return columns.size() > 0;}
  void add(int i, String name)
  {
    CSVColumn col = new CSVColumn();
    col.name = name;
    col.index = i;
    columns.add(col);
    indices.append(i);
  }
  
  void add(int i,CSVColumn col)
  {
    columns.add(col);
    indices.append(i);
  }
}


//aggregate helper classes (the pair is ALWAYS sorted by the first element)
class AFSPCOMP implements Comparator<AGG_F_S_PAIR>
{
  @Override
  public int compare(AGG_F_S_PAIR a, AGG_F_S_PAIR b){
    if(Float.isNaN(a.x) || Float.isNaN(b.x)) return -1;
    else
    return (a.x < b.x) ? -1 : (a.x==b.x) ? 0 : 1;
  }
}
class AGG_F_S_PAIR 
{
  float x; String y;
  AGG_F_S_PAIR(float s, String ss) { x = s; y = ss;}
  
}



class AFFPCOMP implements Comparator<AGG_F_F_PAIR>
{
  @Override
  public int compare(AGG_F_F_PAIR a, AGG_F_F_PAIR b){
      if(Float.isNaN(a.x) || Float.isNaN(b.x)) return -1;
    else
      return (a.x < b.x) ? -1 : (a.x==b.x) ? 0 : 1;
  }
}
class AGG_F_F_PAIR
{
  float x; float y;
  AGG_F_F_PAIR(float s, float ss) { x = s; y = ss;}
}

class ASSPCOMP implements Comparator<AGG_S_S_PAIR>
{
  @Override
  public int compare(AGG_S_S_PAIR a, AGG_S_S_PAIR b){
    return a.x.compareTo(b.x);
  }
}
class AGG_S_S_PAIR
{
  String x; String y;
  AGG_S_S_PAIR(String s, String ss) { x = s; y = ss;}
}

class ASFPCOMP implements Comparator<AGG_S_F_PAIR>
{
  @Override
  public int compare(AGG_S_F_PAIR a, AGG_S_F_PAIR b){
    return a.x.compareTo(b.x);
  }
}
class AGG_S_F_PAIR
{
  String x; float y;
  AGG_S_F_PAIR(String s, float ss) { x = s; y = ss;}
}

enum AggMode{ AVG, MODE, MEDIAN, COUNT_AVG }
class AggregateColumn
{
  Typetype type = Typetype.STRING;
  int bucketsize= 10;  //how many 'rows' would fit into each entry in this aggregate column?
  StringList aggElementNames = new StringList();  //for example, an aggregate element from 1992-2006 would contain the elements 1992,1993,1994...,2006 and so on for the entire list
  FloatList  aggElementValues = new FloatList();  //depending on the aggregation method, this list is matched with the list above, and its value corresponds to the aggregation for those elements
  StringList aggElementStringValues = new StringList();
  
  float maxVal = 0;
  float minVal = 0;
  
  StringList uniqueStringValues = new StringList();
  IntList stringHistogram = new IntList();
  
  FloatList uniqueFloatValues = new FloatList();
  IntList floatHistogram = new IntList();

}

class CSVColumn
{
  Typetype type = Typetype.STRING;
  boolean repeatedValues = false; //this is used to determine if the column is unique values only
  String name = "";
  int index = 0;
  float maxVal = 0;
  float minVal = 0;
  boolean showDetails = false;
  
  int modevalueindex = 0;
  //this corresponds only with the stringlist
  IntList colorValues = new IntList();
   
  StringList uniqueStringValues = new StringList();
  IntList stringHistogram = new IntList();
  
  //add the ability to consider aggregate data in the case of repeated v  FloatList uniqueFloatValues = new FloatList();
  FloatList uniqueFloatValues = new FloatList();
  IntList floatHistogram = new IntList();

  //current ideas: average, count
  //the new list we work on, the aggregate list, must be a unique value list, where the y value is the result of the aggregate function
  //do we also want to sort the aggregate list?
  //for that aggregate list, do we want to aggregate the columns themselves further so we have a more digestible number (in the event of too many x elements on screen)?
}