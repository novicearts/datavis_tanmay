enum APP_ACTION
{
  NONE,
  LOAD_FILE,
  HELP,
  MOVE_CAT,
  FLIP_AXIS
}
screen_details scr = new screen_details();
application_details app = new application_details();

//-----------
/*
*
*      Processing Setup Code
*
*
*/
//-----------

void setup(){
  size(800,400);
  scr.init("");
  app.init();
  selectInput("Select a csv data file to use","parseCSV");
}


void parseCSV(File csvfile)
{
 if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
 }else{
    
    app.csv.setupData(csvfile);
    scr.init("");
    
    
    
    //setup the charts
    app.AddChart(app.csv, scr.screen_margin,scr.screen_margin,
                      scr.screen_width, scr.screen_height);
    app.GetMainChart();
    app.mainChart.drawBorder = false;
    int colcount = app.csv.allColumns.columns.size();
    float gapX = app.mainChart.w/(float)(colcount);
    app.hotzoneradius = (int)gapX/2;
    int i = 0;
    
    //setup the axes for dragging
    for(CSVColumn col : app.csv.allColumns.columns)
    {
      //app.axes.add(col);
      int hotzone = app.mainChart.u0 + (int)gapX*i+(int)gapX/2;
      app.hotzones.append(hotzone);
      dragFrame drag = app.AddDrag(col.name,APP_ACTION.NONE, hotzone ,app.mainChart.v0, 4 ,app.mainChart.h+3);
      drag.minX = app.mainChart.u0;
      drag.minY = app.mainChart.v0;
      drag.maxX = app.mainChart.u0 + app.mainChart.w;
      drag.maxY = app.mainChart.v0 + app.mainChart.h+3;
      drag.column = col;
      drag.textDirection = Direction.DOWN;
      drag.topVal = col.maxVal;
      drag.bottomVal = col.minVal;
      //add a flip button
      
        buttonFrame button = app.AddButton("Flip",APP_ACTION.FLIP_AXIS,hotzone-60 ,drag.maxY,30);
        button.affectorid = i;
      
      i++;
    }
     
    app.AddButton("Load..",APP_ACTION.LOAD_FILE,0,0,30);
    app.AddButton(" ?",APP_ACTION.HELP,0,30,30);
    
    if(app.csv.hasCategoricalData)
    {
      app.categoryColumn = app.csv.stringColumns.columns.get(app.catindex);
      app.usingcategories = true;
      app.showLegend = true;
      app.AddButton("Next Category",APP_ACTION.MOVE_CAT,scr.screen_margin+scr.screen_width-150,0,30);
    }
   
    app.isRunning= true;
 }
}


//-----------
/*
*
*      Processing Application Code
*
*
*/
//-----------

void reload()
{
  app.isRunning = false;
  app = new application_details();
  scr = new screen_details();
  setup();   
}

void mousePressed()
{
  for(buttonFrame b : app.buttons)
  {
    if(b.inBounds())
    {
      b.processClick();
      app.currentButtonID = b.id;
      return;
    }
  }
    for(dragFrame d : app.drag_frames)
  {
     /*if(d.inBounds())
     {
       d.processClick();
       d.lastX = mouseX; d.lastY = mouseY;
       d.offsetX = mouseX - d.u0; d.offsetY = mouseY - d.v0;
       app.currentDragID = d.id;
       return;
     }
     else*/ if(inRectBounds(mouseX,mouseY, app.hotzones.get(d.id)-app.hotzoneradius,app.mainChart.v0,app.hotzoneradius*2,app.mainChart.h))
     {
       d.processClick();
       d.lastX = mouseX; d.lastY = mouseY;
       d.offsetX = d.w/2; d.offsetY = mouseY - d.v0;
       app.currentDragID = d.id;
       return;
     }
  }
}

void mouseReleased()
{
  for(buttonFrame b : app.buttons)
  {
    if(app.currentButtonID == b.id)
    {
      b.processRelease();
      app.currentButtonID = -1;
      return;
    }
  }
  for(dragFrame d : app.drag_frames)
  {
    if(app.currentDragID == d.id)
    {
      d.processRelease();
      if(d.id ==app.hotzoneSwapID || app.hotzoneSwapID == -1)
      {
        d.u0 = app.hotzones.get(d.id);
        d.v0 = app.mainChart.v0;
      }
      else
      {
        //swap the relevant data first, then snap both pieces to their new homes
        int swapid = app.hotzoneSwapID;
        int tempid = d.id;
        int tempu0 = app.hotzones.get(tempid);
        int tempv0 = app.mainChart.v0;
        //swap the positions and frame ids first
        dragFrame swapframe = app.drag_frames.get(app.hotzoneSwapID);
        d.id = swapframe.id;
        swapframe.id = tempid;
        
        d.u0 = swapframe.u0;
        d.v0 = tempv0;
        swapframe.u0 = tempu0;
        swapframe.v0 = tempv0;
        
        //then swap their list locations
        Collections.swap(app.drag_frames,swapid,tempid);
        
      }
      app.currentDragID = -1;
      app.hotzoneDragID = -1;
      app.hotzoneSwapID = -1;
      return;
    }
  }
  
}

void mouseDragged()
{
  for(buttonFrame b : app.buttons)
  {
    if(!b.inBounds() && b.id == app.currentButtonID)
    {
      b.justRelease();
      app.currentButtonID = -1;
      return;
    }
  }
  for(dragFrame d : app.drag_frames)
  {
    if(d.id == app.currentDragID)
    {
      d.processDrag(mouseX,mouseY);
      for(int i =0; i < app.hotzones.size();i++)
      {
        //if(i == d.id) continue;
        if(d.u0 < app.hotzones.get(i)+app.hotzoneradius && 
           d.u0 > app.hotzones.get(i)-app.hotzoneradius &&
          app.hotzoneSwapID != i)
        {
          app.hotzoneDragID = d.id;
          app.hotzoneSwapID = i;
          //println("swap "+d.text+" with "+app.drag_frames.get(i).text+"?");
        }
      }
    }
  }
}
void keyPressed()
{
  switch(key)
  {
    case '/':
    {
      handleAction(APP_ACTION.HELP);
    }
    break;
    case TAB:
    case ' ':
    case 'g':
    case 'G':
    case CODED:
    {
      if(keyCode == SHIFT){}
      switch(keyCode)
      {
        case UP:
        case DOWN:
        case LEFT:
        case RIGHT:
        default:break;
      }
    }
    break;
    default:break;
  }
  
}

//void GetNextAttribute()

void handleAction(APP_ACTION action)
{
  //note: almost ALL actions will affect the main chart, so grab a reference to it now!
  switch(action)
  {
    case LOAD_FILE:
    {
      reload();
    }
    break;
    case HELP:
    {
      app.showHelp = !app.showHelp;
    }
    break;
    case MOVE_CAT:
    {
      app.getNextCategory();
    }
    break;
    case FLIP_AXIS:
    {
      app.drag_frames.get(app.affectid).flipAxis = !app.drag_frames.get(app.affectid).flipAxis;
      app.affectid = -1;
    }
    break;
    default:
    {
      println(action.toString());
    }break;
  }
}

void draw()
{
  background(255);
  fill(0);
  if(app.isRunning)
  {
    textSize(scr.textsize/2);
    
    
    //draw everything
    for(frame chart : app.charts)
    {
      if(chart != null)
      {
        chart.draw();
      }
    }
    for(buttonFrame b : app.buttons)
    {
      if(b != null)
        b.draw();
    }
    for(dragFrame d : app.drag_frames)
    {
      if(d != null)
      {
        d.draw();
     
        if(d.id == app.hotzoneSwapID)
        {
          fill(100,100);
          rect(app.hotzones.get(d.id)-app.hotzoneradius, app.mainChart.v0, app.hotzoneradius*2, app.mainChart.h);
        }
      }
    }
    
    //if mouse is obviously outside of the frame, clear the highlighted row
    if(app.currentDragID == -1)
    {
      if(!app.mainChart.inBounds())
      {
        app.highlighted_row = -1;
      }
       else
       {
         //determine where we should highlight
       
         //determine which hotzone we're in
         int hotzone = 0;
         for(int hzone : app.hotzones)
         {
           if(mouseX > hzone-app.hotzoneradius && mouseX < hzone+app.hotzoneradius) {break;}
           hotzone++;
         }
         if(hotzone < app.drag_frames.size()-1)
         {
           //prepare the things we need
           dragFrame next = app.drag_frames.get(hotzone+1);
           int curx = app.hotzones.get(hotzone);
           int nextx = app.hotzones.get(hotzone+1);
           app.drag_frames.get(hotzone).determineHighlight(next,curx,nextx);
         }
         else if(hotzone < app.drag_frames.size())
         {
           dragFrame next = app.drag_frames.get(hotzone);
           int curx = app.hotzones.get(hotzone-1);
           int nextx = app.hotzones.get(hotzone);
           app.drag_frames.get(hotzone-1).determineHighlight(next,curx,nextx);
         }
       }
    }
    //deal with any edge cases regarding the last drag frame
    {
      app.drag_frames.get(app.drag_frames.size()-1).flipAxis();
    }
    
    //now draw the data
    for(int i = 0; i < app.drag_frames.size()-1;i++)
    {
      dragFrame d = app.drag_frames.get(i);
      dragFrame d1 = app.drag_frames.get(i+1);
      d.drawLines(d1);  
      
    }
    
    
    
    //draw any final details
    for(dragFrame d : app.drag_frames)
    {
      d.drawDetails();
    }
    
    //draw mouse details
    if(app.mainChart.inBounds())
    {
      if(app.highlighted_row != -1)
      {
        stroke(255,0,0,255);
        strokeWeight(2);
        int x = app.drag_frames.get(app.highlightMoveID).u0 + app.drag_frames.get(app.highlightMoveID).w/2;
        line(mouseX,mouseY,x,app.highlightY);
        strokeWeight(1);
        stroke(0);
      }
    }
    
    //check if any of the buttons were pressed
    for(buttonFrame b : app.buttons)
    {
      if(b.wasClicked)
      {
        //handle the button and clear it
        app.currentAction = b.action;
        b.wasClicked = false;
        
        if(b.affectorid != -1)
        {
          //this button does something too!
          app.affectid = b.affectorid;
        }
        
        break;
      }
    }
    
    //if action string is non-empty, handle it and then clear it
    if(app.currentAction != APP_ACTION.NONE)
    {
      handleAction(app.currentAction);
      app.currentAction = APP_ACTION.NONE;
    }
    
    //any additional logic
    if(app.showLegend)
    {
      app.drawLegend();
    }
    
    if(app.showHelp)
    {
      fill(180);
      rect(scr.screen_margin,scr.screen_margin,scr.screen_width,scr.screen_height);
      fill(0);
      text(app.helptext,scr.screen_margin*2,scr.screen_margin*2);
    }
  }
}