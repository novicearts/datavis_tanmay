

class screen_details
{
  void init()
  {
    screen_width = width - screen_margin*2;
    screen_height = height - screen_margin*2;
    key_x = screen_width + screen_margin/2;
    key_y = screen_height + screen_margin/2;
    SetTitle(title);
  }
  void init(String _title)
  {
    title = _title;
    init();
  }
  void SetTitle (String _title)
  {
    title = _title;
    title_x = screen_margin + screen_width/2;
    title_y = screen_margin/6;
  }
  int screen_margin = 75;   int screen_width = 0;   int screen_height = 0;
  int key_x = 0;   int key_y = 0;
  int title_x = 0; int title_y = 0;
  String title = "";
  int textsize = 32;
}