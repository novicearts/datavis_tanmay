

class application_details
{
  boolean isRunning = false;
  boolean showLegend = false;
  ArrayList<chartFrame> charts = new ArrayList<chartFrame>();
  chartFrame mainChart = null;
  ArrayList<buttonFrame> buttons = new ArrayList<buttonFrame>();
  ArrayList<dragFrame> drag_frames = new ArrayList<dragFrame>();
  
  int linealpha = 80;
  int affectid = -1;
  int highlighted_row = -1;
  int highlightMoveID = 0;
  int highlightX = 0;
  int highlightY = 0;
  
  CSVColumn categoryColumn = null;
  boolean usingcategories = false;
  int catindex = 0;
  
  IntList hotzones = new IntList();  //these are used for drag frame positioning
  int hotzoneradius = 15;
  int dragradius = 6;
  
  APP_ACTION currentAction = APP_ACTION.NONE;
  int currentButtonID = -1;
  int currentDragID = -1;
  
  //note: when a swap occurs, the actual data lists involved will reorder themselves, but the hotzone data will not, that remains constant
  //the same reordering logic will always apply regardless of the order of the data (as they will always be based on position in array)
  int hotzoneDragID = -1;
  int hotzoneSwapID = -1;
  
  KeyboardMode keymode = KeyboardMode.ATTRIBUTES;
  
  boolean showHelp = false;
  String helptext = 
        "Hi there!\n"
         + "To see the Parallel Coordinate Charts in action,\n"
         + "simply click and drag near one of the axes, and release on top of another axis to swap them.\n"
         + "You can open up this help box anytime by pressing the '/' key, or by pressing the '?' button on the left side.\n"
         + "You can also mouse over the chart data to highlight a particular data line of interest.\n"
         + "And the load button to the left lets you select a different file.\n"
         + "If unique categorical columns are detected, we do our best to assign a unique color to each distinct value\n"
         + "The colors will be associated with the currently identified category as indicated by the legend on the right\n"
         + "To switch between different categories, simply click the \"Next Category\" button.";
  //the drag frames keep track of the relevant columns for us
  //public ArrayList<CSVColumn> axes = new ArrayList<CSVColumn>();
  
  CSV csv = null;
  void init()
  {
    csv = new CSV();
  }
  
  void getNextCategory()
  {
    catindex = (catindex+1)%csv.stringColumns.columns.size();
    categoryColumn = csv.stringColumns.columns.get(catindex);
  }
  
  chartFrame AddChart(CSV csv,int u0, int v0, int w, int h)
  {
    chartFrame chart = new chartFrame();
    
    chart.setRect(u0,v0,w,h);

    chart.setup(csv.name,csv);
    println(chart.Title.title);
    chart.Title.direction = Direction.UP;
    chart.Title.V_margin = chart.title_margin;
     
    chart.drawToolTip  = true;
    
    charts.add(chart);
    return chart;
  }
  void GetMainChart()
  {
    if(charts.size() > 0) mainChart = charts.get(0);
    else 
    {
      charts.add(new chartFrame());
      mainChart = charts.get(0);
      mainChart.drawFrame = false;
    }
  }
  buttonFrame AddButton(String text, APP_ACTION action, int u0, int v0, int buttonheight)
  {
    buttonFrame button = new buttonFrame();
    button.action = action;
    button.text = text;
    button.id = buttons.size();
    int buttonwidth = button.buttontextsize*(int)(button.text.length()/1.5)+10;

    button.setRect(    u0,
                       v0,
                       buttonwidth,buttonheight);
    buttons.add(button);
    return button; 
  }
  
  dragFrame AddDrag(String text, APP_ACTION action, int u0, int v0, int buttonheight)
  {
    dragFrame drag = new dragFrame();
    drag.action = action;
    drag.text = text;
    drag.id = drag_frames.size();
    int buttonwidth = drag.buttontextsize*(int)(drag.text.length()/1.5)+10;
    drag.setRect(  u0,v0,buttonwidth,buttonheight);
    drag_frames.add(drag);
    return drag;
  }
  
  dragFrame AddDrag(String text,APP_ACTION action, int u0, int v0, int buttonwidth, int buttonheight)
  {
    dragFrame drag = new dragFrame();
    drag.action = action;
    drag.text = text;
    drag.id = drag_frames.size();
    drag.setRect(  u0,v0,buttonwidth,buttonheight);
    drag_frames.add(drag);
    return drag;
  }
  
  void drawLegend()
  {
    String max = categoryColumn.name;
    max = max.substring(0, (max.length() >14 ? 14 : max.length())) + (max.length() > 14 ? "." : "");
    String legend = max+":";
    text(legend, mainChart.u0+mainChart.w +5, 20);
    for(int i = 0; i < categoryColumn.uniqueStringValues.size(); i++)
      {
        max = categoryColumn.uniqueStringValues.get(i);
        max = max.substring(0, (max.length() > 7 ? 7 : max.length())) + (max.length() > 7 ? "." : "");
        int x =  mainChart.u0+mainChart.w +5;
        int y = 20+20*(i+1);
        fill(0);
        text(max, x, y);
        noStroke();
        fill(mainChart.categoryColumn.colorValues.get(i));
        ellipse( x+ 45, y-2, mainChart.pointsize/2,mainChart.pointsize/2);
      }
  }
}