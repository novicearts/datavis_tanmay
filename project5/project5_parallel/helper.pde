
//-----------
/*
*
*      Helper functions
*
*
*/
//-----------
import java.util.Set;
import java.util.HashSet;
boolean HasRepeatedValues(String[] array)
{
  Set<String> foundElements = new HashSet<String>();
  for( String s : array)
  {
    if(foundElements.contains(s))
    {
      return true;
    }
    foundElements.add(s);
  }
  return false;
}

float clamp(float val,float min, float max)
{
  return min(max, max(min,val));
}

boolean inRectBounds( int x, int y, int u0, int v0, int w, int h)
{
  return  x > u0 && x < u0 + w
        &&y > v0 && y < v0 + h;
}
boolean inCircleBounds(int x, int y, int sx, int sy, int radius)
{
  return sq(x - sx)/(radius*radius) + sq(y - sy)/(radius*radius) < 1 ;
}
boolean inEllipseBounds(int x, int y, int sx, int sy, int radiusA, int radiusB)
{
  return sq(x - sx)/(radiusA*radiusA) + sq(y - sy)/(radiusB*radiusB) < 1 ;
}