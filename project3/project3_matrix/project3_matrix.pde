//image settings are up here
int screen_margin = 100;
int screen_width = width -screen_margin*2;
int screen_height = height - screen_margin*2;

//application running data
frame largeChart = null;
//we're gonna allocate a list of charts
ArrayList<frame> charts = new ArrayList<frame>();
boolean isRunning = false;
boolean showLarge = false;
CSV csv = null;

int numberOfColumns = 0;

void setup(){
  size(800,600);  
  screen_width = width -screen_margin*2;
  screen_height = height - screen_margin*2;
  largeChart  = new chartFrame();
  csv = new CSV();
  selectInput("Select a csv data file to use","parseCSV");
  
}



void parseCSV(File csvfile){
   if(csvfile == null){
     println("Failed to select csv file!");
     println("Please select a csv file to use this application!");
     exit();
   }else{
     csv.setupData(csvfile);
     //we have retrieved the data we need, begin running!
     
     numberOfColumns = csv.data.getColumnCount();
     if(numberOfColumns > 4)
       numberOfColumns = 4;
     println("col count: "+numberOfColumns);
     int framewidthinterval = screen_width/numberOfColumns;
     int frameheightinterval = screen_height/numberOfColumns;
     println("scrn dim: ", framewidthinterval,frameheightinterval);
     int framex = screen_margin;
     int framey = screen_margin - screen_margin/2;
     
     //first, let's get a proper cartesian product here
     //let's set the rect prematurely here
     for(int i = 0; i < numberOfColumns; i ++)
     {
       for(int j = i; j < numberOfColumns; j++)
       {
         if(i != j)
         {
           println(csv.data.getColumnTitle(i),csv.data.getColumnTitle(j));
           frame chart = new chartFrame();
           
           framex = screen_margin + (framewidthinterval+ 40)*i;
           framey = screen_margin - screen_margin/3 + (frameheightinterval+20)*j;
           
           String ytitle = "";
           String xtitle = "";
           xtitle = csv.data.getColumnTitle(i);
           ytitle = csv.data.getColumnTitle(j);
           //with this, hopefully we are allowing the frame title to be correctly placed
           if(j-1 == i) 
           {
             chart.showTitle = true;
           }
           if(i==0)
           {
             
             chart.showYScale = true;
             chart.showYTitle = true;
           }
           if(j == numberOfColumns -1)
           {
             chart.showXScale = true;
           }
           chart.setupData(xtitle,
                           "",i,
                           ytitle,j,
                           ColorMode.HIGHLIGHT,3,
                           csv,ChartType.SCATTER, 2, false,false);
           
           
           //now the hard part...figure out where the chart should be, and what size it should be
           chart.setRect( framex , framey , framewidthinterval , frameheightinterval  );
           
           charts.add(chart);
           
         }
       }
     }
     
    //init the chart
    ((chartFrame)largeChart).copyData((chartFrame)charts.get(0),csv);
    largeChart.setRect(screen_margin+screen_width/2,screen_margin,
                        screen_width/2,screen_height/2 - screen_margin/3);
    largeChart.showXScale = true;
    largeChart.showYScale = true;
    largeChart.showTitle = true;
    largeChart.showYTitle = true;
    ((chartFrame)largeChart).drawGridLines = true;
    
    //showLarge = true;
    isRunning = true;
   }
   
}

void mousePressed()
{
  showLarge = false;
  for(frame chart : charts)
  {
    if(mouseX > chart.u0 && mouseX < chart.u0 + chart.w
      &&mouseY > chart.v0 && mouseY < chart.v0 + chart.h)
    {
      showLarge = true;
      ((chartFrame)largeChart).copyData((chartFrame)chart,csv);
    }
  }
}


void draw(){
  background( 255 );
 int textsize = 64;
 String title = "Scatterplot Matrix";
  if(isRunning){
    
    textSize(textsize/2);
    text(title, screen_margin/2+width/2 - textsize*title.length()/6, screen_margin/2 );
    for(frame chart : charts)
    {
      if( chart != null ){
         chart.draw();
      }
    }
    
    if(showLarge)
    {
      if(largeChart != null){
        largeChart.draw();
      }
    }
  }
  else
  {
  }
}


//a helper class for dealing with CSV parsing and data
class CSV
{
 String path = ""; 
 Table data = null;
 void setupData(File file)
 {
   path = file.getAbsolutePath();
   data = loadTable(path,"header");
 }
}

abstract class frame {
  int u0,v0,w,h;
  int framewidth = 0;
  int frameheight = 0;
  int framemode = 0;
  String tooltip = "";
  String tooltipX = "";
  String tooltipY = "";
  String tooltipXData = "";
  String tooltipYData = "";
  boolean showXScale = false;
  boolean showYScale = false;
  boolean showXTitle = false;
  boolean showYTitle = false;
  boolean showTitle = false;
  void setRect( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
    
    this.framewidth = w -u0;
    this.frameheight = h - v0;
   }
  void drawFrame(){
    stroke(0);
    noFill();
    rect(u0,v0,w,h);
  };
  abstract void setupData
  (String t, String x,int xcol, String y,int ycol,ColorMode cm, int colorcol,CSV csv,ChartType chartType, int pointsize, boolean grid, boolean tooltip);
  abstract void draw(); 
}