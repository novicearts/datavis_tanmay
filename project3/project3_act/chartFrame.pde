class chartFrame extends frame
{ 
  String title = "";
  int titlemargin = 10;
  String xaxis = "";
  int xmargin = 60;
  String yaxis = "";
  int ymargin = 30;
  int yVertMargin = -40;
  int textsize = 32;
  
  int xcolumndata = 0;
  int ycolumndata = 1;
  
  int pointsize = 5;
  
  int rowsize = 0;
  float Xmaxval = 0f;
  float Ymaxval = 0f;
  ChartType chartmode;
  
  ColorMode colorMode;
  int colorColumn = -1;  //if <0, we dont have a use for this
  CSV csv = null;
  
  
  boolean drawGridLines = true;
  boolean drawToolTip = true;
  int gridinterval = 5;
  
  //use the x title and the y title to extract the actual column in the table
  void setupData(String t, String x,int xcol, String y, int ycol, ColorMode cm,int cc,CSV c, ChartType chartType, int pointsize, boolean grid,
                boolean tool)
  {
    title = t;
    xaxis = x;
    ycolumndata = ycol;
    xcolumndata = xcol;
    yaxis = y;
    csv = c;
    this.colorMode = cm;
    this.colorColumn =cc; 
    this.pointsize = pointsize;
    chartmode= chartType;
    drawGridLines = grid;
    drawToolTip = tool;
    if(csv == null)
    {print("failed to save data");}
    else
    {
      rowsize = csv.data.getRowCount();
      for(int i = 0; i < rowsize; i ++)
      {
        if(csv.data.getRow(i).getFloat(xcolumndata) > Xmaxval)
          Xmaxval = csv.data.getRow(i).getFloat(xcolumndata) ;
          
        if(csv.data.getRow(i).getFloat(ycolumndata) > Ymaxval)
          Ymaxval = csv.data.getRow(i).getFloat(ycolumndata) ;
      }
    }
  }
  void draw(){
    stroke(0,0,0);
    drawFrame();
    //draw the title
    fill(50);
    textSize(textsize);
    text(title, u0+w/2 - textsize*title.length()/4, v0 - titlemargin);
    
    //draw the xaxis
    textSize(textsize/2);
    text(xaxis, u0+w/2 - textsize*xaxis.length()/8, v0 + h + xmargin);
    
    //draw the yaxis
    pushMatrix();
    translate(u0 -ymargin,v0 + h/2 -yVertMargin);
    rotate(-PI/2.0);
    text(yaxis, 0, 0);
    popMatrix();
    //now draw the data
    if(csv != null)
   {
      rectMode(CENTER); 
      textSize(textsize/2.5);
      
      
      
      fill(120);
      stroke(120,50);
      
      if(drawGridLines)
      {
        //draw the grid lines in regular intervals
        float xgridinterval = Xmaxval / gridinterval;
        float ygridinterval = Ymaxval / gridinterval;
        
        //draw x lines
        for(int i = 0; i < gridinterval; i++)
        {
          line(u0,v0+h-(i*ygridinterval)*(v0 + frameheight)/Ymaxval,
               (2*u0+framewidth), v0+h-(i*ygridinterval)*(v0 + frameheight)/Ymaxval);
        }
        
        //draw y lines
        for(int i = 0; i < gridinterval; i++)
        {
          line(u0 + i*xgridinterval * (u0 + framewidth)/Xmaxval, v0+h,
               u0 + i*xgridinterval * (u0 + framewidth)/Xmaxval, v0+h-(v0 + frameheight));
        }
      }
      fill(0,200,200);
      stroke(50);
      //initial pass to place data on the chart
      switch(chartmode)
      {
       
       //note: we might need to be careful with how we consider the x coord here 
        case SCATTER:
        {
           //start with getting xsize # of circles drawn
          for(int i = 0; i < rowsize; i++)
          {
            float scatterwidth = csv.data.getRow(i).getFloat(xcolumndata) * (u0 + framewidth)/Xmaxval;
            float scatterheight = csv.data.getRow(i).getFloat(ycolumndata) * (v0 + frameheight)/Ymaxval;
            // --use this code for rendering line charts
            //noStroke();
            
            ellipse(u0+scatterwidth,v0+h-scatterheight,pointsize,pointsize);            
          }
        }
        break;
        
        default:
        break;
      }
      
      
      
      fill(50);
      stroke(0,0,0);
      
      
      rectMode(CORNER);
      
      
      
      //draw the scales (i have my maxval, and i have 0)  
      float scalex = u0;
      
      //position the zeros
      text("0",scalex  - (textsize/2.5)*"0".length(),v0+h);
      text("0",scalex,v0+h + 15);
      
      //position the max values
      text(str((int)Ymaxval),scalex  - (textsize/3)*str((int)Ymaxval).length(),v0+h - (v0 + frameheight));
      text(str((int)Xmaxval),scalex + (u0+ framewidth),v0+h + 15);
      
      //position the halfway points
      text(str((int)Ymaxval/2),scalex  - (textsize/3)*str((int)Ymaxval/2).length(),v0+h - (Ymaxval/2)* (v0 + frameheight)/Ymaxval);
      text(str((int)Xmaxval/2),scalex + (Xmaxval/2)*(u0+ framewidth)/Xmaxval,v0+h + 15);     
      
      
      
      if(drawToolTip)
      {
        for(int i = 0; i < rowsize; i++)
          {
            tooltipXData =  csv.data.getRow(i).getString(xcolumndata);
            tooltipYData =  csv.data.getRow(i).getString(ycolumndata);
            float sx = u0 + csv.data.getRow(i).getFloat(xcolumndata) * (u0 + framewidth)/Xmaxval;
            float sy = v0 + h - ( csv.data.getRow(i).getFloat(ycolumndata) * (v0 + frameheight)/Ymaxval);
            // --use this code for rendering line charts
            //noStroke();
            
            //ellipse(u0+scatterwidth,v0+h-scatterheight,pointsize,pointsize);   
            float pointradius = pointsize/2;
            
            //if we are in ellipse (check this using ellipse math)
            if( sq(mouseX - sx)/(pointradius*pointradius) + sq(mouseY - sy)/(pointradius*pointradius) < 1)
            {
              String s1 = tooltipX+" : "+tooltipXData;
              String s2 = tooltipY+" : "+tooltipYData;
              fill(180);
              stroke(0,0,0);
              if(mouseX > w/2)
              {
                float lengthx = (textsize/2)*s1.length();
                rect(mouseX - lengthx/2,mouseY,lengthx,(textsize/2)*3);
                
                fill(0);
                text(s1,mouseX- lengthx/2+10,mouseY+20);
                text(s2,mouseX- lengthx/2+10,mouseY+40);
              }
              else
              {
                rect(mouseX,mouseY,(textsize/2)*s1.length(),(textsize/2)*3);
                fill(0);
                text(s1,mouseX+20,mouseY+20);
                text(s2,mouseX+20,mouseY+40);
              }
              
            }
            
          }
      }
    }
    
 
  }
}