Controls:
Use left click and drag to move the nodes in the graph around
Use right click to toggle pinning the moused over vertex to stop movement

If you mouse over a given node, a tooltip will show up giving you more details
There should also be a legend telling you any categorical details
