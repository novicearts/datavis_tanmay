// You shouldn't need to modify anything in this file but you can if you want


public static final float DAMPING_COEFFICIENT = 0.65f;
public static final float SCREEN_MARGIN = 20f;
public enum vertMouseMode
{
  NONE,HIGHLIGHTED,CLICKED,PINNED
}

public class GraphVertex {

  String id;
  PVector pos = new PVector(0,0);  
  PVector acc = new PVector(0,0);
  PVector vel = new PVector(0,0);
  PVector frc = new PVector(0,0);
  vertMouseMode mouseMode = vertMouseMode.NONE;
  float mass = 1;
  float diam = 1;
  int group;
  boolean canMove = true;
  float buffer = 0f;
  public GraphVertex()
  {
    id = "NONE";
    group = -1;
    pos.set(-999,-999);
  }
  public GraphVertex( String _id, int _group, float _x, float _y ){
    id = _id;
    group = _group;
    pos.set(_x,_y);
  }

  public String getID(){ return id; }
  
  public boolean inBounds(PVector point)
  {
    //if(pos.sub(point).mag() < diam) return true;
    return (abs(pos.x - point.x) < diam/2 && abs(pos.y - point.y) < diam/2);

  }

  public void    setPosition( float _x, float _y ){ pos.set(_x,_y); }
  public void    setPositionWithBounds( float x, float y )
  {
    if(!canMove) return;
    if(x < SCREEN_MARGIN)
    {
      x = SCREEN_MARGIN + buffer;
      clearForce();
    }
    if(x > width - SCREEN_MARGIN)
    {
      x = width - SCREEN_MARGIN - buffer;
      clearForce();
    }
    if(y < SCREEN_MARGIN)
    {
      y = SCREEN_MARGIN + buffer;
      clearForce();
    }
    if(y > height - SCREEN_MARGIN)
    {
      y = height - SCREEN_MARGIN - buffer;
      clearForce();
    }
    
    
    pos.set(x,y); 
  }
  public PVector getPosition(){ return pos; }

  public void    setMass( float m ){ mass = m; }
  public float   getMass( ){ return mass; }

  public void    setDiameter( float d ){ diam = d; }
  public float   getDiameter( ){ return diam; }

  public void    setVelocity( float _vx, float _vy ){ vel.set(_vx,_vy); }
  public PVector getVelocity(){ return vel; }
  
  public void    setAcceleration( float _ax, float _ay ){ acc.set(_ax,_ay); }
  public PVector getAcceleration(){ return acc; }

  public void    clearForce( ){ frc.set(0,0); }
  public void    addForce( float _fx, float _fy ){ frc.x+=_fx; frc.y+=_fy; }
  public PVector getForce(){ return frc; }
  
  
  
  // the following code probably shouldn't be modified unless you know what you're doing.
  void updatePosition( float deltaT ){
    if(mouseMode == vertMouseMode.PINNED || mouseMode == vertMouseMode.CLICKED) 
    {
      setPositionWithBounds(pos.x,pos.y);
      return;
    }
    float accelerationX = frc.x / mass;
    float accelerationY = frc.y / mass;
      
    setAcceleration(accelerationX, accelerationY);

    float velocityX = (vel.x + deltaT * accelerationX) * DAMPING_COEFFICIENT;
    float velocityY = (vel.y + deltaT * accelerationY) * DAMPING_COEFFICIENT;

    setVelocity(velocityX, velocityY);    
      
    float x = (float) (pos.x + deltaT * velocityX + accelerationX * Math.pow(deltaT, 2.0f) / 2.0f);
    float y = (float) (pos.y + deltaT * velocityY + accelerationY * Math.pow(deltaT, 2.0f) / 2.0f);

    setPositionWithBounds( x, y );
    
  }  

}