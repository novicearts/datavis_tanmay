class TooltipData
{
  String text1 = "";
  String text2 = "";
  String text3 = "";
  String text4 = "";
  boolean showTooltip = true;
  
  void draw(boolean cue_drawing, int drawx, int drawy, int w, int h, int textsize)
  {
    if(cue_drawing && showTooltip)
    {
      fill(180);
      stroke(0,0,0);
      float lengthx = 270f;
      if(drawy+h > height)
        {
          //float lengthx = (textsize/2)*maxstrlen;
          float newdrawy = drawy - 2*h;
          float drawloc = drawx - w/2; 
          rect(drawloc,newdrawy,w,h);
          
          fill(0);
          
          text(text1, drawloc+10,newdrawy+20);
          text(text2, drawloc+10,newdrawy+40);
          text(text3, drawloc+10,newdrawy+60);
          text(text4, drawloc+10,newdrawy+80);
        }
        else
        {
          //float lengthx = (textsize/2)*maxstrlen;
          float drawloc = drawx - w/2; 
          rect(drawloc,drawy,w,h);
          
          fill(0);
          
          text(text1, drawloc+10,drawy+20);
          text(text2, drawloc+10,drawy+40);
          text(text3, drawloc+10,drawy+60);
          text(text4, drawloc+10,drawy+80);
        }
          
    }
  }
}