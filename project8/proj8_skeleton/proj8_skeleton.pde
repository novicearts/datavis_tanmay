//modification to occur here
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
Frame myFrame = null;
boolean isRunning = false;
String helpText = "Controls:\n"+
                  "Use left click and drag to move the nodes in the graph around\n"+
                  "Use right click to toggle pinning the moused over vertex to stop movement\n"+
                  "\n"+
                  "If you mouse over a given node, a tooltip will show up giving you more details\n"+
                  "(Press '/' to toggle tooltips\n"+
                  "There should also be a legend telling you any categorical details\n";


void setup() {
  size(800, 800);
  frameRate(60);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();
    HashMap<Integer,Integer> graphColors = new HashMap<Integer,Integer>();
    int maxgroup = 0;
    int mingroup = 10;
    JSONObject json = loadJSONObject(selection);
    JSONArray nodes = json.getJSONArray("nodes");
    JSONArray links = json.getJSONArray("links");
    
    //start with the nodes
    for(int i = 0; i < nodes.size(); i++)
    {
      JSONObject node = nodes.getJSONObject(i);
      GraphVertex vert = new GraphVertex(node.getString("id"), node.getInt("group"),width/2 + random(-150,150),height/2 + random(-150,150));
      vert.setMass(1);
      
      verts.add(vert);
    }
    //then the links
    for(int i = 0; i < links.size(); i++)
    {
      JSONObject link = links.getJSONObject(i);
      
      GraphEdge edge = new GraphEdge(  null,null, link.getFloat("value") );
      
      //find _v0
      for(GraphVertex v : verts)
       {
        if(v.id.equals(link.getString("source"))){
          edge.v0 = v;
          edge.v0.mass += edge.weight;
          //if the group id is not in graph colors add it
          if(!graphColors.containsKey(v.group))
          {
            if(maxgroup < v.group) maxgroup = v.group;
            if(mingroup > v.group) mingroup = v.group;
            graphColors.put(v.group,color(0,200,200));
          }
          break;
          
        }
       }
      //find _v1
    
      for(GraphVertex v : verts)
      {
        if(v.id.equals(link.getString("target"))){
          edge.v1 = v;
          edge.v1.mass += edge.weight;
          break;
        }
      }
      edges.add(edge);
    }
    for(GraphVertex v:verts)
    {
      float temp = v.getMass()* .25f;
      float scaleval = (temp > 10 ? temp : 10); 
      v.setDiameter(scaleval);
    }

    //now compute the size of the graph colors, and figure out appropriate colors
    colorMode(HSB,360,100,100);
    for(Integer i : graphColors.keySet())
    {
      graphColors.put(i, color(map(i,mingroup,maxgroup,15,350),
                               map(i,mingroup,maxgroup,20,100),
                               map(i,mingroup,maxgroup,100,70),255));
    }
    colorMode(RGB,255);
    myFrame = new ForceDirectedLayout( verts, edges, graphColors );
    //verts.get(50).canMove = false;
    isRunning = true;
  }
  
  
}


void draw() {
  background( 255 );

  if(isRunning){
    
    
    //add helper text
    fill(0);
    text(helpText,SCREEN_MARGIN,height-80-SCREEN_MARGIN);
    
    if ( myFrame != null ) {
      myFrame.setPosition( 0, 0, width, height );
      myFrame.draw();
    }
    
    
    
    //draw legend
    fill(0);
    float y = SCREEN_MARGIN;
    float offset = 20;
    text("Legend(Group):",SCREEN_MARGIN,y);
    ForceDirectedLayout fframe = (ForceDirectedLayout)myFrame;
    for(Integer i : fframe.graphColors.keySet())
    {
      fill(0);
      text(str(i),SCREEN_MARGIN, SCREEN_MARGIN+y);
      fill(fframe.graphColors.get(i));
      ellipse(SCREEN_MARGIN+ 30, SCREEN_MARGIN+y-offset/2,7,7);
      y += offset;
    }
    
    
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

void keyPressed()
{
  if(isRunning){
    switch(key){
    case '/':
    println("toggle tooltip");
    ((ForceDirectedLayout)myFrame).tooltip.showTooltip = !((ForceDirectedLayout)myFrame).tooltip.showTooltip;
    break;
    }
  }
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}