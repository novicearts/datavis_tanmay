// most modification should occur in this file
import java.util.HashMap;

class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 6.0f;   // update this value
  float SPRING_SCALE   = 0.45f; // update this value
  float REPULSE_SCALE  = 275.0f;  // update this value

  float TIME_STEP      = 0.25f;    // probably don't need to update this

  float translucency = 135f;

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;
  
  HashMap<Integer,Integer> graphColors = new HashMap<Integer,Integer>();
  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;
  TooltipData tooltip = new TooltipData();
  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges, HashMap<Integer,Integer> _gc ) {
    verts = _verts;
    edges = _edges;
    graphColors = _gc;
  }

  //similar to Coulomb's law
  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    float thresh = .1f;
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    PVector diff = new PVector(v1.getPosition().x - v0.getPosition().x,
                               v1.getPosition().y - v0.getPosition().y);
    if(diff.mag() < thresh)
    {
      PVector unit = PVector.random2D();
      diff = unit.mult(.1f);     
    }
    diff.limit(1000);
    
    
    float distance = diff.mag();
    if(v0.group == v1.group)
    {
      distance *= 2.05f;
    }
    
    PVector unit = new PVector(diff.x/distance, diff.y/distance);
    float mass = v0.getMass()*v1.getMass();
    PVector repulseForce = new PVector(REPULSE_SCALE * (mass/pow(distance,2))*unit.x,
                                       REPULSE_SCALE * (mass/pow(distance,2))*unit.y);
    
    v0.addForce( -repulseForce.x,-repulseForce.y );
    v1.addForce(  repulseForce.x, repulseForce.y);
    
  }

  //similar to Hooke's law
  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    float thresh = .1f;
    float springLength = edge.springLength;
    PVector diff = new PVector(edge.v1.getPosition().x - edge.v0.getPosition().x,
                               edge.v1.getPosition().y - edge.v0.getPosition().y);
    if(diff.mag() < thresh)
    {
      PVector unit = PVector.random2D();
      diff = unit.mult(.1f);     
    }
    float distance = abs(diff.mag() - RESTING_LENGTH);
    //if(distance < 0f) distance = 1f;
    PVector unit = new PVector(diff.x/distance,diff.y/distance);
    //String curid = edge.v0.id;
    float d = distance - springLength;
    
    PVector springForce = new PVector(-1*SPRING_SCALE*d*unit.x*edge.weight,
                                      -1*SPRING_SCALE*d*unit.y*edge.weight);
     edge.v0.addForce( -springForce.x,-springForce.y );
     edge.v1.addForce(  springForce.x, springForce.y);
  }

  void draw() {
    PVector mouse = new PVector(mouseX,mouseY);
    update(mouse); // don't modify this line
    // TODO: ADD CODE TO DRAW THE GRAPH
    for(int i = 0; i < edges.size(); i++)
    {
      GraphEdge e = edges.get(i);
      GraphVertex v0 = e.v0;
      GraphVertex v1 = e.v1;
      strokeWeight(e.weight);
      
      if(((v0.inBounds(mouse) || v1.inBounds(mouse)) && selected == null) || (selected == v0 || selected == v1))
        stroke(240,50,0,translucency);
      else
        stroke(100,translucency);
      
      line(v0.getPosition().x,v0.getPosition().y,
          v1.getPosition().x,v1.getPosition().y);
      strokeWeight(1);
    }
    
    for(int i = 0; i < verts.size(); i++)
    {
      GraphVertex v = verts.get(i);
      boolean inbounds = v.inBounds(mouse);
      
      if(v.mouseMode != vertMouseMode.PINNED){
      
        if(selected == null){
          if(v.mouseMode == vertMouseMode.NONE && inbounds)
          {
            v.mouseMode = vertMouseMode.HIGHLIGHTED; 
          }
          else if(!inbounds) v.mouseMode = vertMouseMode.NONE;
        }
      }
      
      switch(v.mouseMode)
      {
         
         case HIGHLIGHTED:
         case CLICKED:
         case NONE:
         //change this to do group based colors
         stroke(0);
         strokeWeight(1);
         fill(graphColors.get(v.group));
         break;
         case PINNED:
         stroke(200,50,0);
         strokeWeight(3);
         fill(graphColors.get(v.group));
         break;
      }
      
      ellipseMode(CENTER);
      ellipse(v.getPosition().x,v.getPosition().y,v.diam,v.diam);
      ellipseMode(CORNER);
    }
    
    
    //draw the tooltip
    for(GraphVertex v : verts)
    {
      boolean inbounds = v.inBounds(mouse);
      tooltip.text1 = "ID:    "+v.id;
      tooltip.text2 = "GROUP: "+v.group;
      tooltip.draw(inbounds, mouseX,mouseY+40,200,50,16);
    }
    
  }


  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE
    if(mouseButton == LEFT){
      PVector mouse = new PVector(mouseX,mouseY);
      for(GraphVertex v : verts)
      {
        if(v.inBounds(mouse)){
          selected = v;
          selected.mouseMode = vertMouseMode.CLICKED;
          break;
        }
      }
    }
    else if(mouseButton == RIGHT){
      PVector mouse = new PVector(mouseX,mouseY);
      for(GraphVertex v : verts)
      {
        if(v.inBounds(mouse)){
          if(v.mouseMode != vertMouseMode.PINNED)
            v.mouseMode = vertMouseMode.PINNED;
          else v.mouseMode = vertMouseMode.NONE;
          break;
        }
      }
    }
    
    
  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE
    if(mouseButton == LEFT){
      if(selected != null && selected.mouseMode != vertMouseMode.PINNED)
        selected.mouseMode = vertMouseMode.NONE;
      selected = null;
    }
    else if(mouseButton == RIGHT)
    {
      //do nothing...
    }
  }



  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update(PVector mouse) {
    PVector avg = new PVector();
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
      avg.add(v0.pos);
    }
    avg.div(verts.size());

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
    
    PVector centerdisplacement = (new PVector(width/2,height/2)).sub(avg);
    
    for(GraphVertex v: verts)
    {
      if(v.mouseMode != vertMouseMode.PINNED)
        v.pos.add(centerdisplacement);
    }
    
    //update selected node
    if(selected != null)
    {
       selected.pos =  mouse;
    }
  }
  
}